package config

import (
	"testing"

	"github.com/spf13/viper"
)

func TestInit(t *testing.T) {
	tests := []struct {
		name string
		want Config
	}{
		{
			name: "initialize config",
			want: Conf(),
		},
	}

	defaultConf := Config{}
	defaultConf.Port = "2047"
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Init(viper.GetString("STATION_CONFIG_PATH"))
			if err != nil {
				t.Error(err)
			}
			if got.Port != defaultConf.Port {
				t.Errorf("Init() = %v, want %v", got, tt.want)
			}
		})
	}
}
