package config

import (
	"os"
	"path/filepath"
	"runtime"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
)

type Config struct {
	// env vars
	APIMode         string `mapstructure:"mode"`
	Port            string `mapstructure:"port"`
	BroadcasterPort string `mapstructure:"broadcaster_port"`
	BroadcasterHost string `mapstructure:"broadcaster_host"`
	RDBPort         string `mapstructure:"rdb_port"`

	// secret vars
	PublicAuthKey       string `mapstructure:"public_auth_key"`
	PrivateAuthKey      string `mapstructure:"private_auth_key"`
	MailgunPrivateKey   string `mapstructure:"mailgun_private_api_key"`
	MailgunTestKey      string `mapstructure:"mailgun_test_api_key"`
	MailgunDomain       string `mapstructure:"mailgun_domain"`
	MailgunTestDomain   string `mapstructure:"mailgun_test_domain"`
	YoutubeApiKey       string `mapstructure:"youtube_api_key"`
	SpotifyClientSecret string `mapstructure:"spotify_client_secret"`
	SpotifyClientID     string `mapstructure:"spotify_client_id"`
	DeezerApiSecret     string `mapstructure:"deezer_api_secret"`
	SoundcloudClientID  string `mapstructure:"soundcloud_client_id"`
	TelegramBotKey      string `mapstructure:"telegram_bot_key"`
	InstragramToken     string `mapstructure:"instagram_token"`
	DownloadProxy       string `mapstructure:"download_proxy"`

	// these are config file vars
	LogLevel  string         `mapstructure:"log_level"`
	DBPath    string         `mapstructure:"db_path"`
	Station   StationConfig  `mapstructure:"station"`
	Api       ApiConfig      `mapstructure:"api"`
	Bots      BotsConfig     `mapstructure:"bots"`
	Audio     AudioConfig    `mapstructure:"audio"`
	Times     TimesConfig    `mapstructure:"times"`
	Slots     SlotConfig     `mapstructure:"slots"`
	Playlists PlaylistConfig `mapstructure:"playlists"`
}

type StationConfig struct {
	RunFixtures   bool   `mapstructure:"run_fixtures"`
	FixturesFiles string `mapstructure:"fixtures_file"`
	BaseDir       string `mapstructure:"base_dir"`
	ExtractDir    string
	LibraryDir    string
	RotationDir   string
	SelectionsDir string
	JinglesDir    string
	ImageDir      string
	LogsDir       string
}

type ApiConfig struct {
	ScheduleCacheExpiration time.Duration `mapstructure:"schedule_cache_expiration"`
	RateLimitDuration       time.Duration `mapstructure:"rate_limit_duration"`
	PlaylistPageSize        int           `mapstructure:"playlist_page_size"`
	SlotPageSize            int           `mapstructure:"slot_page_size"`
}

type BotsConfig struct {
	CanSendEmail        bool   `mapstructure:"can_send_email"`
	CanPostBotTelegram  bool   `mapstructure:"can_post_bot_tg"`
	CanPostBotInstagram bool   `mapstructure:"can_post_bot_tg"`
	TeamEmail           string `mapstructure:"team_email"`
	InternalChatID      string `mapstructure:"internal_chat_id"`
	PublicChatID        string `mapstructure:"public_chat_id"`
}

type SlotConfig struct {
	MinNumber int           `mapstructure:"number_min"`
	MaxNumber int           `mapstructure:"number_max"`
	Duration  time.Duration `mapstructure:"duration"`
}

type PlaylistConfig struct {
	MinDuration time.Duration `mapstructure:"duration_min"`
	MaxDuration time.Duration `mapstructure:"duration_max"`
	MinFilesize float64       `mapstructure:"filesize_min"`
	MaxFilesize float64       `mapstructure:"filesize_max"`
}

type AudioConfig struct {
	Encoder             string `mapstructure:"encoder"`
	Format              string `mapstructure:"format"`
	ShouldNormalize     bool   `mapstructure:"should_normalize"`
	ShouldRemoveSilence bool   `mapstructure:"should_remove_silence"`
}

type TimesConfig struct {
	Morning    string `mapstructure:"morning"`
	Afternoon  string `mapstructure:"afternoon"`
	Evening    string `mapstructure:"evening"`
	Night      string `mapstructure:"night"`
	ShouldSync bool   `mapstructure:"should_sync"`
	InSync     bool
}

var conf Config

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

const DEFAULT_CONFIG_PATH = "/etc/station/"

// Init loads a config.toml file based on the provided complete filepath
// or by looking in the DEFAULT_CONFIG_PATH and the working directory
func Init(configPath string) (Config, error) {
	viper.SetEnvPrefix("station")
	viper.AutomaticEnv()

	if viper.GetString("STATION_MODE") == "test" {
		viper.SetConfigName("config.test")
	} else {
		viper.SetConfigName("config")
	}
	viper.SetConfigType("toml")

	if configPath == "" {
		viper.AddConfigPath(filepath.Join(Basepath, "../"))
		viper.AddConfigPath(DEFAULT_CONFIG_PATH)
	} else {
		viper.SetConfigFile(configPath)
	}
	if err := viper.ReadInConfig(); err != nil {
		return conf, err
	}

	viper.OnConfigChange(func(e fsnotify.Event) {
		log.Info().Str("name", e.Name).Msg("[Config] file changed")
		if err := viper.Unmarshal(&conf); err != nil {
			log.Error().Err(err)
		}
	})
	viper.WatchConfig()

	// env defaults
	viper.SetDefault("mode", "dev")
	viper.SetDefault("port", "2047")
	viper.SetDefault("rdb_port", "6379")
	viper.SetDefault("broadcaster_host", "localhost")
	viper.SetDefault("broadcaster_port", "8001")
	viper.SetDefault("db_path", "")

	// secrets defaults
	viper.SetDefault("public_auth_key", "")
	viper.SetDefault("private_auth_key", "")
	viper.SetDefault("mailgun_private_api_key", "")
	viper.SetDefault("mailgun_test_api_key", "")
	viper.SetDefault("mailgun_domain", "")
	viper.SetDefault("mailgun_test_domain", "")
	viper.SetDefault("youtube_api_key", "")
	viper.SetDefault("spotify_client_secret", "")
	viper.SetDefault("spotify_client_id", "")
	viper.SetDefault("deezer_api_secret", "")
	viper.SetDefault("soundcloud_client_id", "")
	viper.SetDefault("telegram_bot_key", "")
	viper.SetDefault("instagram_token", "")
	viper.SetDefault("download_proxy", "")

	viper.SetDefault("log_level", "info")

	//station defaults
	viper.SetDefault("station.run_fixtures", false)
	if conf.APIMode == "test" {
		viper.SetDefault("station.fixtures_file", "playlists_test.yml")
	} else {
		viper.SetDefault("station.fixtures_file", "playlists.yml")
	}
	viper.SetDefault("station.base_dir", "/tmp/radio")
	conf.Station.LibraryDir = "library"
	conf.Station.ExtractDir = "extracted"
	conf.Station.RotationDir = "rotation"
	conf.Station.SelectionsDir = "selections"
	conf.Station.JinglesDir = "jingles"
	conf.Station.ImageDir = "images"
	conf.Station.LogsDir = "logs"

	// api
	viper.SetDefault("api.schedule_cache_expiration", time.Minute*10)
	viper.SetDefault("api.rate_limit_duration", time.Minute)
	viper.SetDefault("api.playlist_page_size", 15)
	viper.SetDefault("api.slot_page_size", 25)

	// bots
	viper.SetDefault("bots.can_send_email", false)
	viper.SetDefault("bots.can_post_bot_tg", false)
	viper.SetDefault("bots.can_post_bot_ig", false)
	viper.SetDefault("bots.internal_chat_id", "")
	viper.SetDefault("bots.public_chat_id", "")

	//slot defaults
	viper.SetDefault("slots.number_min", 8)
	viper.SetDefault("slots.number_max", 8)
	viper.SetDefault("slots.duration", "6h")

	//playlist defaults
	viper.SetDefault("playlists.filesize_min", 0.5)
	viper.SetDefault("playlists.filesize_max", 100.0)
	viper.SetDefault("playlists.duration_min", "10m")
	viper.SetDefault("playlists.duration_max", "6h30m")

	// audio defaults
	viper.SetDefault("audio.encoder", "libfdk_aac")
	viper.SetDefault("audio.format", "aac")
	viper.SetDefault("audio.should_normalize", true)
	viper.SetDefault("audio.should_remove_silence", false)

	// times defaults
	viper.SetDefault("times.morning", "6:00AM")
	viper.SetDefault("times.afternoon", "12:00PM")
	viper.SetDefault("times.evening", "6:00AM")
	viper.SetDefault("times.night", "12:00AM")
	viper.SetDefault("times.should_sync", false)

	if err := viper.Unmarshal(&conf); err != nil {
		panic(err)
	}

	mustMakeAllDirectories(conf.Station)
	if conf.Times.ShouldSync {
		if err := SyncWithBroadcaster(); err != nil {
			log.Error().Err(err).Msg("[Init]")
		}
	}

	return conf, nil
}

// mustMakeAllDirectories creates all the necessary directories in BaseDir
func mustMakeAllDirectories(sc StationConfig) {
	err := os.MkdirAll(sc.BaseDir, os.ModePerm)
	if err != nil {
		panic(err)
	}

	dirs := []string{sc.LibraryDir, sc.ExtractDir, sc.RotationDir, sc.SelectionsDir, sc.JinglesDir, sc.ImageDir, sc.LogsDir}
	for _, dir := range dirs {
		err = os.MkdirAll(filepath.Join(sc.BaseDir, dir), os.ModePerm)
		if err != nil {
			panic(err)
		}
	}

	moments := []string{"morning", "afternoon", "evening", "night"}
	for _, m := range moments {
		err := os.MkdirAll(filepath.Join(sc.BaseDir, sc.RotationDir, m), os.ModePerm)
		if err != nil {
			panic(err)
		}

		err = os.MkdirAll(filepath.Join(sc.BaseDir, sc.LibraryDir, m), os.ModePerm)
		if err != nil {
			panic(err)
		}

		err = os.MkdirAll(filepath.Join(sc.BaseDir, sc.SelectionsDir, m), os.ModePerm)
		if err != nil {
			panic(err)
		}
	}

	binaries := []string{"ffmpeg", "source", "magick"}
	for _, b := range binaries {
		err := os.MkdirAll(filepath.Join(sc.BaseDir, sc.LogsDir, b), os.ModePerm)
		if err != nil {
			panic(err)
		}
	}
}

func Conf() Config {
	return conf
}

func Audio() AudioConfig {
	return conf.Audio
}

func Station() StationConfig {
	return conf.Station
}

func Api() ApiConfig {
	return conf.Api
}

func Bots() BotsConfig {
	return conf.Bots
}

func Playlists() PlaylistConfig {
	return conf.Playlists
}

func Slots() SlotConfig {
	return conf.Slots
}

func Times() TimesConfig {
	return conf.Times
}

func RotationDir() string {
	return filepath.Join(conf.Station.BaseDir, conf.Station.RotationDir)
}

func LibraryDir() string {
	return filepath.Join(conf.Station.BaseDir, conf.Station.LibraryDir)
}

func ExtractDir() string {
	return filepath.Join(conf.Station.BaseDir, conf.Station.ExtractDir)
}

func SelectionsDir() string {
	return filepath.Join(conf.Station.BaseDir, conf.Station.SelectionsDir)
}

func ImageDir() string {
	return filepath.Join(conf.Station.BaseDir, conf.Station.ImageDir)
}

func LogsDir() string {
	return filepath.Join(conf.Station.BaseDir, conf.Station.LogsDir)
}
