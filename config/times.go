package config

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

type broadcasterTimes struct {
	Morning   string `json:"morning"`
	Afternoon string `json:"afternoon"`
	Evening   string `json:"evening"`
	Night     string `json:"night"`
}

func SyncWithBroadcaster() error {
	requestURL := fmt.Sprintf("http://%s:%s/times", conf.BroadcasterHost, conf.BroadcasterPort)
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		conf.Times.InSync = false
		return err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		conf.Times.InSync = false
		return err
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		conf.Times.InSync = false
		return err
	}

	var times broadcasterTimes
	err = json.Unmarshal(resBody, &times)
	if err != nil {
		conf.Times.InSync = false
		return err
	}

	log.Warn().Msg("[syncWithBroadcaster] minutes are ignored during sync check")

	// parse the slot start time
	// compare the hours and minutes for each moment
	broadcasterMorningHour, err := extractHour(times.Morning)
	if err != nil {
		conf.Times.InSync = false
		return err
	}
	stationMorning, _ := time.ParseInLocation(time.Kitchen, conf.Times.Morning, time.Local)
	if stationMorning.Hour() != broadcasterMorningHour {
		conf.Times.InSync = false
		return fmt.Errorf("mismatch station and broadcast mornings: %d %d", stationMorning.Hour(), broadcasterMorningHour)
	}

	broadcasterAfternoonHour, err := extractHour(times.Afternoon)
	if err != nil {
		conf.Times.InSync = false
		return err
	}
	stationAfternoon, _ := time.ParseInLocation(time.Kitchen, conf.Times.Afternoon, time.Local)
	if stationAfternoon.Hour() != broadcasterAfternoonHour {
		conf.Times.InSync = false
		return fmt.Errorf("mismatch station and broadcast afternoon: %d %d", stationAfternoon.Hour(), broadcasterAfternoonHour)
	}

	broadcasterEveningHour, err := extractHour(times.Evening)
	if err != nil {
		conf.Times.InSync = false
		return err
	}
	stationEvening, _ := time.ParseInLocation(time.Kitchen, conf.Times.Evening, time.Local)
	if stationEvening.Hour() != broadcasterEveningHour {
		conf.Times.InSync = false
		return fmt.Errorf("mismatch station and broadcast evenings: %d %d", stationEvening.Hour(), broadcasterEveningHour)
	}

	broadcasterNightHour, err := extractHour(times.Night)
	if err != nil {
		conf.Times.InSync = false
		return err
	}
	stationNight, _ := time.ParseInLocation(time.Kitchen, conf.Times.Night, time.Local)
	if stationNight.Hour() != broadcasterNightHour {
		conf.Times.InSync = false
		return fmt.Errorf("mismatch station and broadcast night: %d %d", stationNight.Hour(), broadcasterNightHour)
	}

	log.Info().Msg("[syncWithBroadcaster] in sync")
	conf.Times.InSync = true

	return nil
}

func (tc TimesConfig) MorningTime() time.Time {
	t, err := time.ParseInLocation(time.Kitchen, tc.Morning, time.Local)
	if err != nil {
		log.Error().Err(err).Str("moment", tc.Morning).Msg("[Times] failed to parse")
	}
	return t

}

func (tc TimesConfig) AfternoonTime() time.Time {
	t, err := time.ParseInLocation(time.Kitchen, tc.Afternoon, time.Local)
	if err != nil {
		log.Error().Err(err).Str("moment", tc.Afternoon).Msg("[Times] failed to parse")
	}
	return t
}

func (tc TimesConfig) EveningTime() time.Time {
	t, err := time.ParseInLocation(time.Kitchen, tc.Evening, time.Local)
	if err != nil {
		log.Error().Err(err).Str("moment", tc.Evening).Msg("[Times] failed to parse")
	}
	return t
}

func (tc TimesConfig) NightTime() time.Time {
	t, err := time.ParseInLocation(time.Kitchen, tc.Night, time.Local)
	if err != nil {
		log.Error().Err(err).Str("moment", tc.Night).Msg("[Times] failed to parse")
	}
	return t
}

func extractHour(raw string) (int, error) {
	r := strings.Split(raw, "-")
	if len(r) < 2 {
		return 0, fmt.Errorf("could not parse broadcaster times: %v", r)
	}
	rawHour := strings.Split(r[0], "h")
	h, err := strconv.Atoi(rawHour[0])
	if err != nil {

		return 0, err
	}
	return h, nil
}
