package library

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"station/station"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
)

func TestDialCRUD(t *testing.T) {
	teardown := setup(false, false)
	defer teardown(t)

	t.Run("Get schedule", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/schedule")
		getSchedule(c)
		assert.Equal(t, http.StatusOK, res.Code)
		var sched []station.Day
		err := json.Unmarshal(res.Body.Bytes(), &sched)
		assert.Nil(t, err)
	})

	t.Run("Get status", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/status")
		getStatus(c)
		assert.Equal(t, http.StatusOK, res.Code)
		t.Log("should check for contents of the response")
	})

}
