package library

import (
	"encoding/json"
	"net/http"
	"station/config"
	"station/station"
	"station/utils"
	"time"

	"github.com/labstack/echo"
	"github.com/rs/zerolog/log"
)

// getSchedule returns an array of Day programming
// first checking a cached version, then asking the database
func getSchedule(c echo.Context) error {
	var schedule []station.Day
	var err error

	if rdb != nil {
		res := rdb.Get(ctx, "schedule")
		raw, err := res.Result()
		if err == nil {
			// we have a schedule BUT
			// we always want a fresh schedule at the beginning of each moment
			// so we force update the schedule if this is the first request we've gotten after a moment has ended
			// first check which moment we're in (e.g. it's 06:01am, so we're between 06:00am and 12:00pm)
			// then, check if the res.Time() is outside one of the bounds (e.g. 05:58am)
			// if that's the case, force a schedule fetch and update cache
			night := config.Times().NightTime().Hour()
			morning := config.Times().MorningTime().Hour()
			afternoon := config.Times().AfternoonTime().Hour()
			evening := config.Times().EveningTime().Hour()
			now := time.Now().Hour()

			// we get the timestamp of last update by getting the TTL
			ttl, err := rdb.TTL(ctx, "schedule").Result()
			if err != nil {
				log.Error().Err(err).Msg("[getSchedule] failed to get updated time")
				return c.JSON(http.StatusInternalServerError, schedule)
			}
			last_updated := time.Now().Add(-1 * (config.Api().ScheduleCacheExpiration - ttl))

			shouldUpdateSchedule := (now >= night && now < morning && last_updated.Hour() >= morning) || (now >= morning && now < afternoon && last_updated.Hour() < morning) || (now >= afternoon && now < evening && last_updated.Hour() < afternoon)

			// if we don't need to force update the schedule, return what we got from the cache
			if !shouldUpdateSchedule {
				err = json.Unmarshal([]byte(raw), &schedule)
				if err != nil {
					log.Error().Err(err).Msg("[getSchedule] parse raw")
					return c.JSON(http.StatusInternalServerError, schedule)
				}
				return c.JSON(http.StatusOK, schedule)
			}
		} else {
			log.Warn().Err(err).Msg("[getSchedule] cache schedule not set")
		}
	}

	schedule, err = station.GetSchedule()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	// update cache
	if rdb != nil {
		raw_bytes, err := json.Marshal(schedule)
		if err != nil {
			log.Error().Err(err).Msg("[getSchedule] failed to marshal schedule")
		}
		rdb.Set(ctx, "schedule", string(raw_bytes), config.Api().ScheduleCacheExpiration)
	}
	return c.JSON(http.StatusOK, schedule)
}

func getStatus(c echo.Context) error {
	status, err := station.GetStatus()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, status)
}

// rotate accepts a POST request from the moment that just finished
// the track can only be (morning | afternoon | evening | night)
// and triggers the rotation
func rotate(c echo.Context) error {
	moment := c.FormValue("moment")
	if !utils.IsValidMoment(moment) {
		return c.String(http.StatusBadRequest, "invalid moment")
	}

	err := station.Rotate(moment)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, moment)
}
