package library

//--  this file takes care of the public API requests

import (
	"context"
	"fmt"
	"net/http"
	"station/config"
	"station/station"
	"strconv"
	"time"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/redis/go-redis/v9"
	"github.com/rs/zerolog/log"
)

func Start() *http.Server {

	router := echo.New()
	router.Use(middleware.CORS())
	router.Use(middleware.LoggerWithConfig(middleware.LoggerConfig{
		Format: "\033[32m${time_rfc3339}\033[0m | ${method} | ${uri} | ${status} | ${remote_ip} | ${error.message}\n",
	}))
	router.Use(middleware.Recover())
	router.Use(middleware.BodyLimit("4M"))
	publicAuthorizer := middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		if config.Conf().PublicAuthKey == "" {
			log.Error().Err(fmt.Errorf("no env PUBLIC_AUTH_KEY to check against")).Msg("env error")
			return false, fmt.Errorf("no env key to check against")
		}
		return key == config.Conf().PublicAuthKey, nil
	})

	privateAuthorizer := middleware.KeyAuth(func(key string, c echo.Context) (bool, error) {
		if config.Conf().PrivateAuthKey == "" {
			log.Error().Err(fmt.Errorf("no env PRIVATE_AUTH_KEY to check against")).Msg("env error")
			return false, fmt.Errorf("no env key to check against")
		}
		return key == config.Conf().PrivateAuthKey, nil
	})

	setupRDB()

	router.GET("/", getDefault)

	playlists := router.Group("/playlists")
	{
		playlists.GET("/all", getAllPlaylists)
		playlists.GET("/", getPlaylists)
		playlists.GET("/leastplayed", getLeastPlayedPlaylists)
		playlists.GET("/:id", getPlaylist)
		playlists.GET("/duration", getDurationPlaylist)
		playlists.POST("/", createPlaylist, rateLimit, publicAuthorizer)
		playlists.POST("/:id/prepare", preparePlaylist, privateAuthorizer)
		playlists.POST("/:id/post", postSocialMediaPlaylist, privateAuthorizer)
		playlists.POST("/:id/post/tg", postSocialMediaTelegram, privateAuthorizer)
		playlists.POST("/:id/post/ig", postSocialMediaInstagram, privateAuthorizer)
		playlists.POST("/:id/images", generateImagesPlaylist, privateAuthorizer)
		playlists.PATCH("/:id", editPlaylist, privateAuthorizer)
		playlists.DELETE("/:id", deletePlaylist, privateAuthorizer)
		playlists.POST("/selections/:moment", addSelection, privateAuthorizer)
	}

	slots := router.Group("/slots")
	{
		slots.GET("/", getAllSlots)
		slots.POST("/", createSlot, privateAuthorizer)
		slots.GET("/:id", getSlot)
		slots.DELETE("/:id", deleteSlot, privateAuthorizer)
		slots.GET("/queue", getQueuedSlots)
		slots.GET("/past", getPastSlots)
		slots.POST("/:id/curate", curateSlot, privateAuthorizer)
		slots.POST("/:id/post", postSocialMediaSlot, privateAuthorizer)
		slots.PATCH("/:id/sort", sortSlotPlaylists, privateAuthorizer)
	}

	moments := router.Group("/moments")
	{
		moments.GET("/:moment/playlists", getMomentPlaylists)
		moments.GET("/:moment/slots", getMomentSlots)
	}

	palettes := router.Group("/palettes")
	{
		palettes.GET("/", getAllPalettes)
		palettes.POST("/", createPalette)
		palettes.PATCH("/playlists/:id", assignPalette)
		palettes.DELETE("/:id", removePalette)
	}

	colors := router.Group("/colors")
	{
		colors.GET("/", getAllColors)
		colors.POST("/", createColor)
		colors.DELETE("/:id", removeColor)
	}

	dial := router.Group("/dial")
	{
		dial.GET("/status", getStatus, privateAuthorizer)
		dial.GET("/broadcaster", getBroadcaster)
		dial.GET("/schedule", getSchedule)
		dial.GET("/config", getConfig, privateAuthorizer)
		dial.POST("/rotate", rotate, privateAuthorizer)
	}

	s := &http.Server{
		Addr:         ":" + config.Conf().Port,
		Handler:      router,
		ReadTimeout:  5 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		log.Info().Str("port", config.Conf().Port).Msg("[Start] api")
		if err := s.ListenAndServe(); err != http.ErrServerClosed {
			panic(err)
		}
	}()

	return s
}

var ctx = context.Background()
var rdb *redis.Client

// setupRDB connects to the Redis service.
// Unless a RDBPort is not present in the config, it initializes the redis client on localhost
func setupRDB() {
	if config.Conf().RDBPort == "" {
		log.Warn().Msg("[setupRDB] no port set in config for redis server")
		return
	}

	rdb = redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("localhost:%s", config.Conf().RDBPort),
		Password: "",
		DB:       0,
	})

	if err := rdb.Ping(ctx).Err(); err != nil {
		log.Error().Err(err).Str("port", config.Conf().RDBPort).Msg("[setupRDB] could not find redis service")
		rdb = nil
	}

}

// rate limiter
func rateLimit(next echo.HandlerFunc) echo.HandlerFunc {
	return func(c echo.Context) error {
		if config.Conf().RDBPort == "" || rdb == nil {
			return next(c)
		}

		ip := c.RealIP()
		now := time.Now().Unix()
		timestamp, err := rdb.Get(ctx, ip).Result()
		if err != nil {
			// first time we see this ip
			if err == redis.Nil {
				err = rdb.Set(ctx, ip, now, time.Hour).Err()
				if err != nil {
					panic(err)
				}
				return next(c)
			} else {
				log.Error().Err(err).Msg("[rateLimit]")
				return err
			}
		}

		// we have a ts now we need to check if it has been 1 minute since last IP try
		i, err := strconv.ParseInt(timestamp, 10, 64)
		if err != nil {
			log.Error().Err(err).Msg("[rateLimit]")
			return err
		}
		ts := time.Unix(i, 0)
		if time.Since(ts) < config.Api().RateLimitDuration {
			log.Warn().Str("ip", ip).Msg("[rateLimit]")
			return c.String(http.StatusTooManyRequests, "please wait a minute")
		}

		err = rdb.Set(ctx, ip, now, time.Hour).Err()
		if err != nil {
			panic(err)
		}
		return next(c)
	}
}

func getDefault(c echo.Context) error {
	return c.String(http.StatusOK, "Welcome to the Dial Radio API! You can learn more at https://gitlab.com/dial-radio/station")
}

func getBroadcaster(c echo.Context) error {

	broadcaster, err := station.GetBroadcaster()
	if err != nil {
		log.Error().Err(err).Msg("[getBroadcaster]")
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, broadcaster)
}

func getConfig(c echo.Context) error {
	return c.JSON(http.StatusOK, config.Conf())
}
