package library

import (
	"fmt"
	"io"
	"math"
	"net/http"
	"net/url"
	"regexp"
	"station/bots"
	"station/config"
	"station/crate"
	"station/mailer"
	"station/provider"
	"station/station"
	"station/utils"
	"strconv"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
	"github.com/rs/zerolog/log"
)

func getAllPlaylists(c echo.Context) error {
	p, err := crate.GetContributionPlaylists()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, p)
}

type PlaylistResponse struct {
	Playlists []crate.Playlist `json:"playlists"`
	Start     int              `json:"start"`
	End       int              `json:"end"`
	Total     int              `json:"total"`
}

// getPlaylists returns a fixed-length page of playlists, given a start page
func getPlaylists(c echo.Context) error {
	page, err := strconv.Atoi(c.QueryParam("page"))

	if c.QueryParam("page") == "" {
		page = 0
	} else if err != nil {
		return c.String(http.StatusBadRequest, fmt.Sprintf("wrong page param: %s", c.QueryParam("page")))
	}

	playlists, err := crate.GetPagedContributionPlaylists(page)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	total, err := crate.GetContributionPlaylists()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	end := math.Min(float64(len(total)), float64((page+1)*config.Api().PlaylistPageSize))
	start := math.Max(0, end-float64(config.Api().PlaylistPageSize))
	resp := PlaylistResponse{
		Playlists: playlists,
		Total:     len(total),
		Start:     int(start) + 1,
		End:       int(end),
	}
	return c.JSON(http.StatusOK, resp)
}

func getLeastPlayedPlaylists(c echo.Context) error {
	moment := c.QueryParam("moment")
	if !utils.IsValidMoment(moment) {
		return c.String(http.StatusBadRequest, fmt.Sprintf("wrong moment: %s", moment))
	}
	playlists, err := crate.GetLeastPlayedPlaylists(moment)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, playlists)
}

func getPlaylist(c echo.Context) error {
	var p crate.Playlist
	var err error
	query := c.Param("id")
	onlyDigits := regexp.MustCompile(`^\d+$`)
	if onlyDigits.MatchString(query) {
		p, err = crate.GetPlaylistBy("id", query, true)
	} else {
		p, err = crate.GetPlaylistBy("slug", query, true)
	}

	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	return c.JSON(http.StatusOK, p)
}

func createPlaylist(c echo.Context) error {
	var payload mailer.ErrorPayload
	var p crate.Playlist

	// send an email if there was any error in the contribute process
	defer func() {
		if payload.Error != "" {
			// only define the payload if we have a playlist
			if p.Artist != "" {
				payload = mailer.ErrorPayload{
					Name:      p.Artist,
					Email:     p.Email,
					Title:     p.Title,
					URL:       p.URL,
					Timestamp: time.Now(),
					Error:     payload.Error,
				}
			}

			err := mailer.SendMail(config.Bots().TeamEmail, "Error report!", "error", payload)
			if err != nil {
				log.Warn().Str("error", err.Error()).Msg("[createPlaylist]")
			}
		}
	}()

	if err := c.Bind(&p); err != nil {
		payload = mailer.ErrorPayload{
			Name:      c.FormValue("artist"),
			Email:     c.FormValue("email"),
			Title:     c.FormValue("title"),
			URL:       c.FormValue("url"),
			Error:     err.Error(),
			Timestamp: time.Now(),
		}

		return c.String(http.StatusBadRequest, payload.Error)
	}

	palette, err := crate.GetPalette(p.Color, p.Moment)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	p.Palette = palette

	validate := validator.New()
	if err := validate.Struct(&p); err != nil {
		payload.Error = err.Error()
		return c.String(http.StatusBadRequest, payload.Error)
	}

	if err = station.Validate(&p); err != nil {
		payload.Error = err.Error()
		return c.String(http.StatusBadRequest, payload.Error)
	}

	created, err := crate.CreatePlaylist(p)
	if err != nil {
		payload.Error = err.Error()
		return c.String(http.StatusInternalServerError, payload.Error)
	}

	img, err := c.FormFile("image")
	if err == nil {
		const IMAGE_REGEX = `(.*\.)(png|PNG|jpg|JPG|jpeg|JPEG|gif|GIF)`
		re := regexp.MustCompile(IMAGE_REGEX)

		if re.FindString(img.Filename) == "" {
			return c.String(http.StatusBadRequest, "the file provided should be .png, .jpg, .jpeg or .gif")
		}
		file, err := img.Open()
		if err != nil {
			payload.Error = err.Error()
			return c.String(http.StatusInternalServerError, payload.Error)
		}
		defer file.Close()
		bytes, err := io.ReadAll(file)
		if err != nil {
			payload.Error = err.Error()
			return c.String(http.StatusInternalServerError, payload.Error)
		}
		err = station.SaveImage(&created, bytes, img.Filename)
		if err != nil {
			payload.Error = err.Error()
			return c.String(http.StatusInternalServerError, payload.Error)
		}
		created, err = crate.EditPlaylist(created)
		if err != nil {
			payload.Error = err.Error()
			return c.String(http.StatusInternalServerError, payload.Error)
		}
	} else {
		log.Warn().Msg("[createPlaylist] no image file supplied, defaulting to fallback")
	}

	if config.Bots().CanSendEmail {
		p := mailer.ReceivedPayload{
			Name:   created.Artist,
			Moment: created.Moment,
			Title:  created.Title,
			URL:    fmt.Sprintf("https://dialradio.live/playlists/%d", created.ID),
		}

		if err := mailer.SendMail(created.Email, "Playlist received!", "received", p); err != nil {
			log.Error().Err(err).Msg("[createPlaylist] sending received email")
		}
	}

	if config.Bots().CanPostBotTelegram {
		err = bots.PostReceivedPlaylist(created)
		if err != nil {
			log.Error().Err(err).Msg("[createPlaylist] failed to post bot")
		}
	}

	go station.Prepare(&created)

	return c.JSON(http.StatusOK, created)
}

func editPlaylist(c echo.Context) error {
	raw_id := c.Param("id")
	id, err := strconv.Atoi(raw_id)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	var p crate.Playlist
	c.Bind(&p)
	p.ID = uint(id)

	if !utils.IsValidMoment(p.Moment) {
		return c.String(http.StatusBadRequest, fmt.Sprintf("moment should be [morning, afternoon, evening, night]: %s", p.Moment))
	}

	existing, err := crate.GetPlaylistBy("id", raw_id, false)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	if p.PaletteID == 0 || crate.IsDefaultPalette(p.Palette) {
		palette, err := crate.GetPalette(existing.Color, existing.Moment)
		if err != nil {
			return c.String(http.StatusNotFound, err.Error())
		}
		p.Palette = palette
	}

	updated, err := crate.EditPlaylist(p)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, updated)
}

func deletePlaylist(c echo.Context) error {
	id := c.Param("id")
	deletedID, err := crate.DeletePlaylist(id)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.String(http.StatusOK, deletedID)
}

func getDurationPlaylist(c echo.Context) error {
	raw_url := c.QueryParam("url")
	u, err := url.Parse(raw_url)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	p, err := provider.New(u.String())
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	d, err := p.Duration()
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	if d < config.Playlists().MinDuration {
		return c.String(http.StatusNotAcceptable, d.String())
	} else if d > config.Playlists().MaxDuration {
		return c.String(http.StatusNotAcceptable, d.String())
	}

	return c.String(http.StatusOK, d.String())
}

func preparePlaylist(c echo.Context) error {
	raw_id := c.Param("id")
	id, err := strconv.Atoi(raw_id)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	p, err := crate.GetPlaylistBy("id", fmt.Sprint(id), true)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	go station.Prepare(&p)

	return c.String(http.StatusAccepted, fmt.Sprintf("making pment %d - %s available", p.ID, p.Filename))
}

func generateImagesPlaylist(c echo.Context) error {
	id := c.Param("id")
	playlist, err := crate.GetPlaylistBy("id", id, true)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	palette, err := crate.GetPaletteByID(fmt.Sprintf("%d", playlist.PaletteID))
	if err != nil {
		log.Error().Err(err).Msg("[Curate] fetching palette")
		return err
	}
	playlist.Palette = palette

	cover, err := bots.GenerateCoverImage(playlist)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	tracklists, err := bots.GenerateTracklistImages(playlist)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, map[string][]string{"images": append([]string{cover}, tracklists...)})
}

func postSocialMediaPlaylist(c echo.Context) error {
	id := c.Param("id")
	playlist, err := crate.GetPlaylistBy("id", id, true)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	err = bots.PostSocialMedia(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[library] post bots")
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "published")
}

func postSocialMediaTelegram(c echo.Context) error {
	id := c.Param("id")
	playlist, err := crate.GetPlaylistBy("id", id, true)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	err = bots.PostTelegram(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[library] post tg")
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "published")
}

func postSocialMediaInstagram(c echo.Context) error {
	id := c.Param("id")
	playlist, err := crate.GetPlaylistBy("id", id, true)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	err = bots.PostInstagram(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[library] post ig")
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "published")
}

// addSelection handles any new selection track that has been played by the Liquid Soap process
func addSelection(c echo.Context) error {
	moment := c.Param("moment")
	if !utils.IsValidMoment(moment) {
		return c.String(http.StatusBadRequest, fmt.Sprintf("%s is not a valid moment", moment))
	}

	if moment != utils.GetLocalMoment() {
		log.Error().Str("remote", moment).Str("local", utils.GetLocalMoment()).Msg("[addSelection] moment mismatch")
		return c.String(http.StatusBadRequest, fmt.Sprintf("moment mismatch! remote: %s vs. local: %s", moment, utils.GetLocalMoment()))
	}

	var selection crate.Track
	if err := c.Bind(&selection); err != nil {
		log.Error().Err(err).Msg("[addSelection]")
		return c.String(http.StatusBadRequest, err.Error())
	}
	selection.Moment = moment
	selection.Type = utils.Selection

	validate := validator.New()
	if err := validate.StructExcept(&selection, "Playlist"); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	err := station.UpdateSelectionPlaylist(&selection)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	log.Info().Str("moment", moment).Str("artist", selection.Artist).Str("title", selection.Title).Msg("[addSelection]")
	return c.String(http.StatusOK, fmt.Sprintf("added %s - %s to current selection", selection.Artist, selection.Title))
}
