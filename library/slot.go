package library

import (
	"fmt"
	"net/http"
	"station/bots"
	"station/crate"
	"station/station"
	"station/utils"
	"strconv"

	"github.com/labstack/echo"
	"github.com/rs/zerolog/log"
)

func getAllSlots(c echo.Context) error {
	var page int

	raw := c.QueryParam("page")
	if raw == "" {
		page = 0
	} else {
		p, err := strconv.Atoi(raw)
		if err != nil {
			return c.String(http.StatusBadRequest, err.Error())
		}

		page = p
	}

	slots, err := crate.GetAllSlots(page)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, slots)
}

func getSlot(c echo.Context) error {
	id := c.Param("id")
	slot, err := crate.GetSlot(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}
	return c.JSON(http.StatusOK, slot)
}

func deleteSlot(c echo.Context) error {
	id := c.Param("id")
	slot, err := crate.GetSlot(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	id, err = crate.DeleteSlot(fmt.Sprint(slot.ID))
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.String(http.StatusOK, id)
}

func createSlot(c echo.Context) error {
	m := c.FormValue("moment")

	if !utils.IsValidMoment(m) {
		return c.String(http.StatusNotFound, fmt.Sprintf("invalid moment: %s [morning, afternoon, evening, night]", m))
	}

	slot, err := station.NewSlot(m)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	return c.JSON(http.StatusOK, slot)
}

func getQueuedSlots(c echo.Context) error {
	slots, err := crate.GetQueuedSlots()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, slots)
}

func getPastSlots(c echo.Context) error {
	slots, err := crate.GetPastSlots()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, slots)
}

func curateSlot(c echo.Context) error {
	id := c.Param("id")
	slot, err := crate.GetSlot(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}
	err = station.Curate(&slot)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}
	return c.JSON(http.StatusOK, slot)
}

func postSocialMediaSlot(c echo.Context) error {
	id := c.Param("id")
	slot, err := crate.GetSlot(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	for _, playlist := range slot.Playlists {
		palette, err := crate.GetPaletteByID(fmt.Sprintf("%d", playlist.PaletteID))
		if err != nil {
			log.Error().Err(err).Msg("[Curate] fetching palette")
			return err
		}
		playlist.Palette = palette

		err = bots.PostSocialMedia(playlist)
		if err != nil {
			log.Error().Err(err).Msg("[Curate] post bots")
			return c.String(http.StatusInternalServerError, err.Error())
		}
	}

	return c.String(http.StatusOK, "published")
}

func sortSlotPlaylists(c echo.Context) error {
	id := c.Param("id")
	slot, err := crate.GetSlot(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	err = station.Sort(&slot)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, fmt.Sprintf("sorted %d", slot.ID))
}
