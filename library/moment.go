package library

import (
	"net/http"
	"station/crate"
	"station/utils"

	"github.com/labstack/echo"
)

// getMomentPlaylists returns all Playlists for a given Moment
func getMomentPlaylists(c echo.Context) error {
	moment := c.Param("moment")
	if !utils.IsValidMoment(moment) {
		return c.String(http.StatusBadRequest, "invalid moment")
	}
	playlists, err := crate.GetPlaylistsByMoment(moment)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, playlists)
}

func getMomentSlots(c echo.Context) error {
	moment := c.Param("moment")
	if !utils.IsValidMoment(moment) {
		return c.String(http.StatusBadRequest, "invalid moment")
	}
	playlists, err := crate.GetSlotsByMoment(moment)
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, playlists)
}
