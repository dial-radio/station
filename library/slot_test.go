package library

import (
	"bytes"
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"station/crate"
	"testing"

	"github.com/labstack/echo"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSlotsCRUD(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Get all slots", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots")
		getAllSlots(c)
		assert.Equal(t, http.StatusOK, res.Code)
		var slots []crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &slots)
		require.Nil(t, err)
		assert.Equal(t, 8, len(slots))
	})

	t.Run("Get all slots with pagination", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots")
		c.QueryParams().Add("page", "1")
		getAllSlots(c)
		assert.Equal(t, http.StatusOK, res.Code)
		var slots []crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &slots)
		require.Nil(t, err)
		assert.Equal(t, 0, len(slots))
	})

	t.Run("Get slot", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots")
		c.SetParamNames("id")
		c.SetParamValues("1")
		getSlot(c)
		require.Equal(t, http.StatusOK, res.Code)
		var slot crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &slot)
		require.Nil(t, err)
		assert.GreaterOrEqual(t, len(slot.Playlists), 1)
	})

	t.Run("Get past slots", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots/past")
		getPastSlots(c)
		assert.Equal(t, http.StatusOK, res.Code)
		var slots []crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &slots)
		assert.Nil(t, err)
	})

	t.Run("Get queued slots", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots/queued")
		getQueuedSlots(c)
		assert.Equal(t, http.StatusOK, res.Code)
		var slots []crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &slots)
		require.Nil(t, err)
		assert.Greater(t, len(slots), 0)
	})

	t.Run("Create slot", func(t *testing.T) {
		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("moment", "night")
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots")

		createSlot(c)

		assert.Equal(t, http.StatusOK, res.Code)
		var slot crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &slot)
		require.Nil(t, err)
	})

	t.Run("Get least played", func(t *testing.T) {
		// get the slot as existing
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/slots")
		c.SetParamNames("id")
		c.SetParamValues("1")
		getSlot(c)
		require.Equal(t, http.StatusOK, res.Code)
		var previous crate.Slot
		err := json.Unmarshal(res.Body.Bytes(), &previous)
		require.Nil(t, err)

		// get least played playlists
		res = httptest.NewRecorder()
		req = httptest.NewRequest(http.MethodGet, fmt.Sprintf("/playlists/leastplayed?moment=%s", previous.Moment), nil)
		c = echo.New().NewContext(req, res)
		getLeastPlayedPlaylists(c)
		require.Equal(t, http.StatusOK, res.Code)
		var playlists []crate.Playlist
		err = json.Unmarshal(res.Body.Bytes(), &playlists)
		require.NotEmpty(t, playlists)
		require.Nil(t, err)

		// curate slot and confirm that playlists are different
		res = httptest.NewRecorder()
		req = httptest.NewRequest(http.MethodGet, "/", nil)
		c = echo.New().NewContext(req, res)
		c.SetPath("/slots")
		c.SetParamNames("id")
		c.SetParamValues("1")
		curateSlot(c)
		require.Equal(t, http.StatusOK, res.Code)
		var slot crate.Slot
		err = json.Unmarshal(res.Body.Bytes(), &slot)
		require.Nil(t, err)
		assert.GreaterOrEqual(t, len(slot.Playlists), 1)
		assert.NotEqual(t, previous.Playlists[0].ID, slot.Playlists[0].ID)
		assert.Equal(t, slot.Playlists[0].ID, playlists[0].ID)
	})

}
