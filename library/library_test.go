package library

import (
	"context"
	"path/filepath"
	"runtime"
	"station/config"
	"station/crate"
	"station/station"
	"station/utils"
	"testing"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

func setup(runFixtures bool, runInit bool) func(t *testing.T) {
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	viper.Set("STATION_MODE", "test")
	viper.Set("STATION_RDB_PORT", "") // skip rate limit during tests
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	if err := crate.OpenDB(utils.Memory, true); err != nil {
		panic(err)
	}

	if runFixtures {
		if err := crate.RunFixtures(); err != nil {
			panic(err)
		}
	}

	if runInit {
		if err := station.Init(); err != nil {
			panic(err)
		}
	}

	return func(t *testing.T) {}
}

func TestRouterSetup(t *testing.T) {
	teardown := setup(false, false)
	defer teardown(t)

	t.Run("Setup router", func(t *testing.T) {
		server := Start()
		require.NotNil(t, server)
		server.Shutdown(context.Background())
	})
}
