package library

import (
	"bytes"
	"encoding/json"
	"fmt"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"path/filepath"
	"station/crate"
	"station/utils"
	"strings"
	"testing"

	"github.com/joho/godotenv"
	"github.com/labstack/echo"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const EXPECTED_PLAYLIST_TOTAL = 12

func TestPlaylistsGet(t *testing.T) {
	teardown := setup(true, false)
	defer teardown(t)

	t.Run("Get all playlists", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")
		getAllPlaylists(c)
		assert.Equal(t, http.StatusOK, res.Code)
		var playlists []crate.Playlist
		err := json.Unmarshal(res.Body.Bytes(), &playlists)
		require.Nil(t, err)
		assert.Equal(t, EXPECTED_PLAYLIST_TOTAL, len(playlists))
	})

	t.Run("Get least played playlists", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/playlists/leastplayed?moment=afternoon", nil)
		c := echo.New().NewContext(req, res)
		getLeastPlayedPlaylists(c)
		require.Equal(t, http.StatusOK, res.Code)
		var playlists []crate.Playlist
		err := json.Unmarshal(res.Body.Bytes(), &playlists)
		require.Nil(t, err)
		assert.GreaterOrEqual(t, len(playlists), 1)
	})

	t.Run("Get playlist", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodGet, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")
		c.SetParamNames("id")
		c.SetParamValues("1")
		getPlaylist(c)
		require.Equal(t, http.StatusOK, res.Code)
		var playlist crate.Playlist
		err := json.Unmarshal(res.Body.Bytes(), &playlist)
		require.Nil(t, err)
		assert.Equal(t, "1_morning.ogg", playlist.Filename)
	})

}

func TestPlaylistsCreate(t *testing.T) {
	if viper.GetString("STATION_YOUTUBE_API_KEY") == "" {
		if err := godotenv.Load(filepath.Join(Basepath, "../.secrets")); err != nil {
			t.Log("failed to load secrets env")
		}
	}
	teardown := setup(true, false)
	defer teardown(t)

	t.Run("Create playlist", func(t *testing.T) {
		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("email", "test@pierre.net")
		writer.WriteField("description", "Test description")
		writer.WriteField("moment", utils.Evening)
		writer.WriteField("artist", "periode")
		writer.WriteField("title", "my super mix")
		writer.WriteField("url", "https://www.youtube.com/playlist?list=PL1EGwoLDLFtI3m9N_ufXDr_8wvJF7DJzh")
		writer.WriteField("color", "#ff0000")

		part, _ := writer.CreateFormFile("image", "test.png")
		bytes, err := os.ReadFile(fmt.Sprintf("../files/fallback_%s.png", utils.Evening))
		if err != nil {
			t.Fail()
		}
		part.Write(bytes)
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")

		createPlaylist(c)

		assert.Equal(t, http.StatusOK, res.Code)
		var playlist crate.Playlist
		err = json.Unmarshal(res.Body.Bytes(), &playlist)
		require.Nil(t, err)
	})

	t.Run("Create playlist with missing moment", func(t *testing.T) {
		f := make(url.Values)
		f.Set("url", "https://stream.test.live/test")

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(f.Encode()))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationForm)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")

		createPlaylist(c)

		assert.Equal(t, http.StatusBadRequest, res.Code)
	})

	t.Run("Create playlist with malformed URL", func(t *testing.T) {
		f := make(url.Values)
		f.Set("title", "third test title")
		f.Set("artist", "third test artist")
		f.Set("url", "live/test")

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", strings.NewReader(f.Encode()))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationForm)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")

		createPlaylist(c)

		assert.Equal(t, http.StatusBadRequest, res.Code)
	})
}

func TestPlaylistsOperations(t *testing.T) {
	teardown := setup(true, false)
	defer teardown(t)

	t.Run("Edit playlist (no Moment change)", func(t *testing.T) {
		// first get the playlist to find out its Moment
		gres := httptest.NewRecorder()
		greq := httptest.NewRequest(http.MethodGet, "/", nil)
		gc := echo.New().NewContext(greq, gres)
		gc.SetPath("/playlists")
		gc.SetParamNames("id")
		gc.SetParamValues("1")
		getPlaylist(gc)
		require.Equal(t, http.StatusOK, gres.Code)
		var gplaylist crate.Playlist
		err := json.Unmarshal(gres.Body.Bytes(), &gplaylist)
		require.Nil(t, err)

		f := make(url.Values)
		f.Set("description", "edited description")
		f.Set("moment", gplaylist.Moment)

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPatch, "/", strings.NewReader(f.Encode()))
		req.Header.Set(echo.HeaderContentType, echo.MIMEApplicationForm)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")
		c.SetParamNames("id")
		c.SetParamValues("1")

		editPlaylist(c)

		assert.Equal(t, http.StatusOK, res.Code)
		var playlist crate.Playlist
		err = json.Unmarshal(res.Body.Bytes(), &playlist)
		require.Nil(t, err)
		assert.Equal(t, gplaylist.Palette, playlist.Palette)
		assert.Equal(t, gplaylist.Color, playlist.Color)
		assert.Equal(t, playlist.Description, "edited description")
	})

	t.Run("Delete playlist", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodDelete, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists")
		c.SetParamNames("id")
		c.SetParamValues("1")

		deletePlaylist(c)

		assert.Equal(t, http.StatusOK, res.Code)
		assert.Equal(t, "1", res.Body.String())
	})

	t.Run("Make playlist available", func(t *testing.T) {
		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodDelete, "/", nil)
		c := echo.New().NewContext(req, res)
		c.SetPath("/playlists/9/available")
		c.SetParamNames("id")
		c.SetParamValues("9")

		preparePlaylist(c)

		assert.Equal(t, http.StatusAccepted, res.Code)
	})
}

func TestSelections(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Create add selection", func(t *testing.T) {
		moment := utils.GetLocalMoment()
		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("moment", moment)
		writer.WriteField("artist", "test selection artist")
		writer.WriteField("title", "test selection title")
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath(fmt.Sprintf("/playlists/selections/%s", moment))
		c.SetParamNames("moment")
		c.SetParamValues(moment)

		addSelection(c)

		assert.Equal(t, http.StatusOK, res.Code)
	})

	t.Run("Create add selection (wrong moment)", func(t *testing.T) {
		var moment string

		switch utils.GetLocalMoment() {
		case utils.Morning:
			moment = utils.Afternoon
		case utils.Afternoon:
			moment = utils.Evening
		case utils.Evening:
			moment = utils.Night
		case utils.Night:
			moment = utils.Morning
		default:
			t.Error("switch case fail")
		}

		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("moment", moment)
		writer.WriteField("artist", "test selection artist")
		writer.WriteField("title", "test selection title")
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath(fmt.Sprintf("/playlists/selections/%s", moment))
		c.SetParamNames("moment")
		c.SetParamValues(moment)

		addSelection(c)

		assert.Equal(t, http.StatusBadRequest, res.Code)
	})

	t.Run("Create add selection (invalid moment)", func(t *testing.T) {
		var moment string

		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("moment", moment)
		writer.WriteField("artist", "test selection artist")
		writer.WriteField("title", "test selection title")
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath(fmt.Sprintf("/playlists/selections/%s", utils.Morning))
		c.SetParamNames("moment")
		c.SetParamValues(moment)

		addSelection(c)

		assert.Equal(t, http.StatusBadRequest, res.Code)
	})

	t.Run("Create add selection (invalid moment)", func(t *testing.T) {
		var moment string

		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("moment", moment)
		writer.WriteField("artist", "test selection artist")
		writer.WriteField("title", "test selection title")
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath(fmt.Sprintf("/playlists/selections/%s", utils.Morning))
		c.SetParamNames("moment")
		c.SetParamValues(moment)

		addSelection(c)

		assert.Equal(t, http.StatusBadRequest, res.Code)
	})

	t.Run("Create add selection (invalid track)", func(t *testing.T) {
		var moment = utils.GetLocalMoment()

		body := new(bytes.Buffer)
		writer := multipart.NewWriter(body)
		writer.WriteField("moment", moment)
		writer.WriteField("artist", "test selection artist is more than 100000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000 chars")
		writer.WriteField("title", "test selection title")
		writer.Close()

		res := httptest.NewRecorder()
		req := httptest.NewRequest(http.MethodPost, "/", body)
		req.Header.Set(echo.HeaderContentType, writer.FormDataContentType())
		c := echo.New().NewContext(req, res)
		c.SetPath(fmt.Sprintf("/playlists/selections/%s", moment))
		c.SetParamNames("moment")
		c.SetParamValues(moment)

		addSelection(c)

		assert.Equal(t, http.StatusBadRequest, res.Code)
	})
}
