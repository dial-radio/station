package library

import (
	"net/http"
	"station/crate"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
)

func getAllPalettes(c echo.Context) error {
	palettes, err := crate.GetAllPalettes()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, palettes)
}

func createPalette(c echo.Context) error {
	var palette crate.Palette
	if err := c.Bind(&palette); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	validate := validator.New()
	if err := validate.Struct(&palette); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	crate.CreatePalette(palette)
	return c.JSON(http.StatusOK, palette)
}

// assignPalette adds or replaces the current Palette of a Playlist
func assignPalette(c echo.Context) error {
	id := c.Param("id")
	playlist, err := crate.GetPlaylistBy("id", id, false)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	p, err := crate.GetPalette(playlist.Color, playlist.Moment)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}
	playlist.Palette = p

	edited, err := crate.EditPlaylist(playlist)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.JSON(http.StatusOK, edited)
}

func removePalette(c echo.Context) error {
	id := c.Param("id")
	p, err := crate.GetPaletteByID(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	err = crate.RemovePalette(p.ID)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "deleted")
}
