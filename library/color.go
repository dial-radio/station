package library

import (
	"net/http"
	"station/crate"

	"github.com/go-playground/validator/v10"
	"github.com/labstack/echo"
)

func getAllColors(c echo.Context) error {
	colors, err := crate.GetAllColors()
	if err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	return c.JSON(http.StatusOK, colors)
}

func createColor(c echo.Context) error {
	var color crate.Color
	if err := c.Bind(&color); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}

	validate := validator.New()
	if err := validate.Struct(&color); err != nil {
		return c.String(http.StatusBadRequest, err.Error())
	}
	color.Type = crate.COLOR_HIGHLIGHT

	crate.CreateColor(color)
	return c.JSON(http.StatusOK, color)
}

func removeColor(c echo.Context) error {
	id := c.Param("id")
	p, err := crate.GetColor(id)
	if err != nil {
		return c.String(http.StatusNotFound, err.Error())
	}

	err = crate.DeleteColor(p.ID)
	if err != nil {
		return c.String(http.StatusInternalServerError, err.Error())
	}

	return c.String(http.StatusOK, "deleted")
}
