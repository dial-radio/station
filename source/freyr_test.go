package source

import (
	"net/url"
	"path/filepath"
	"station/config"
	"testing"
)

func TestFreyrSource_Copy(t *testing.T) {
	teardown := setup()
	defer teardown(t)

	targetUrl, _ := url.Parse("https://www.deezer.com/us/playlist/11927510201")
	targetDir := filepath.Join(config.Station().BaseDir, "copy-freyr")

	type fields struct {
		url url.URL
	}
	type args struct {
		dst string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "minimal deezer playlist",
			fields: fields{
				url: *targetUrl,
			},
			args: args{
				dst: targetDir,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := FreyrSource{
				url: tt.fields.url,
			}
			if err := d.Copy(tt.args.dst); (err != nil) != tt.wantErr {
				if err.Error() == "exit status 4" {
					t.Skip("Freyr network error should only happen on GitLab CI. Skipping...")
				} else {
					t.Errorf("FreyrSource.Copy() error = %v, wantErr %v", err, tt.wantErr)
				}
			}
		})
	}
}
