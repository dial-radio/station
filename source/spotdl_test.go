package source

import (
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"station/config"
	"station/utils"
	"testing"

	"github.com/stretchr/testify/assert"
)

const spotifyPlaylist = "https://open.spotify.com/playlist/3wdnljwPkhfRpIZCyyyzBc?si=N5XvD7WcQ_C8KGZ7I8BvbQ"

// const spotifyPlaylistShort = "https://spotify.link/alWo6b7P9Db"

func TestSpotDLSource_Copy(t *testing.T) {
	teardown := setup()
	defer teardown(t)

	targetUrl, _ := url.Parse(spotifyPlaylist)
	// targetUrlShort, _ := url.Parse(spotifyPlaylistShort)
	targetDir := filepath.Join(config.Station().BaseDir, "copy-spotdl")

	type fields struct {
		url url.URL
	}
	type args struct {
		dst string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "minimal playlist",
			fields: fields{
				url: *targetUrl,
			},
			args: args{
				dst: targetDir,
			},
			want:    2,
			wantErr: false,
		},
		// {
		// 	name: "long playlist with .link ending URL",
		// 	fields: fields{
		// 		url: *targetUrlShort,
		// 	},
		// 	args: args{
		// 		dst: targetDir,
		// 	},
		//  want: 14,
		// 	wantErr: false,
		// },
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := SpotDLSource{
				url: tt.fields.url,
			}
			if err := d.Copy(tt.args.dst); (err != nil) != tt.wantErr {
				t.Errorf("SpotDLSource.Copy() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				//check for files
				list, err := os.ReadDir(targetDir)
				if err != nil {
					t.Error(err)
				}
				assert.Equal(t, tt.want, len(list), fmt.Sprintf("expecting %d files to be copied, found %d", tt.want, len(list)))
			}
		})
	}

	utils.CleanupDirFiles(targetDir)
}
