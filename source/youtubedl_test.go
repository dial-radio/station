package source

import (
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"station/utils"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestYoutubeDLSource_Copy(t *testing.T) {
	teardown := setup()
	defer teardown(t)

	targetUrlYT, _ := url.Parse("https://www.youtube.com/playlist?list=PLyQtZRmg0ogcmftmAcCLwgpFFDNxG9xI-")
	targetUrlSC, _ := url.Parse("https://soundcloud.com/pierrepierre/sets/test")
	targetUrlMC, _ := url.Parse("https://www.mixcloud.com/MesmaRecords/good-nuit-by-aymar/")
	targetDir := filepath.Join(os.TempDir(), "copy-yt-dl")

	type fields struct {
		url url.URL
	}
	type args struct {
		dst string
	}
	tests := []struct {
		name      string
		fields    fields
		args      args
		wantErr   bool
		wantFiles int
	}{
		{
			name: "minimal playlist YT",
			fields: fields{
				url: *targetUrlYT,
			},
			args: args{
				dst: targetDir,
			},
			wantErr:   false,
			wantFiles: 2,
		},
		{
			name: "minimal playlist SC",
			fields: fields{
				url: *targetUrlSC,
			},
			args: args{
				dst: targetDir,
			},
			wantErr:   false,
			wantFiles: 1,
		},
		{
			name: "minimal playlist MC",
			fields: fields{
				url: *targetUrlMC,
			},
			args: args{
				dst: targetDir,
			},
			wantErr:   false,
			wantFiles: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := YoutubeDLSource{
				url: tt.fields.url,
			}
			if err := d.Copy(tt.args.dst); (err != nil) != tt.wantErr {
				t.Errorf("YoutubeDLSource.Copy() error = %v, wantErr %v", err, tt.wantErr)
			} else {
				//check for files
				list, err := os.ReadDir(targetDir)
				if err != nil {
					t.Error(err)
				}
				assert.Equal(t, tt.wantFiles, len(list), fmt.Sprintf("expecting %d files to be copied", tt.wantFiles))
				utils.CleanupDirFiles(filepath.Join(os.TempDir(), "copy-yt-dl"))

			}
		})
	}
}
