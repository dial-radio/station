package source

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"station/config"
	"station/utils"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

type YoutubeDLSource struct {
	url url.URL
}

// NewYoutubeDLSource checks that the url links to a public playlist
// then calculates the duration of the playlist, and returns an error if the playlist is too short or too long
func NewYoutubeDLSource(url url.URL) (YoutubeDLSource, error) {
	src := YoutubeDLSource{url: url}
	return src, nil
}

func (s YoutubeDLSource) URL() url.URL {
	return s.url
}

func (s YoutubeDLSource) Copy(dst string) error {
	if _, err := exec.LookPath("yt-dlp"); err != nil {
		return err
	}

	err := os.MkdirAll(dst, os.ModePerm)
	if err != nil {
		log.Warn().Msg("[Copy] cannot make dest dir")
		return err
	}

	args := []string{}
	args = append(args, "--yes-playlist")
	args = append(args, "--no-abort-on-error")
	args = append(args, "--ignore-errors")
	args = append(args, "--sleep-interval", "2")
	args = append(args, "-x")
	if config.Audio().Format == "ogg" {
		args = append(args, "--audio-format", "vorbis")
	} else {
		args = append(args, "--audio-format", config.Audio().Format)
	}
	args = append(args, "--audio-quality", "9")
	args = append(args, "--no-progress")
	args = append(args, "--no-overwrites")
	args = append(args, "--embed-metadata")
	if config.Conf().DownloadProxy != "" {
		args = append(args, "--proxy", config.Conf().DownloadProxy)
	}
	args = append(args, "-o", filepath.Join(dst, "%(playlist_index)s-%(title)s.%(ext)s"))
	args = append(args, s.url.String())

	cmd := exec.Command("yt-dlp", args...)
	suffix := utils.RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_ytdl_%s.log", y, utils.PadDateTime(int(m)), utils.PadDateTime(d), utils.PadDateTime(h), utils.PadDateTime(min), utils.PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "source", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	log.Debug().Strs("args", args).Str("URL", s.url.String()).Msg("[copy] youtube-dl")
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error starting youtube-dl")
		return err
	}

	err = cmd.Wait()
	if err != nil {
		// check if the files are actually downloaded
		hasDownloaded := false
		file, err := os.Open(filepath.Join(config.LogsDir(), "source", log_dst))
		if err != nil {
			return err
		}
		defer file.Close()
		scan := bufio.NewScanner(file)
		for scan.Scan() {
			if strings.Contains(scan.Text(), "[download] Finished downloading playlist") {
				hasDownloaded = true
			}
		}

		if hasDownloaded { // false positive
			log.Warn().Msg("[Copy] return code 1 from yt-dlp but files are downloaded, continuing")
		} else {
			log.Error().Err(err).Msg("[Copy] error completing youtube-dl")
			return err
		}
	}

	return nil
}
