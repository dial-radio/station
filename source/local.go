package source

import (
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"station/config"
	"station/utils"

	"github.com/rs/zerolog/log"
)

type LocalSource struct {
	url url.URL
}

// NewLocalSource checks that all files are there, and that they are not too heavy. It differentiates between file and directory. The current implementation might not be the most efficient.
func NewLocalSource(url url.URL) (LocalSource, error) {
	var src LocalSource

	fileinfo, err := os.Stat(url.Path)
	if err != nil {
		log.Warn().Msg("[NewLocalSource] file does not exist")
		return src, err
	}

	var paths []string
	if fileinfo.IsDir() {
		entries, err := os.ReadDir(url.Path)
		if err != nil {
			return src, err
		}
		for _, ent := range entries {
			if !ent.IsDir() {
				paths = append(paths, filepath.Join(url.Path, ent.Name()))
			}
		}
	} else {
		paths = append(paths, url.Path)
	}

	for _, path := range paths {
		fileinfo, err := os.Stat(path)
		if err != nil {
			log.Warn().Msg("[NewLocalSource] file does not exist")
			return src, err
		}

		mb := float64(fileinfo.Size()) / 1_000_000.0
		if mb < config.Playlists().MinFilesize || mb > config.Playlists().MaxFilesize {
			log.Warn().Float64("size", mb).Str("file", fileinfo.Name()).Msg("[NewLocalSource] file is wrong size")
			return src, fmt.Errorf("%s: filesize should be between %fMb and %fMb: %fMb", fileinfo.Name(), config.Playlists().MinFilesize, config.Playlists().MaxFilesize, mb)
		}

		ext := filepath.Ext(path)
		if !utils.IsAudioExtension(ext) {
			log.Error().Str("ext", fileinfo.Name()).Msg("check for the file extension!")
			return src, fmt.Errorf("file extension should be: %s", config.Audio().Format)
		}

	}

	src = LocalSource{url: url}
	return src, nil
}

func (d LocalSource) URL() url.URL {
	return d.url
}

// Downlad LocalSource checks for a file (TOOD: a directory), parses metadata and copies the files over to the ExtractedDir
func (d LocalSource) Copy(dst string) error {

	err := os.MkdirAll(dst, os.ModePerm)
	if err != nil {
		log.Warn().Msg("[Copy] cannot make dest dir")
		return err
	}

	fileinfo, err := os.Stat(d.url.Path)
	if err != nil {
		log.Warn().Msg("[Copy] path does not exist")
		return err
	}

	var paths []string
	if fileinfo.IsDir() {
		entries, err := os.ReadDir(d.url.Path)
		if err != nil {
			return err
		}
		for _, ent := range entries {
			if !ent.IsDir() {
				paths = append(paths, filepath.Join(d.url.Path, ent.Name()))
			}
		}

	} else {
		paths = append(paths, d.url.Path)
	}

	for _, p := range paths {
		info, err := os.Stat(p)
		if err != nil {
			log.Warn().Msg("[Copy] file does not exist")
			return err
		}

		var dest = filepath.Join(dst, info.Name())
		err = utils.CopyFile(p, dest)
		if err != nil {
			return err
		}
	}

	return err
}
