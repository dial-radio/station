package source

import (
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"station/config"
	"station/utils"
	"time"

	"github.com/rs/zerolog/log"
)

type SpotDLSource struct {
	url url.URL
}

func NewSpotDLSource(url url.URL) (SpotDLSource, error) {
	src := SpotDLSource{url: url}
	return src, nil
}

func (s SpotDLSource) URL() url.URL {
	return s.url
}

func (s SpotDLSource) Copy(dst string) error {
	if _, err := exec.LookPath("spotdl"); err != nil {
		return err
	}

	err := os.MkdirAll(dst, os.ModePerm)
	if err != nil {
		log.Warn().Msg("[Copy] cannot make dest dir")
		return err
	}

	args := []string{}
	args = append(args, "download")
	args = append(args, s.url.String())
	args = append(args, "--print-errors")
	if config.Audio().Format == "aac" {
		args = append(args, "--format", "m4a")
	} else if config.Audio().Format == "ogg" {
		args = append(args, "--format", "opus")
	} else {
		args = append(args, "--format", config.Audio().Format)
	}
	args = append(args, "--ffmpeg-args", fmt.Sprintf("-c:a %s", config.Audio().Encoder))
	if config.Conf().SpotifyClientID != "" {
		args = append(args, "--client-id", config.Conf().SpotifyClientID)
	}
	if config.Conf().SpotifyClientSecret != "" {
		args = append(args, "--client-secret", config.Conf().SpotifyClientSecret)
	}
	if config.Conf().DownloadProxy != "" {
		args = append(args, "--yt-dlp-args", fmt.Sprintf("--proxy %s", config.Conf().DownloadProxy))
	}
	args = append(args, "--output", filepath.Join(dst, "{list-position}_{artist}_{title}.{output-ext}"))

	cmd := exec.Command("spotdl", args...)
	suffix := utils.RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_spotdl_%s.log", y, utils.PadDateTime(int(m)), utils.PadDateTime(d), utils.PadDateTime(h), utils.PadDateTime(min), utils.PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "source", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	log.Debug().Strs("args", args).Str("URL", s.url.String()).Msg("[copy] spotdl")
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error starting spotdl")
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error completing spotdl")
		return err
	}

	return nil
}
