# Source

This package takes care of managing the different ways to copy playlists to a local file system. Each source needs to be able to implement the generic `Source` interface:

```go
// a Source is an interface for downloading files
type Source interface {
  Copy(dst string) error // Copy takes a path as input and makes sure all files are available in that directory
  URL() url.URL // URL returns the source url
}
```

## Current sources

- [yt-dlp](https://github.com/yt-dlp/yt-dlp)
- [freyrjs](https://github.com/miraclx/freyr-js)
- [spotdl](https://github.com/spotDL/spotify-downloader)
- local disk
