package source

import (
	"bufio"
	"fmt"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"station/config"
	"station/utils"
	"time"

	"github.com/grafov/m3u8"
	"github.com/rs/zerolog/log"
)

type FreyrSource struct {
	url url.URL
}

func NewFreyrSource(url url.URL) (FreyrSource, error) {
	src := FreyrSource{url: url}
	return src, nil
}

func (s FreyrSource) URL() url.URL {
	return s.url
}

func (s FreyrSource) Copy(dst string) error {
	if _, err := exec.LookPath("freyr"); err != nil {
		return err
	}

	err := os.MkdirAll(dst, os.ModePerm)
	if err != nil {
		log.Warn().Msg("[Copy] cannot make dest dir")
		return err
	}

	args := []string{}
	args = append(args, "get")
	args = append(args, "--no-logo")
	args = append(args, "--no-header")
	args = append(args, "--no-tree")
	args = append(args, "--no-bar")
	args = append(args, "--no-cover")
	args = append(args, "-p", "playlist")
	args = append(args, "--playlist-noescape")
	if config.Conf().APIMode == "release" {
		args = append(args, "--config", "/home/pierre/.config/FreyrCLI/d3fault.x4p")
	}
	args = append(args, "-d", dst)
	args = append(args, s.url.String())

	cmd := exec.Command("freyr", args...)

	suffix := utils.RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_freyr_%s.log", y, utils.PadDateTime(int(m)), utils.PadDateTime(d), utils.PadDateTime(h), utils.PadDateTime(min), utils.PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "source", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	log.Debug().Strs("args", args).Str("URL", s.url.String()).Msg("[copy] freyr")
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error starting freyr")
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error completing freyr")
		return err
	}

	// index files via playlist
	f, err := os.Open(filepath.Join(dst, "playlist.m3u8"))
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error opening playlist")
		return err
	}
	p, listType, err := m3u8.DecodeFrom(bufio.NewReader(f), true)
	if err != nil {
		log.Error().Err(err).Msg("[Copy] error decoding playlist")
		return err
	}
	if listType != m3u8.MEDIA {
		log.Error().Err(err).Msg("[Copy] playlist is not media type!")
		return err
	}

	mediapl := p.(*m3u8.MediaPlaylist)
	for i, m := range mediapl.Segments {
		if m != nil {
			err := os.Rename(filepath.Join(dst, m.URI), filepath.Join(dst, fmt.Sprintf("0%d_%s", (i+1), m.URI)))
			if err != nil {
				log.Error().Err(err).Msg("[Copy] failed to rename file")
				return err
			}
		}
	}

	return nil
}
