package source

import (
	"fmt"
	"net/url"
	"path/filepath"
	"station/provider"

	"github.com/rs/zerolog/log"
)

// a Source is an interface for downloading files
type Source interface {
	Copy(dst string) error // Copy takes a path as input and makes sure all files are available in that directory
	URL() url.URL
}

// New returns the appropriate Source based on a regex match of the rawURL's Hostname()
func New(rawUrl string) (Source, error) {
	u, err := url.ParseRequestURI(rawUrl)
	if err != nil {
		return nil, err
	}

	if filepath.IsAbs(rawUrl) { // local path
		fp := fmt.Sprintf("file://%s", rawUrl)
		u, err := url.Parse(fp)
		if err != nil {
			return nil, err
		}
		return NewLocalSource(*u)
	} else { // remote path
		p, err := provider.Identify(*u)
		if err != nil {
			log.Error().Err(err).Msg("[NewSource]")
		}

		switch p {
		case provider.YOUTUBE:
			return NewYoutubeDLSource(*u)
		case provider.SOUNDCLOUD:
			return NewYoutubeDLSource(*u)
		case provider.SPOTIFY:
			return NewSpotDLSource(*u)
		case provider.DEEZER:
			return NewFreyrSource(*u)
		case provider.APPLE_MUSIC:
			return NewFreyrSource(*u)
		case provider.MIXCLOUD:
			return NewYoutubeDLSource(*u)
		}

		return nil, fmt.Errorf("did not match any known source [youtube.com, soundcloud.com, spotify.com, apple.com, deezer.com, mixcloud.com]")

	}
}
