package source

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"station/config"
	"station/utils"
	"testing"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

func setup() func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	// copy files over for the local source test

	src := filepath.Join(Basepath, "../files/fallback.ogg")
	dst := filepath.Join(os.TempDir(), "fallback.ogg")
	err := utils.CopyFile(src, dst)
	if err != nil {
		panic(err)
	}

	return func(t *testing.T) {
		utils.CleanupDirFiles(filepath.Join(config.Station().BaseDir, "copy-yt-dl"))
		utils.CleanupDirFiles(filepath.Join(config.Station().BaseDir, "copy-freyr"))
		utils.CleanupDirFiles(filepath.Join(config.Station().BaseDir, "copy-spotdl"))
	}
}

func TestNewSource(t *testing.T) {
	teardown := setup()
	defer teardown(t)

	type args struct {
		rawUrl string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "malformed",
			args: args{
				rawUrl: "WtBo1KK2K7RG",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "local file",
			args: args{
				rawUrl: filepath.Join(os.TempDir(), "fallback.ogg"),
			},
			want:    fmt.Sprintf("file://%s/%s", os.TempDir(), "fallback.ogg"),
			wantErr: false,
		},
		{
			name: "youtube basic",
			args: args{
				rawUrl: "https://www.youtube.com/playlist?list=PLtP8IJbMRbLwJwxyaCUaUWtBo1KK2K7RG",
			},
			want:    "https://www.youtube.com/playlist?list=PLtP8IJbMRbLwJwxyaCUaUWtBo1KK2K7RG",
			wantErr: false,
		},
		{
			name: "youtu.be shortened",
			args: args{
				rawUrl: "https://www.youtu.be/playlist?list=PLtP8IJbMRbLwJwxyaCUaUWtBo1KK2K7RG",
			},
			want:    "https://www.youtu.be/playlist?list=PLtP8IJbMRbLwJwxyaCUaUWtBo1KK2K7RG",
			wantErr: false,
		},
		{
			name: "youtube fake",
			args: args{
				rawUrl: "https://www.youtube.fake/playlist?list=PLtP8IJbMRbLwJwxyaCUaUWtBo1KK2K7RG",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "soundcloud basic",
			args: args{
				rawUrl: "https://soundcloud.com/pierrepierre/sets/weird",
			},
			want:    "https://soundcloud.com/pierrepierre/sets/weird",
			wantErr: false,
		},
		{
			name: "soundcloud  short",
			args: args{
				rawUrl: "https://on.soundcloud.com/CC8hK",
			},
			want:    "https://on.soundcloud.com/CC8hK",
			wantErr: false,
		},
		{
			name: "soundcloud fake",
			args: args{
				rawUrl: "https://soundcloud.other.link/SNokEEDVeWjPAigv6",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "spotify basic",
			args: args{
				rawUrl: "https://open.spotify.com/playlist/6wDIsRymCUKUbIUa1zztWH?si=17d62593e7eb4099",
			},
			want:    "https://open.spotify.com/playlist/6wDIsRymCUKUbIUa1zztWH?si=17d62593e7eb4099",
			wantErr: false,
		},
		{
			name: "spotify  short",
			args: args{
				rawUrl: "https://open.spotify.com/playlist/6wDIsRymCUKUbIUa1zztWH?si=17d62593e7eb4099",
			},
			want:    "https://open.spotify.com/playlist/6wDIsRymCUKUbIUa1zztWH?si=17d62593e7eb4099",
			wantErr: false,
		},
		{
			name: "spotify fake",
			args: args{
				rawUrl: "https://spotify.other.link/SNokEEDVeWjPAigv6",
			},
			want:    "",
			wantErr: true,
		},
		{
			name: "deezer basic",
			args: args{
				rawUrl: "https://www.deezer.com/fr/playlist/9435718462?deferredFl=1",
			},
			want:    "https://www.deezer.com/fr/playlist/9435718462?deferredFl=1",
			wantErr: false,
		},
		{
			name: "deezer  short",
			args: args{
				rawUrl: "https://deezer.page.link/SNokEEDVeWjPAigv6",
			},
			want:    "https://deezer.page.link/SNokEEDVeWjPAigv6",
			wantErr: false,
		},
		{
			name: "deezer fake",
			args: args{
				rawUrl: "https://deezer.other.link/SNokEEDVeWjPAigv6",
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.rawUrl)
			if tt.wantErr {
				require.NotNil(t, err, fmt.Sprintf("[%s] required an error", tt.name))
			} else {
				url := got.URL()
				assert.Equal(t, tt.want, url.String(), fmt.Sprintf("New() = %v, want %v", got, tt.want))
			}
		})
	}
}
