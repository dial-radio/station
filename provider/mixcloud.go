package provider

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"time"
)

type MixcloudProvider struct {
	url url.URL
}

func NewMixcloudProvider(url url.URL) (MixcloudProvider, error) {
	p := MixcloudProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return MixcloudProvider{url: cleaned}, nil
}

func (p MixcloudProvider) URL() url.URL {
	return p.url
}

func (p MixcloudProvider) Validate() (url.URL, error) {
	// find the username, sets, playlistname
	re := regexp.MustCompile(`\/(.+)\/(.+)`)
	matches := re.FindSubmatch([]byte(p.url.Path))

	if len(matches) < 3 {
		return p.url, fmt.Errorf("the provided URL does not seem to be a mixcloud playlist: %s", p.url.String())
	}
	user := string(matches[1])
	show := string(matches[2])
	cleaned, err := url.Parse(fmt.Sprintf("https://www.mixcloud.com/%s/%s", user, show))
	p.url = *cleaned
	return p.url, err
}

func (p MixcloudProvider) Duration() (time.Duration, error) {
	apiUrl := p.url
	apiUrl.Host = "api.mixcloud.com"
	resp, err := http.Get(apiUrl.String())
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	var item struct {
		AudioLength int `json:"audio_length"`
	}

	err = json.Unmarshal(body, &item)
	if err != nil {
		panic(err)
	}

	return time.ParseDuration(fmt.Sprintf("%ds", item.AudioLength))
}
