package provider

import (
	"net/url"
	"station/config"
	"testing"
	"time"

	"github.com/spf13/viper"
)

func TestDeezerDuration(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))
	expectedDuration, _ := time.ParseDuration("1h09m16s")
	targetUrl, _ := url.Parse("https://www.deezer.com/us/playlist/12807864201")
	type fields struct {
		p DeezerProvider
	}
	tests := []struct {
		name    string
		fields  fields
		want    time.Duration
		wantErr bool
	}{
		{
			name: "minimal playlist",
			fields: fields{
				p: DeezerProvider{
					url: *targetUrl,
				},
			},
			want:    expectedDuration,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.p.Duration()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetDeezerDuration() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetDeezerDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDeezerProvider_Validate(t *testing.T) {
	targetUrl, _ := url.Parse("https://deezer.com/playlist/9435718462")
	correctUrl, _ := url.Parse("https://www.deezer.com/us/playlist/9435718462?si=N5XvD7WcQ_C8KGZ7I8BvbQ")
	wrongUrlPath, _ := url.Parse("https://www.deezer.com/us/playlists/9435718462")

	type fields struct {
		url url.URL
	}
	tests := []struct {
		name    string
		fields  fields
		want    url.URL
		wantErr bool
	}{
		{
			name: "should validate playlist",
			fields: fields{
				url: *correctUrl,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should fail playlist on path",
			fields: fields{
				url: *wrongUrlPath,
			},
			want:    *wrongUrlPath,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := DeezerProvider{
				url: tt.fields.url,
			}
			got, err := p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("DeezerProvider.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.want != got {
				t.Errorf("DeezerProvider.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
