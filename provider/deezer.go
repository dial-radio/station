package provider

import (
	"encoding/json"
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/gocolly/colly/v2"
	"github.com/rs/zerolog/log"
)

type DeezerProvider struct {
	url url.URL
}

type DeezerPlaylist struct {
	Data struct {
		Duration int `json:"DURATION"`
	} `json:"DATA"`
}

func NewDeezerProvider(url url.URL) (DeezerProvider, error) {
	p := DeezerProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return DeezerProvider{url: cleaned}, nil
}

func (p DeezerProvider) URL() url.URL {
	return p.url
}

func (p DeezerProvider) Validate() (url.URL, error) {
	// need to find the country, playlist, id paths
	re := regexp.MustCompile(`(\/[a-z][a-z])?(\/playlist\/)(\d+)`)
	matches := re.FindSubmatch([]byte(p.url.Path))

	if len(matches) < 2 {
		return p.url, fmt.Errorf("the provided URL does not seem to be a deezer playlist: %s", p.url.String())
	}
	id := string(matches[len(matches)-1])
	cleaned, err := url.Parse(fmt.Sprintf("https://deezer.com/playlist/%s", id))
	p.url = *cleaned
	return p.url, err
}

// getDeezerDuration scrapes the HTML returned from the playlist duration, and marshals the window.data object to find the duration
func (p DeezerProvider) Duration() (time.Duration, error) {
	var d time.Duration

	c := colly.NewCollector()
	c.OnHTML("body", func(e *colly.HTMLElement) {

		scripts := e.ChildTexts("script")
		raw := strings.Split(scripts[0], " = ")

		var pl DeezerPlaylist
		err := json.Unmarshal([]byte(raw[1]), &pl)
		if err != nil {
			panic(err)
		}

		d, err = time.ParseDuration(fmt.Sprintf("%ds", pl.Data.Duration))
		if err != nil {
			log.Error().Err(err).Int("raw", pl.Data.Duration).Msg("[DeezerDuration] error parsing duration")
		}
	})

	c.OnError(func(r *colly.Response, err error) {
		log.Error().Err(err).Msg("[DeezerDuration]")
	})

	c.Visit(p.url.String())

	return d, nil
}
