package provider

import (
	"fmt"
	"net/url"
	"os"
	"path/filepath"
	"station/utils"
	"time"

	"github.com/rs/zerolog/log"
)

type LocalProvider struct {
	url url.URL
}

func NewLocalProvider(url url.URL) (LocalProvider, error) {
	p := LocalProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return LocalProvider{url: cleaned}, nil
}

func (p LocalProvider) URL() url.URL {
	return p.url
}

func (p LocalProvider) Validate() (url.URL, error) {
	c := filepath.Clean(p.url.String())
	if c == "" {
		log.Warn().Msg("[Download] file does not exist")
		return p.url, fmt.Errorf("[Download] file does not exist")
	}

	u, err := url.Parse(c)
	p.url = *u
	return p.url, err
}

func (p LocalProvider) Duration() (time.Duration, error) {
	var duration time.Duration
	fileinfo, err := os.Stat(p.url.String())
	if err != nil {
		log.Warn().Msg("[Download] file does not exist")
		return 0.0, err
	}

	if fileinfo.IsDir() {
		entries, err := os.ReadDir(p.url.String())
		if err != nil {
			return 0.0, err
		}
		for _, ent := range entries {
			if !ent.IsDir() {
				dur, err := utils.GetAudioFileDuration(filepath.Join(p.url.String(), ent.Name()))
				if err != nil {
					return duration, err
				}
				duration += dur
			}
		}

	} else {
		duration, err = utils.GetAudioFileDuration(p.url.String())
		if err != nil {
			return 0.0, err
		}
	}

	return duration, nil
}
