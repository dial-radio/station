package provider

import (
	"net/url"
	"station/config"
	"testing"
	"time"

	"github.com/spf13/viper"
)

func TestSoundCloudDuration(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	expectedDuration, _ := time.ParseDuration("14m0.803s")
	targetUrl, _ := url.Parse("https://soundcloud.com/pierrepierre/sets/weird")

	type fields struct {
		p SoundcloudProvider
	}
	tests := []struct {
		name    string
		fields  fields
		want    time.Duration
		wantErr bool
	}{
		{
			name: "minimal playlist",
			fields: fields{
				p: SoundcloudProvider{
					url: *targetUrl,
				},
			},
			want:    expectedDuration,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.p.Duration()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetSoundCloudDuration() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetSoundCloudDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSoundcloudProvider_Validate(t *testing.T) {
	targetUrl, _ := url.Parse("https://soundcloud.com/pierrepierre/sets/weird")
	correctUrl, _ := url.Parse("https://soundcloud.com/pierrepierre/sets/weird?si=N5XvD7WcQ_C8KGZ7I8BvbQ")
	wrongUrlPath, _ := url.Parse("https://soundcloud.com/pierrepierre/playlists/weird")

	type fields struct {
		url url.URL
	}
	tests := []struct {
		name    string
		fields  fields
		want    url.URL
		wantErr bool
	}{
		{
			name: "should validate playlist",
			fields: fields{
				url: *correctUrl,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should fail playlist on path",
			fields: fields{
				url: *wrongUrlPath,
			},
			want:    *wrongUrlPath,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := SoundcloudProvider{
				url: tt.fields.url,
			}
			got, err := p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("SoundcloudProvider.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.want != got {
				t.Errorf("SoundcloudProvider.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
