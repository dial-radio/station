package provider

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"station/config"
	"strings"
	"time"

	"github.com/gocolly/colly/v2"
	"github.com/rs/zerolog/log"
)

type SpotifyProvider struct {
	url url.URL
}

type SpotifyTracks struct {
	Next  string `json:"next"`
	Items []struct {
		Track struct {
			DurationMS int    `json:"duration_ms"`
			Name       string `json:"name"`
		} `json:"track"`
	} `json:"items"`
}

func NewSpotifyProvider(url url.URL) (SpotifyProvider, error) {
	p := SpotifyProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return SpotifyProvider{url: cleaned}, nil
}

func (p SpotifyProvider) URL() url.URL {
	return p.url
}

func (p SpotifyProvider) Validate() (url.URL, error) {

	re := regexp.MustCompile(`(spotify\.com\/playlist\/|spotify\.link\/)(\w+)`)
	matches := re.FindSubmatch([]byte(p.url.String()))

	if len(matches) < 2 {
		return p.url, fmt.Errorf("the provided URL does not seem to be a spotify playlist: %s", p.url.String())
	}

	if strings.Contains(p.url.Host, "spotify.link") {
		var rawUrl string
		c := colly.NewCollector()
		c.OnHTML(".secondary-action", func(e *colly.HTMLElement) {
			rawUrl = e.Attr("href")
		})

		c.OnError(func(r *colly.Response, err error) {
			log.Error().Err(err).Msg("[Validate] spotify.link")
		})

		c.Visit(p.url.String())

		cleaned, err := url.Parse(rawUrl)
		if err != nil {
			log.Error().Err(err).Msg("[Validate] spotify.link")
			return p.url, err
		}
		cleaned.RawQuery = ""
		p.url = *cleaned

		return p.url, nil
	}

	id := string(matches[len(matches)-1])
	cleaned, err := url.Parse(fmt.Sprintf("https://open.spotify.com/playlist/%s", id))
	p.url = *cleaned
	return p.url, err
}

func (p SpotifyProvider) Duration() (time.Duration, error) {
	var totalDuration time.Duration

	if strings.Contains(p.url.Host, "spotify.link") {
		var rawUrl string
		c := colly.NewCollector()
		c.OnHTML(".secondary-action", func(e *colly.HTMLElement) {
			rawUrl = e.Attr("href")
		})
		c.OnError(func(r *colly.Response, err error) {
			log.Error().Err(err).Msg("[Validate] spotify.link")
		})
		c.Visit(p.url.String())

		cleaned, err := url.Parse(rawUrl)
		if err != nil {
			log.Error().Err(err).Msg("[Validate] spotify.link")
			return 0, err
		}
		cleaned.RawQuery = ""
		p.url = *cleaned
	}

	paths := strings.Split(p.url.Path, "/")
	playlist_id := paths[len(paths)-1]

	tracks, err := getAllSpotifyTracks(playlist_id)
	if err != nil {
		log.Error().Err(err).Msg("[Duration] failed to get spotify playlist")
		return 0, err
	}

	for _, item := range tracks.Items {
		d, err := time.ParseDuration(fmt.Sprintf("%dms", item.Track.DurationMS))
		if err != nil {
			log.Warn().Int("raw", item.Track.DurationMS).Msg("[Duration] invalid parsing")
		} else {
			totalDuration += d
		}
	}

	return totalDuration, nil
}

// getSpotifyTracks takes a playlist ID and returns a playlist with all tracks
func getAllSpotifyTracks(id string) (SpotifyTracks, error) {
	if config.Conf().SpotifyClientID == "" {
		return SpotifyTracks{}, fmt.Errorf("missing spotify credentials (id)")
	}

	if config.Conf().SpotifyClientSecret == "" {
		return SpotifyTracks{}, fmt.Errorf("missing spotify credentials (secrets)")
	}

	//first get the access token
	form := url.Values{}
	form.Add("grant_type", "client_credentials")
	form.Add("client_id", config.Conf().SpotifyClientID)
	form.Add("client_secret", config.Conf().SpotifyClientSecret)
	access_endpoint := &url.URL{
		Scheme: "https",
		Host:   "accounts.spotify.com",
		Path:   "api/token",
	}
	resp, err := http.Post(access_endpoint.String(), "application/x-www-form-urlencoded", strings.NewReader(form.Encode()))
	if err != nil {
		return SpotifyTracks{}, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)

	if resp.StatusCode != http.StatusOK {
		fmt.Println(string(body))
		return SpotifyTracks{}, fmt.Errorf("could not get auth key")
	}

	if err != nil {
		return SpotifyTracks{}, err
	}

	type AccessTokenResponse struct {
		Token string `json:"access_token"`
	}
	var access AccessTokenResponse

	err = json.Unmarshal(body, &access)
	if err != nil {
		return SpotifyTracks{}, err
	}

	ep := &url.URL{
		Scheme: "https",
		Host:   "api.spotify.com",
		Path:   fmt.Sprintf("v1/playlists/%s/tracks", id),
	}
	playlist, err := getSpotifyTracks(ep.String(), access.Token)

	return playlist, err
}

// getSpotifyTracks takes a url and returns a playlist with the current page of tracks
func getSpotifyTracks(endpoint string, token string) (SpotifyTracks, error) {
	var tracks SpotifyTracks

	client := &http.Client{}
	ep, err := url.Parse(endpoint)
	if err != nil {
		return tracks, err
	}

	req, err := http.NewRequest("GET", ep.String(), nil)
	if err != nil {
		return tracks, err
	}
	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	q := req.URL.Query()
	if !q.Has("fields") {
		q.Add("fields", "items.track.duration_ms,next")
	}
	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)
	if err != nil {
		return tracks, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != http.StatusOK {
		return tracks, fmt.Errorf("could not find tracks resource")
	}
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return tracks, err
	}

	err = json.Unmarshal(body, &tracks)
	if err != nil {
		return tracks, err
	}

	if tracks.Next != "" {
		// here, do another request
		additional, err := getSpotifyTracks(tracks.Next, token)
		if err != nil {
			return tracks, err
		}

		tracks.Items = append(tracks.Items, additional.Items...)
	}

	return tracks, nil
}
