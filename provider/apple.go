package provider

import (
	"fmt"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/gocolly/colly/v2"
	"github.com/rs/zerolog/log"
)

type AppleMusicProvider struct {
	url url.URL
}

func NewAppleMusicProvider(url url.URL) (AppleMusicProvider, error) {
	p := AppleMusicProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return AppleMusicProvider{url: cleaned}, nil
}

func (p AppleMusicProvider) URL() url.URL {
	return p.url
}

func (p AppleMusicProvider) Validate() (url.URL, error) {
	// need to find the lang, playlist, name, id path
	re := regexp.MustCompile(`(\/[a-z][a-z])?(\/playlist\/)(.+)(\/)(.+)`)
	matches := re.FindSubmatch([]byte(p.url.Path))

	if len(matches) < 4 {
		return p.url, fmt.Errorf("the provided URL does not seem to be an apple music playlist: %s", p.url.String())
	}
	id := string(matches[len(matches)-1])
	title := string(matches[len(matches)-3])
	cleaned, err := url.Parse(fmt.Sprintf("https://music.apple.com/playlist/%s/%s", title, id))
	p.url = *cleaned
	return p.url, err
}

// getAppleMusic scrapes the HTML returned from the playlist duration, and marshals the window.data object to find the duration
func (p AppleMusicProvider) Duration() (time.Duration, error) {
	var d time.Duration
	var err error

	c := colly.NewCollector()
	c.OnHTML(".description", func(e *colly.HTMLElement) {
		cleaned := strings.ReplaceAll(e.Text, "\u00a0", " ")
		re := regexp.MustCompile(`(\d+)`)
		// extract all groups of digits with a regular expression
		raw_time := re.FindAllString(cleaned, -1)
		var hours, minutes string
		if len(raw_time) == 3 {
			hours = raw_time[1]
			minutes = raw_time[2]
		} else if len(raw_time) == 2 {
			if strings.Contains(cleaned, "minute") {
				hours = "0"
				minutes = raw_time[1]
			} else if strings.Contains(cleaned, "hour") {
				hours = raw_time[1]
				minutes = "0"
			}
		}

		d, err = time.ParseDuration(fmt.Sprintf("%sh%sm", hours, minutes))
		if err != nil {
			log.Error().Err(err).Str("raw", cleaned).Msg("[getAppleDuration] error parsing duration")
		}
	})

	c.OnError(func(r *colly.Response, err error) {
		fmt.Println(err)
	})

	c.Visit(p.url.String())

	return d, nil
}
