# Providers

This packages takes care of managing the different playlist providers. Unlike sources, these are usually platforms that listeners interact with in order to constitute their playlists. Each provider needs to be able to implement the generic `Provider` interface:

```go
// a Provider is an interface for handling the idiosyncracies of the streaming providers
type Provider interface {
  Duration() (time.Duration, error)
  // Validate checks that it is indeed a playlist for the given provider, and then returns a cleaned URL
  Validate() (url.URL, error)
  URL() url.URL
}
```

## Current providers

- Youtube
- Spotify
- Apple Music
- Deezer
- Mixcloud
- Local files
