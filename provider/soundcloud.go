package provider

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"station/config"
	"time"
)

type SoundcloudProvider struct {
	url url.URL
}

func NewSoundcloudProvider(url url.URL) (SoundcloudProvider, error) {
	p := SoundcloudProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return SoundcloudProvider{url: cleaned}, nil
}

func (p SoundcloudProvider) URL() url.URL {
	return p.url
}

func (p SoundcloudProvider) Validate() (url.URL, error) {
	// find the username, sets, playlistname
	re := regexp.MustCompile(`\/(.+)(\/sets\/)(.+)`)
	matches := re.FindSubmatch([]byte(p.url.Path))

	if len(matches) < 4 {
		return p.url, fmt.Errorf("the provided URL does not seem to be a soundcloud playlist: %s", p.url.String())
	}
	id := string(matches[3])
	user := string(matches[1])
	cleaned, err := url.Parse(fmt.Sprintf("https://soundcloud.com/%s/sets/%s", user, id))
	p.url = *cleaned
	return p.url, err
}

func (p SoundcloudProvider) Duration() (time.Duration, error) {
	if config.Conf().SoundcloudClientID == "" {
		return 0, fmt.Errorf("missing soundcloud credentials")
	}
	// get playlist ID
	resp, err := http.Get(p.url.String())
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}
	re := regexp.MustCompile(`(https:\/\/api)(-v2)?(.soundcloud.com\/playlists\/)(\d+)`)
	matches := re.FindSubmatch(body)
	if len(matches) == 0 {
		return 0, fmt.Errorf("the provided URL does not seem to be a soundcloud playlist")
	}
	id := string(matches[len(matches)-1])

	query := &url.Values{}
	query.Add("representation", "full")
	query.Add("client_id", config.Conf().SoundcloudClientID)
	ep := &url.URL{
		Scheme:   "https",
		Host:     "api-v2.soundcloud.com",
		Path:     fmt.Sprintf("playlists/%s", id),
		RawQuery: query.Encode(),
	}

	resp, err = http.Get(ep.String())
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err = io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}

	var item struct {
		Duration int `json:"duration"`
	}

	err = json.Unmarshal(body, &item)
	if err != nil {
		panic(err)
	}

	return time.ParseDuration(fmt.Sprintf("%dms", item.Duration))
}
