package provider

import (
	"fmt"
	"net/url"
	"path/filepath"
	"regexp"
	"time"

	"github.com/rs/zerolog/log"
)

// a Provider is an interface for handling the idiosyncracies of the streaming providers
type Provider interface {
	Duration() (time.Duration, error)
	// Validate checks that it is indeed a playlist for the given provider, and then returns a cleaned URL
	Validate() (url.URL, error)
	URL() url.URL
}

const (
	LOCAL       = "local"
	YOUTUBE     = "youtube"
	SOUNDCLOUD  = "soundcloud"
	SPOTIFY     = "spotify"
	DEEZER      = "deezer"
	APPLE_MUSIC = "apple music"
	MIXCLOUD    = "mixcloud"

	YOUTUBE_REGEX     = `([\w]+\.)*(youtu\.be|youtube\.[a-z][a-z][a-z]?|youtube-nocookie\.[a-z][a-z][a-z]?)$`
	SOUNDCLOUD_REGEX  = `(^soundcloud.[a-z][a-z][a-z]?)$|(on\.soundcloud\.[a-z][a-z][a-z]?)$`
	SPOTIFY_REGEX     = `(open\.)(spotify\.com)$`
	DEEZER_REGEX      = `(on.)?(deezer\.(page\.link|com))$`
	APPLE_MUSIC_REGEX = `(music)\.(apple)\.(com)$`
	MIXCLOUD_REGEX    = `([\w]+\.)*(mixcloud.com)$`
)

// New returns the appropriate Source based on a regex match of the rawURL's Hostname()
func New(rawUrl string) (Provider, error) {
	u, err := url.ParseRequestURI(rawUrl)
	if err != nil {
		return nil, err
	}

	if filepath.IsAbs(rawUrl) { // local path
		u, err := url.Parse(rawUrl)
		if err != nil {
			return nil, err
		}
		return NewLocalProvider(*u)
	} else { // remote path
		p, err := Identify(*u)
		if err != nil {
			log.Error().Err(err).Msg("[NewSource]")
		}

		switch p {
		case YOUTUBE:
			return NewYoutubeProvider(*u)
		case SOUNDCLOUD:
			return NewSoundcloudProvider(*u)
		case SPOTIFY:
			return NewSpotifyProvider(*u)
		case DEEZER:
			return NewDeezerProvider(*u)
		case APPLE_MUSIC:
			return NewAppleMusicProvider(*u)
		case MIXCLOUD:
			return NewMixcloudProvider(*u)
		}

		return nil, fmt.Errorf("did not match any known source [youtube.com, soundcloud.com, spotify.com, apple.com, deezer.com, mixcloud.com]")

	}
}

// Identify returns the provider type based on the URL
func Identify(u url.URL) (string, error) {
	h := u.Hostname()

	re := regexp.MustCompile(YOUTUBE_REGEX)
	if re.FindString(h) != "" {
		return YOUTUBE, nil
	}

	re = regexp.MustCompile(SOUNDCLOUD_REGEX)
	if re.FindString(h) != "" {
		return SOUNDCLOUD, nil
	}

	re = regexp.MustCompile(SPOTIFY_REGEX)
	if re.FindString(h) != "" {
		return SPOTIFY, nil
	}

	re = regexp.MustCompile(DEEZER_REGEX)
	if re.FindString(h) != "" {
		return DEEZER, nil
	}

	re = regexp.MustCompile(APPLE_MUSIC_REGEX)
	if re.FindString(h) != "" {
		return APPLE_MUSIC, nil
	}

	re = regexp.MustCompile(MIXCLOUD_REGEX)
	if re.FindString(h) != "" {
		return MIXCLOUD, nil
	}

	return "", fmt.Errorf("no providers matched")
}

// Compare takes two URLs and returns whether they point to the same playlist
func Compare(u1 string, u2 string) (bool, error) {
	p, err := New(u1)
	if err != nil {
		return false, err
	}

	o, err := New(u2)
	if err != nil {
		return false, err
	}

	if o.URL() == p.URL() {
		return true, nil
	}

	return false, nil
}
