package provider

import (
	"net/url"
	"path/filepath"
	"station/config"
	"station/utils"
	"testing"
	"time"

	"github.com/spf13/viper"
)

func TestLocalDuration(t *testing.T) {
	conf, err := config.Init(viper.GetString("STATION_CONFIG_PATH"))
	if err != nil {
		panic(err)
	}
	var testLocalPath = filepath.Join(conf.Station.BaseDir, "fallback.ogg")
	if err := utils.CopyFile(filepath.Join(Basepath, "../files/fallback.ogg"), testLocalPath); err != nil {
		t.Errorf("could not copy the test file: %s", err)
	}
	expectedDuration, _ := time.ParseDuration("1m17.090227s")
	targetUrl, _ := url.Parse(testLocalPath)

	type fields struct {
		p LocalProvider
	}
	tests := []struct {
		name    string
		fields  fields
		want    time.Duration
		wantErr bool
	}{
		{
			name: "local file",
			fields: fields{
				p: LocalProvider{
					url: *targetUrl,
				},
			},
			want:    expectedDuration,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.p.Duration()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetLocalDuration() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetLocalDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}
func TestLocalProvider_Validate(t *testing.T) {
	targetUrl, _ := url.Parse("/tmp")
	correctUrl, _ := url.Parse("/tmp")
	wrongUrlPath, _ := url.Parse("/tmp?si=N5XvD7WcQ_C8KGZ7I8BvbQ")

	type fields struct {
		url url.URL
	}
	tests := []struct {
		name    string
		fields  fields
		want    url.URL
		wantErr bool
	}{
		{
			name: "should validate playlist",
			fields: fields{
				url: *correctUrl,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should not fail playlist on path",
			fields: fields{
				url: *wrongUrlPath,
			},
			want:    *wrongUrlPath,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := LocalProvider{
				url: tt.fields.url,
			}
			got, err := p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("LocalProvider.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.want != got {
				t.Errorf("LocalProvider.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
