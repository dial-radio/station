package provider

import (
	"net/url"
	"path/filepath"
	"runtime"
	"station/config"
	"testing"
	"time"

	"github.com/spf13/viper"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

func TestYoutubeDuration(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	expectedDurationShort, _ := time.ParseDuration("5m55s")
	targetUrlShort, _ := url.Parse("https://www.youtube.com/playlist?list=PLyQtZRmg0ogcmftmAcCLwgpFFDNxG9xI-")

	expectedDurationLong, _ := time.ParseDuration("4h38m3s")
	targetUrlLong, _ := url.Parse("https://www.youtube.com/playlist?list=PLyQtZRmg0ogfB3E4AuJ2iuvc7FXg0meDl")

	type fields struct {
		p YoutubeProvider
	}
	tests := []struct {
		name    string
		fields  fields
		want    time.Duration
		wantErr bool
	}{
		{
			name: "minimal playlist",
			fields: fields{
				p: YoutubeProvider{
					url: *targetUrlShort,
				},
			},
			want:    expectedDurationShort,
			wantErr: false,
		},
		{
			name: "maximal playlist",
			fields: fields{
				p: YoutubeProvider{
					url: *targetUrlLong,
				},
			},
			want:    expectedDurationLong,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.p.Duration()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetYoutubeDuration() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetYoutubeDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestYoutubeProvider_Validate(t *testing.T) {
	targetUrl, _ := url.Parse("https://youtube.com/playlist?list=PLyQtZRmg0ogcmftmAcCLwgpFFDNxG9xI-")
	wrongUrlQuery, _ := url.Parse("https://youtube.com/playlist?playlist=PLyQtZRmg0ogcmftmAcCLwgpFFDNxG9xI-")
	wrongUrlPath, _ := url.Parse("https://youtube.com/playlists?list=PLyQtZRmg0ogcmftmAcCLwgpFFDNxG9xI-")

	type fields struct {
		url url.URL
	}
	tests := []struct {
		name    string
		fields  fields
		want    url.URL
		wantErr bool
	}{
		{
			name: "should validate playlist",
			fields: fields{
				url: *targetUrl,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should fail playlist on query",
			fields: fields{
				url: *wrongUrlQuery,
			},
			want:    *wrongUrlQuery,
			wantErr: true,
		},
		{
			name: "should fail playlist on path",
			fields: fields{
				url: *wrongUrlPath,
			},
			want:    *wrongUrlPath,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := YoutubeProvider{
				url: tt.fields.url,
			}
			got, err := p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("YoutubeProvider.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.want != got {
				t.Errorf("YoutubeProvider.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
