package provider

import (
	"net/url"
	"station/config"
	"testing"
	"time"

	"github.com/spf13/viper"
)

const spotifyPlaylist = "https://open.spotify.com/playlist/3wdnljwPkhfRpIZCyyyzBc?si=N5XvD7WcQ_C8KGZ7I8BvbQ"
const spotifyPlaylistShort = "https://spotify.link/alWo6b7P9Db"

func TestSpotifyDuration(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	expectedDuration, _ := time.ParseDuration("9m59s716ms")
	targetUrl, _ := url.Parse(spotifyPlaylist)
	targetUrlShort, _ := url.Parse(spotifyPlaylistShort)
	expectedDurationShort, _ := time.ParseDuration("1h0m6.785s")
	targetUrlLong, _ := url.Parse("https://open.spotify.com/playlist/6lfFNi4VUAwpVbceIY2SRL")
	expectedDurationLong, _ := time.ParseDuration("10h24m34.635s")

	type fields struct {
		p SpotifyProvider
	}
	tests := []struct {
		name    string
		fields  fields
		want    time.Duration
		wantErr bool
	}{
		{
			name: "minimal playlist",
			fields: fields{
				p: SpotifyProvider{
					url: *targetUrl,
				},
			},
			want:    expectedDuration,
			wantErr: false,
		},
		{
			name: "minimal playlist short",
			fields: fields{
				p: SpotifyProvider{
					url: *targetUrlShort,
				},
			},
			want:    expectedDurationShort,
			wantErr: false,
		},
		{
			name: "playlist too long",
			fields: fields{
				p: SpotifyProvider{
					url: *targetUrlLong,
				},
			},
			want:    expectedDurationLong,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.p.Duration()
			if (err != nil) != tt.wantErr {
				t.Errorf("SpotifyDuration() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("SpotifyDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSpotifyProvider_Validate(t *testing.T) {
	targetUrl, _ := url.Parse("https://open.spotify.com/playlist/3wdnljwPkhfRpIZCyyyzBc")
	correctUrl, _ := url.Parse("https://open.spotify.com/playlist/3wdnljwPkhfRpIZCyyyzBc?si=N5XvD7WcQ_C8KGZ7I8BvbQ")
	shortUrl, _ := url.Parse("https://spotify.link/alWo6b7P9Db")
	correctUrlShort, _ := url.Parse("https://open.spotify.com/playlist/6bgxFmiFXOi9EXSV3OmVbp")
	wrongUrlPath, _ := url.Parse("https://open.spotify.com/playlists/3wdnljwPkhfRpIZCyyyzBc?si=N5XvD7WcQ_C8KGZ7I8BvbQ")

	type fields struct {
		url url.URL
	}
	tests := []struct {
		name    string
		fields  fields
		want    url.URL
		wantErr bool
	}{
		{
			name: "should validate playlist",
			fields: fields{
				url: *correctUrl,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should validate short playlist",
			fields: fields{
				url: *shortUrl,
			},
			want:    *correctUrlShort,
			wantErr: false,
		},
		{
			name: "should fail playlist on path",
			fields: fields{
				url: *wrongUrlPath,
			},
			want:    *wrongUrlPath,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := SpotifyProvider{
				url: tt.fields.url,
			}
			got, err := p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("SpotifyProvider.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.want != got {
				t.Errorf("SpotifyProvider.Validate() = %v, want %v", got, tt.want)
			}
		})
	}
}
