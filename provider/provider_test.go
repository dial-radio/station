package provider

import (
	"net/url"
	"reflect"
	"testing"
)

func TestCompare(t *testing.T) {
	type args struct {
		u1 string
		u2 string
	}
	tests := []struct {
		name    string
		args    args
		want    bool
		wantErr bool
	}{
		{
			name: "similar urls",
			args: args{
				u1: "https://youtube.com/playlist?list=screamadelica&baby=yes",
				u2: "https://youtube.com/playlist?list=screamadelica",
			},
			want:    true,
			wantErr: false,
		},
		{
			name: "different urls",
			args: args{
				u1: "https://youtube.com/playlist?list=primal&baby=yes",
				u2: "https://youtube.com/playlist?list=scream",
			},
			want:    false,
			wantErr: false,
		},
		{
			name: "broken urls",
			args: args{
				u1: "https://youtube.com/playlists?list=primal&baby=yes",
				u2: "https://youtube.com/playlist?list=scream",
			},
			want:    false,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Compare(tt.args.u1, tt.args.u2)
			if (err != nil) != tt.wantErr {
				t.Errorf("Compare() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Compare() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		rawUrl string
	}

	mixcloudUrl, _ := url.Parse("https://www.mixcloud.com/periode/regular-expressions-043/")
	tests := []struct {
		name    string
		args    args
		want    Provider
		wantErr bool
	}{
		{
			name: "mixcloud valid",
			args: args{
				rawUrl: mixcloudUrl.String(),
			},
			want:    MixcloudProvider{url: *mixcloudUrl},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.rawUrl)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}
