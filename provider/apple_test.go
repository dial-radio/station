package provider

import (
	"net/url"
	"testing"
	"time"
)

func Test_getAppleDuration(t *testing.T) {
	expectedDuration, _ := time.ParseDuration("1h2m0s")
	targetUrl, _ := url.Parse("https://music.apple.com/fr/playlist/fantastic-ballads/pl.u-zPyLmj6TZyGXZ1E?l=en-GB")

	type fields struct {
		p AppleMusicProvider
	}
	tests := []struct {
		name    string
		fields  fields
		want    time.Duration
		wantErr bool
	}{
		{
			name: "minimal playlist",
			fields: fields{
				p: AppleMusicProvider{
					url: *targetUrl,
				},
			},
			want:    expectedDuration,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.fields.p.Duration()
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAppleDuration() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetAppleDuration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAppleMusicProvider_Validate(t *testing.T) {
	targetUrl, _ := url.Parse("https://music.apple.com/playlist/fantastic-ballads/pl.u-zPyLmj6TZyGXZ1E")
	correctUrl, _ := url.Parse("https://music.apple.com/fr/playlist/fantastic-ballads/pl.u-zPyLmj6TZyGXZ1E")
	correctUrlNoLocale, _ := url.Parse("https://music.apple.com/playlist/fantastic-ballads/pl.u-zPyLmj6TZyGXZ1E?l=en-GB")
	wrongUrlPath, _ := url.Parse("https://music.apple.com/fr/playlists/fantastic-ballads/pl.u-zPyLmj6TZyGXZ1E?l=en-GB")

	type fields struct {
		url url.URL
	}
	tests := []struct {
		name    string
		fields  fields
		want    url.URL
		wantErr bool
	}{
		{
			name: "should validate playlist",
			fields: fields{
				url: *correctUrl,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should validate playlist no locale",
			fields: fields{
				url: *correctUrlNoLocale,
			},
			want:    *targetUrl,
			wantErr: false,
		},
		{
			name: "should fail playlist on path",
			fields: fields{
				url: *wrongUrlPath,
			},
			want:    *wrongUrlPath,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := AppleMusicProvider{
				url: tt.fields.url,
			}
			got, err := p.Validate()
			if (err != nil) != tt.wantErr {
				t.Errorf("AppleMusicProvider.Validate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !tt.wantErr && tt.want != got {
				t.Errorf("AppleMusicProvider.Validate() = %v, want %v", got.String(), tt.want.String())
			}
		})
	}
}
