package provider

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"regexp"
	"station/config"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

type YoutubeProvider struct {
	url url.URL
}

type YTPlaylist struct {
	NextPageToken string `json:"nextPageToken"`
	Items         []struct {
		ID             string `json:"id"`
		ContentDetails struct {
			VideoId string `json:"videoId"`
		} `json:"contentDetails"`
	}
	PageInfo struct {
		TotalResults   int `json:"totalResults"`
		ResultsPerPage int `json:"resultsPerPage"`
	}
}

type YTItem struct {
	Items []struct {
		ID             string `json:"id"`
		ContentDetails struct {
			Duration string `json:"duration"`
		} `json:"contentDetails"`
	}
}

func NewYoutubeProvider(url url.URL) (YoutubeProvider, error) {
	p := YoutubeProvider{url: url}
	cleaned, err := p.Validate()
	if err != nil {
		return p, err
	}
	return YoutubeProvider{url: cleaned}, nil
}

func (p YoutubeProvider) URL() url.URL {
	return p.url
}

func (p YoutubeProvider) Validate() (url.URL, error) {
	if p.url.Path != "/playlist" {
		return p.url, fmt.Errorf("the provided URL does not seem to be a youtube playlist: %s", p.url.String())
	}

	id := p.url.Query().Get("list")
	if id == "" {
		return p.url, fmt.Errorf("the provided URL does not seem to have a playlist ID")
	}
	cleaned, err := url.Parse(fmt.Sprintf("https://youtube.com/playlist?list=%s", id))
	return *cleaned, err
}

func (p YoutubeProvider) Duration() (time.Duration, error) {
	var d time.Duration
	query, err := url.ParseQuery(p.url.RawQuery)
	if err != nil {
		return 0, fmt.Errorf("cannot parse playlist query")
	}
	playlist_id := query.Get("list")
	if playlist_id == "" {
		return 0, fmt.Errorf("cannot find playlist id")
	}

	itemIDs, err := getYoutubePlaylistItems(playlist_id, "")
	if err != nil {
		log.Error().Err(err).Msg("[Duration] failed to get yt playlist items")
		return 0, err
	}

	for _, id := range itemIDs {
		item, err := getYoutubeItem(id)
		if err != nil {
			return 0, err
		}
		if len(item.Items) > 0 {
			iso8601 := `^PT(\d+H)?(\d+M)?(\d+S)?`
			expr := regexp.MustCompile(iso8601)
			matches := expr.FindAllStringSubmatch(item.Items[0].ContentDetails.Duration, -1)

			raw := fmt.Sprintf("%s%s%s", strings.ToLower(matches[0][1]), strings.ToLower(matches[0][2]), strings.ToLower(matches[0][3]))
			duration, err := time.ParseDuration(raw)
			if err != nil {
				return d, err
			}
			d += duration

		} else {
			log.Debug().Msg("[Duration] video not available for duration check")
		}
	}

	return d, nil
}

// getYoutubePlaylistItems takes a playlist ID and a page token and returns the resource with contentDetails of its children
func getYoutubePlaylistItems(id string, pageToken string) ([]string, error) {
	var playlist YTPlaylist
	var playlistItemIDs []string
	if config.Conf().YoutubeApiKey == "" {
		return playlistItemIDs, fmt.Errorf("missing youtube credentials")
	}

	query := &url.Values{}
	query.Add("part", "contentDetails")
	query.Add("maxResults", "50")
	if pageToken != "" {
		query.Add("pageToken", pageToken)
	}
	query.Add("playlistId", id)
	query.Add("key", config.Conf().YoutubeApiKey)
	ep := &url.URL{
		Scheme:   "https",
		Host:     "youtube.googleapis.com",
		Path:     "youtube/v3/playlistItems",
		RawQuery: query.Encode(),
	}

	resp, err := http.Get(ep.String())
	if err != nil {
		return playlistItemIDs, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return playlistItemIDs, err
	}

	err = json.Unmarshal(body, &playlist)
	if err != nil {
		return playlistItemIDs, err
	}

	for _, item := range playlist.Items {
		playlistItemIDs = append(playlistItemIDs, item.ContentDetails.VideoId)
	}

	if playlist.NextPageToken != "" {
		ids, err := getYoutubePlaylistItems(id, playlist.NextPageToken)
		if err != nil {
			return playlistItemIDs, err
		}
		playlistItemIDs = append(playlistItemIDs, ids...)
	}
	return playlistItemIDs, nil
}

// getYoutubeItem gets a single YTItem, with its duration
func getYoutubeItem(id string) (YTItem, error) {
	var item YTItem
	if config.Conf().YoutubeApiKey == "" {
		return item, fmt.Errorf("missing youtube credentials")
	}

	query := &url.Values{}
	query.Add("part", "contentDetails")
	query.Add("id", id)
	query.Add("key", config.Conf().YoutubeApiKey)
	ep := &url.URL{
		Scheme:   "https",
		Host:     "youtube.googleapis.com",
		Path:     "youtube/v3/videos",
		RawQuery: query.Encode(),
	}

	resp, err := http.Get(ep.String())
	if err != nil {
		return item, err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return item, err
	}

	err = json.Unmarshal(body, &item)
	if err != nil {
		return item, err
	}
	return item, nil
}
