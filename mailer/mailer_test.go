package mailer

import (
	"path/filepath"
	"runtime"
	"station/config"
	"testing"
	"time"

	"github.com/joho/godotenv"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

func setup(t *testing.T) func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	if viper.GetString("STATION_MAILGUN_DOMAIN") == "" {
		if err := godotenv.Load(filepath.Join(Basepath, "../.secrets")); err != nil {
			t.Log("failed to load secrets env")
		}
	}
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))
	return func(t *testing.T) {}
}

func TestSendMail(t *testing.T) {
	teardown := setup(t)
	defer teardown(t)

	now := time.Now()

	type args struct {
		dest     string
		subject  string
		template string
		payload  Payload
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "sends an email for scheduled event",
			args: args{
				dest:     config.Bots().TeamEmail,
				subject:  "testing emails",
				template: "scheduled",
				payload: ScheduledPayload{
					Name:   "test artist",
					Moment: "test moment",
					Title:  "test title",
					Date:   now.Format(time.DateOnly),
					Time:   now.Format(time.TimeOnly),
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := SendMail(tt.args.dest, tt.args.subject, tt.args.template, tt.args.payload); (err != nil) != tt.wantErr {
				t.Errorf("SendMail() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func Test_loadTemplate(t *testing.T) {

	type args struct {
		name string
		data interface{}
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "load receieved correctly",
			args: args{
				name: "received",
				data: ReceivedPayload{
					Name:   "test name",
					Title:  "test title",
					Moment: "morning",
					URL:    "https://dialradio.live/playlist/test",
				},
			},
			wantErr: false,
		},
		{
			name: "load processed correctly",
			args: args{
				name: "processed",
				data: ProcessedPayload{
					Name:   "test name",
					Title:  "test title",
					Moment: "morning",
					URL:    "https://dialradio.live/playlist/test",
				},
			},
			wantErr: false,
		},
		{
			name: "load scheduled correctly",
			args: args{
				name: "scheduled",
				data: ScheduledPayload{
					Name:   "test name",
					Title:  "test title",
					Moment: "morning",
				},
			},
			wantErr: false,
		},
		{
			name: "load error correctly",
			args: args{
				name: "error",
				data: ErrorPayload{
					Name:  "test name",
					Title: "test title",
					URL:   "https://dialradio.live/playlist/test",
				},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			_, err := loadTemplate(tt.args.name, tt.args.data)
			if (err != nil) != tt.wantErr {
				t.Errorf("loadTemplate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
		})
	}
}
