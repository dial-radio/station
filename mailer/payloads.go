package mailer

import (
	"fmt"
	"time"
)

type Payload interface {
	check() error
	data() interface{}
}

// ReceivedPayload is used to confirm to a contributor that their playlist was received
type ReceivedPayload struct {
	Name   string
	Moment string
	Title  string
	URL    string
}

func (c ReceivedPayload) check() error {
	var err error
	if c.Name == "" || c.URL == "" || c.Moment == "" {
		err = fmt.Errorf("the received payload should not be empty")
	}
	return err
}

func (c ReceivedPayload) data() interface{} {
	return c
}

// ProcessedPayload lets the contributor know that their playlist has been scheduled in a Slot
type ProcessedPayload struct {
	Name   string
	Title  string
	Moment string
	URL    string
}

func (c ProcessedPayload) check() error {
	var err error
	if c.Name == "" || c.URL == "" || c.Moment == "" {
		err = fmt.Errorf("the processed payload should not be empty")
	}
	return err
}

func (c ProcessedPayload) data() interface{} {
	return c
}

// ScheduledPayload lets the contributor know that their playlist has been scheduled in a Slot
type ScheduledPayload struct {
	Name   string
	Title  string
	Moment string
	Date   string
	Time   string
}

func (c ScheduledPayload) check() error {
	var err error
	if c.Name == "" || c.Time == "" || c.Date == "" || c.Moment == "" {
		err = fmt.Errorf("the scheduled payload should not be empty")
	}
	return err
}

func (c ScheduledPayload) data() interface{} {
	return c
}

// ErrorPayload reports errors to the admin team
type ErrorPayload struct {
	Name      string
	Email     string
	Title     string
	URL       string
	Timestamp time.Time
	Error     string
}

func (c ErrorPayload) check() error {
	var err error
	if c.Name == "" || c.Title == "" || c.URL == "" || c.Error == "" {
		err = fmt.Errorf("the error payload should not be empty")
	}
	return err
}

func (c ErrorPayload) data() interface{} {
	return c
}
