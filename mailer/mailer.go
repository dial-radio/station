package mailer

import (
	"context"
	"embed"
	"fmt"
	"html/template"
	"station/config"
	"strings"
	"time"

	"github.com/mailgun/mailgun-go/v4"
)

var (
	//go:embed templates/*.tmpl
	allTemplates embed.FS

	receivedTemplate  *template.Template
	processedTemplate *template.Template
	scheduledTemplate *template.Template
	errorTemplate     *template.Template
)

func init() {
	receivedTemplate = template.Must(template.ParseFS(allTemplates, "templates/received.tmpl"))
	processedTemplate = template.Must(template.ParseFS(allTemplates, "templates/processed.tmpl"))
	scheduledTemplate = template.Must(template.ParseFS(allTemplates, "templates/scheduled.tmpl"))
	errorTemplate = template.Must(template.ParseFS(allTemplates, "templates/error.tmpl"))
}

func loadTemplate(name string, data interface{}) (string, error) {
	var body strings.Builder
	switch name {
	case "received":
		err := receivedTemplate.Execute(&body, data)
		return body.String(), err
	case "processed":
		err := processedTemplate.Execute(&body, data)
		return body.String(), err
	case "scheduled":
		err := scheduledTemplate.Execute(&body, data)
		return body.String(), err
	case "error":
		err := errorTemplate.Execute(&body, data)
		return body.String(), err
	}

	return "", fmt.Errorf("no template found for %s", name)
}

func SendMail(recipient string, subject string, template string, payload Payload) error {
	var err error
	mg := mailgun.NewMailgun(config.Conf().MailgunDomain, config.Conf().MailgunPrivateKey)
	if !strings.Contains(config.Conf().MailgunDomain, "sandbox") {
		mg.SetAPIBase(mailgun.APIBaseEU) //-- rgpd mon amour
	}

	sender := fmt.Sprintf("Dial Radio <%s>", config.Bots().TeamEmail)
	message := mg.NewMessage(sender, subject, "", recipient)

	err = payload.check()
	if err != nil {
		return err
	}

	body, err := loadTemplate(template, payload.data())
	if err != nil {
		return err
	}
	message.SetHtml(body)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()

	_, _, err = mg.Send(ctx, message)
	return err
}
