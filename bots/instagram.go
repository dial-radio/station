package bots

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

// Documentation Instagram media queries : https://developers.facebook.com/docs/instagram-api/reference/ig-user/media#get-media

const API_URL = "https://graph.facebook.com/"
const graphAPIVersion = "v18.0"
const appID = "873170931066897"      // ID of the dial radio instagram app
const igUserID = "17841460929144046" // ID of the dial radio instagram account

type ImageContainerResponse struct {
	Id string `json:"id"`
}

type AccesTokenResponse struct {
	AccessToken string `json:"access_token"`
}

type UserTag struct {
	Username string `json:"username"`
	X        string `json:"x"`
	Y        string `json:"y"`
}

// PostInstagram handles the API calls to Instagram resulting in a new post
func postInstagram(playlist crate.Playlist, images []string) error {
	if config.Conf().InstragramToken == "" {
		return fmt.Errorf("no instagram token found")
	}
	var imageURLs []string
	var imageIDs []string
	var err error

	token, err := regenerateInstagramToken()
	if err != nil {
		log.Error().Err(err).Msg("[PostInstagram] regenerating token")
		return err
	}

	imageURLs = append(imageURLs, images...)
	if playlist.Image != "" {
		imageURLs = append(imageURLs, playlist.Image)
	}

	for _, imageURL := range imageURLs {
		generatedImageID, err := createImageContainer(imageURL, token)
		if err != nil {
			log.Error().Err(err).Msg("[PostInstagram] creating image container")
			return err
		}
		imageIDs = append(imageIDs, generatedImageID)
	}

	var contributor string
	if playlist.InstaHandle != "" {
		contributor = fmt.Sprintf("%s (@%s)", playlist.Artist, playlist.InstaHandle)
	} else {
		contributor = playlist.Artist
	}

	caption := fmt.Sprintf("New playlist coming up!\n\n%s by %s is about to be broadcast for the first time.\n\nTune in on %s/%s at %s:%s CET for a listen.\n\nOr contribute your own at dialradio.live/contribute! \n.\n.\n.\n.\n.\n #dialradio #webradio #worldwideplaylistbroadcasting", playlist.Title, contributor, utils.PadDateTime(int(playlist.ScheduleTime.Day())), utils.PadDateTime(int(playlist.ScheduleTime.Month())), utils.PadDateTime(int(playlist.ScheduleTime.Hour())), utils.PadDateTime(int(playlist.ScheduleTime.Minute())))

	containerID := imageIDs[0]
	if len(imageURLs) > 1 {
		containerID, err = createCarouselContainer(imageIDs, caption, token)
		if err != nil {
			log.Error().Err(err).Msg("[PostInstagram] create carousel container")
			return err
		}
	}

	time.Sleep(10 * time.Second)

	id, err := publishContainer(containerID, token)
	if err != nil {
		log.Error().Err(err).Msg("[PostInstagram] publish container")
		return err
	}

	log.Debug().Str("ID", id).Msg("[PostInstagram]")
	return nil
}

// createImageContainer create an image container with a handle, possibly in carousel mode
func createImageContainer(image string, token string) (string, error) {

	baseURL := fmt.Sprintf("https://graph.facebook.com/%s/%s/media", graphAPIVersion, igUserID)

	queryParams := url.Values{}
	queryParams.Add("image_url", fmt.Sprintf("https://images.dialradio.live/%s", filepath.Base(image)))
	queryParams.Add("access_token", token)
	queryParams.Add("is_carousel_item", "true")

	fullURL := fmt.Sprintf("%s?%s", baseURL, queryParams.Encode())
	req, err := http.NewRequest("POST", fullURL, nil)
	if err != nil {
		log.Error().Err(err).Msg("[createImageContainer] creating request:")
		return "", err
	}

	// Send the request
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("[createImageContainer] sending request:")
		return "", err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("[createImageContainer] reading response body:")
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf(string(body))
	}

	log.Debug().Str("response", string(body)).Msg("[createImageContainer]")

	var container ImageContainerResponse
	err = json.Unmarshal(body, &container)
	if err != nil {
		log.Error().Err(err).Msg("[createImageContainer] unmarshaling JSON:")
		return "", err
	}

	return container.Id, nil
}

// createCarouselContainer creates the carousel, given existing IDs and a caption
func createCarouselContainer(imageIDs []string, caption string, token string) (string, error) {
	baseURL := fmt.Sprintf("https://graph.facebook.com/%s/%s/media", graphAPIVersion, igUserID)

	queryParams := url.Values{}
	queryParams.Add("media_type", "CAROUSEL")
	queryParams.Add("access_token", token)
	queryParams.Add("children", strings.Join(imageIDs, ","))
	queryParams.Add("caption", caption)

	fullURL := fmt.Sprintf("%s?%s", baseURL, queryParams.Encode())
	req, err := http.NewRequest("POST", fullURL, nil)
	if err != nil {
		log.Error().Err(err).Msg("[createCarouselContainer] creating request:")
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("[createCarouselContainer] sending request:")
		return "", err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("[createCarouselContainer] reading response body:")
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		log.Error().Str("body", string(body)).Msg("[createCarouselContainer] non-20X response")
		return "", fmt.Errorf(string(body))
	}

	log.Debug().Str("response", string(body)).Msg("[createCarouselContainer]")

	var container ImageContainerResponse
	err = json.Unmarshal(body, &container)
	if err != nil {
		log.Error().Err(err).Msg("[createCarouselContainer] unmarshaling JSON:")
		return "", err
	}

	return container.Id, nil
}

func publishContainer(containerID string, token string) (string, error) {

	baseURL := fmt.Sprintf("https://graph.facebook.com/%s/%s/media_publish", graphAPIVersion, igUserID)

	queryParams := url.Values{}
	queryParams.Add("creation_id", containerID)
	queryParams.Add("access_token", token)

	fullURL := fmt.Sprintf("%s?%s", baseURL, queryParams.Encode())
	req, err := http.NewRequest("POST", fullURL, nil)
	if err != nil {
		log.Error().Err(err).Msg("[publishCarousel] creating request:")
		return "", err
	}

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("[publishCarousel] sending request:")
		return "", err
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Error().Err(err).Msg("[publishCarousel] reading response body:")
		return "", err
	}

	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf(string(body))
	}

	log.Debug().Str("response", string(body)).Msg("[publishCarousel]")

	var container ImageContainerResponse
	err = json.Unmarshal(body, &container)
	if err != nil {
		log.Error().Err(err).Msg("[publishCarousel] unmarshaling JSON:")
		return "", err
	}

	return container.Id, nil
}

// TODO
func regenerateInstagramToken() (string, error) {
	// We need to get the token from the database and regenerate if it's expired
	// check if the token is expired
	// In case it expires
	// 1. get the short term token GET https://graph.facebook.com/v18.0/{{dial-radio-app}}/?access_token={{token}}
	// 2. Use response to get long term token GET https://graph.facebook.com/{{graph-api-version}}/oauth/access_token?grant_type=fb_exchange_token&client_id={{dial-radio-app}}&client_secret={{app-secret}}&fb_exchange_token={{token}}

	if config.Conf().InstragramToken == "" {
		return "", fmt.Errorf("no instagram token found")
	}

	return config.Conf().InstragramToken, nil

	// appSecret := "SHORT LIVED TOKEN"
	// token := "???"
	// var endpoint = fmt.Sprintf("https://graph.facebook.com/%s/oauth/access_token?grant_type=fb_exchange_token&client_id=%s&client_secret=%s&fb_exchange_token=%s", graphAPIVersion, appID, appSecret, token)
	// req, err := http.NewRequest("GET", endpoint, nil)
	// if err != nil {
	// 	fmt.Println("Error creating request:", err)
	// 	return "", err
	// }

	// // Send the request
	// client := &http.Client{}
	// resp, err := client.Do(req)
	// if err != nil {
	// 	fmt.Println("Error sending request:", err)
	// 	return "", err
	// }
	// defer resp.Body.Close()

	// // Read and print the response body
	// body, err := io.ReadAll(resp.Body)
	// if err != nil {
	// 	fmt.Println("Error reading response body:", err)
	// 	return "", err
	// }

	// fmt.Println("Response:", string(body)) // Response: {"id":"17895695668004550"}

	// return "", nil
}
