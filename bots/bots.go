package bots

import (
	"fmt"
	"station/config"
	"station/crate"

	"github.com/rs/zerolog/log"
)

func PostSocialMedia(playlist crate.Playlist) error {
	if !config.Bots().CanPostBotInstagram && !config.Bots().CanPostBotTelegram {
		log.Info().Bool("CanPostTg", config.Bots().CanPostBotTelegram).Bool("CanPostIg", config.Bots().CanPostBotInstagram).Msg("[PostSocialMedia] skipping posts")
		return nil
	}

	if playlist.Palette.ID == 0 {
		p, err := crate.GetPaletteByID(fmt.Sprintf("%d", playlist.PaletteID))
		if err != nil {
			return err
		}
		playlist.Palette = p
	}

	image, err := GenerateCoverImage(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[PostSocialMedia] generate image")
		return err
	}

	if len(playlist.Tracks) == 0 {
		playlist, err = crate.GetPlaylistBy("id", fmt.Sprintf("%d", playlist.ID), true)
		if err != nil || len(playlist.Tracks) == 0 {
			log.Error().Err(err).Msg("[PostSocialMedia] get tracks")
			return err
		}
	}

	tracklists, err := GenerateTracklistImages(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[PostSocialMedia] generate tracklist")
		return err
	}

	if config.Bots().CanPostBotTelegram {
		go postTelegram(playlist, image)
	}

	if config.Bots().CanPostBotInstagram {
		go postInstagram(playlist, append([]string{image}, tracklists...))
	}

	return nil
}

func PostInstagram(playlist crate.Playlist) error {
	if !config.Bots().CanPostBotInstagram {
		log.Info().Bool("CanPostIg", config.Bots().CanPostBotInstagram).Msg("[PostInstagram] skipping posts")
		return nil
	}

	if playlist.Palette.ID == 0 {
		p, err := crate.GetPaletteByID(fmt.Sprintf("%d", playlist.PaletteID))
		if err != nil {
			return err
		}
		playlist.Palette = p
	}

	image, err := GenerateCoverImage(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[PostInstagram] generate image")
		return err
	}

	if len(playlist.Tracks) == 0 {
		playlist, err = crate.GetPlaylistBy("id", fmt.Sprintf("%d", playlist.ID), true)
		if err != nil || len(playlist.Tracks) == 0 {
			log.Error().Err(err).Msg("[PostInstagram] get tracks")
			return err
		}
	}

	tracklists, err := GenerateTracklistImages(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[PostInstagram] generate tracklist")
		return err
	}

	return postInstagram(playlist, append([]string{image}, tracklists...))
}

func PostTelegram(playlist crate.Playlist) error {
	if !config.Bots().CanPostBotTelegram {
		log.Info().Bool("CanPostTg", config.Bots().CanPostBotTelegram).Msg("[PostTelegram] skipping posts")
		return nil
	}

	if playlist.Palette.ID == 0 {
		p, err := crate.GetPaletteByID(fmt.Sprintf("%d", playlist.PaletteID))
		if err != nil {
			return err
		}
		playlist.Palette = p
	}

	image, err := GenerateCoverImage(playlist)
	if err != nil {
		log.Error().Err(err).Msg("[PostTelegram] generate image")
		return err
	}

	return postTelegram(playlist, image)
}
