package bots

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"

	"github.com/rs/zerolog/log"
)

func postTelegram(playlist crate.Playlist, coverImage string) error {
	if config.Conf().TelegramBotKey == "" {
		log.Error().Msg("[PostTelegram] no api key")
		return errors.New("no api key")
	}

	var image string
	if playlist.Image != "" {
		image = playlist.Image
	} else {
		image = filepath.Base(coverImage)
	}

	title := escapeString(playlist.Title)
	artist := escapeString(playlist.Artist)
	caption := fmt.Sprintf("*New playlist coming up\\!*\n\n[%s](https://dialradio.live/playlists/%d), contributed by %s is about to be broadcast for the first time\\.\n\nTune in on the %s of %s/%s, at %s:%s CET for a listen\\.\n\nContribute your own at [dialradio\\.live/contribute](https://dialradio\\.live) 📡", title, playlist.ID, artist, playlist.Moment, utils.PadDateTime(int(playlist.ScheduleTime.Day())), utils.PadDateTime(int(playlist.ScheduleTime.Month())), utils.PadDateTime(int(playlist.ScheduleTime.Hour())), utils.PadDateTime(int(playlist.ScheduleTime.Minute())))

	var publicChatID string
	if string(config.Bots().PublicChatID[0]) == "@" {
		publicChatID = config.Bots().PublicChatID
	} else {
		publicChatID = fmt.Sprintf("-%s", config.Bots().PublicChatID)
	}
	var endpoint = fmt.Sprintf("https://api.telegram.org/bot%s/sendPhoto", config.Conf().TelegramBotKey)
	form := url.Values{}
	form.Add("chat_id", publicChatID)
	form.Add("photo", fmt.Sprintf("https://images.dialradio.live/%s", image))
	form.Add("parse_mode", "MarkdownV2")
	form.Add("caption", caption)
	res, err := http.Post(endpoint, "application/x-www-form-urlencoded", strings.NewReader(form.Encode()))
	if err != nil {
		log.Error().Err(err).Msg("[PostTelegram] failed request")
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		log.Error().Err(err).Msg("[PostTelegram] failed to read response")
		return err
	}

	if res.StatusCode != http.StatusOK {
		log.Error().Str("body", string(body)).Msg("[PostTelegram] non-20X response")
		return fmt.Errorf("%s", body)
	}
	return nil
}

// Post received Playlists to internal chat
func PostReceivedPlaylist(playlist crate.Playlist) error {
	if config.Conf().TelegramBotKey == "" {
		log.Error().Msg("[PostTelegram] no api key")
		return errors.New("no api key")
	}

	if config.Bots().InternalChatID == "" {
		log.Error().Msg("[PostTelegram] no group chat ID")
		return errors.New("no group chat ID")
	}

	internalChatID := fmt.Sprintf("-%s", config.Bots().InternalChatID)

	title := escapeString(playlist.Title)
	artist := escapeString(playlist.Artist)
	caption := fmt.Sprintf("*New playlist received\\!*\n\n[%s](https://dialradio.live/playlists/%d), contributed by %s for the %s\\.", title, playlist.ID, artist, playlist.Moment)

	var endpoint = fmt.Sprintf("https://api.telegram.org/bot%s/sendMessage", config.Conf().TelegramBotKey)
	form := url.Values{}
	form.Add("chat_id", internalChatID)
	form.Add("parse_mode", "MarkdownV2")
	form.Add("text", caption)
	res, err := http.Post(endpoint, "application/x-www-form-urlencoded", strings.NewReader(form.Encode()))
	if err != nil {
		return err
	}
	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return err
	}

	if res.StatusCode != http.StatusOK {
		log.Error().Str("body", string(body)).Msg("[PostTelegram] non-20X response")
		return fmt.Errorf("%s", body)
	}
	return nil
}

func escapeString(raw string) string {
	r := strings.NewReplacer(
		"\\", "\\\\",
		"_", "\\_",
		"*", "\\*",
		"[", "\\[",
		"]", "\\]",
		"(", "\\(",
		")", "\\)",
		"~", "\\~",
		"`", "\\`",
		">", "\\>",
		"<", "\\<",
		"&", "\\&",
		"#", "\\#",
		"+", "\\+",
		"-", "\\-",
		"=", "\\=",
		"|", "\\|",
		"{", "\\{",
		"}", "\\}",
		".", "\\.",
		"!", "\\!")
	return r.Replace(raw)
}
