package bots

import (
	"embed"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
)

const (
	size         = 1400
	timezone     = "CET (Berlin Time)"
	dialradiotag = "dialradio.live"

	logoTemplate = "logo-template.svg"
	fontRegular  = "AlteHaasGroteskRegular.ttf"
	fontBold     = "AlteHaasGroteskBold.ttf"
)

var (
	//go:embed res/*
	resources embed.FS

	largeFontSize  = 0
	normalFontSize = 0
	padding        = 0.0
)

func init() {
	largeFontSize = size / 12
	normalFontSize = size / 28
	padding = size / 45
}

func GenerateCoverImage(p crate.Playlist) (string, error) {
	if _, err := exec.LookPath("convert"); err != nil {
		return "", err
	}

	// create folder and output file
	hash := utils.RandString(8)
	tmpDir := filepath.Join(config.ImageDir(), fmt.Sprintf("cover_%s", hash))
	if err := os.Mkdir(tmpDir, os.ModePerm); err != nil {
		return "", err
	}
	defer func() {
		err := os.RemoveAll(tmpDir)
		if err != nil {
			log.Error().Err(err).Msg("[GenerateImage] failed to remove tmp dir")
		}
	}()

	logoFile, _ := os.Create(filepath.Join(tmpDir, logoTemplate))
	logoFileBytes, err := resources.ReadFile(filepath.Join("res/images", logoTemplate))
	if err != nil {
		return "", err
	}
	defer logoFile.Close()

	paintedLogo := strings.ReplaceAll(string(logoFileBytes), "$FOREGROUND_COLOR", p.Palette.Foreground)
	paintedLogo = strings.ReplaceAll(paintedLogo, fmt.Sprintf("$%s_COLOR", strings.ToUpper(p.Moment)), p.Color)
	var re = regexp.MustCompile(`\$\w+_COLOR`)
	paintedLogo = re.ReplaceAllString(paintedLogo, p.Palette.Foreground)
	if _, err = io.WriteString(logoFile, paintedLogo); err != nil {
		return "", err
	}

	fontRegularFile, _ := os.Create(filepath.Join(tmpDir, fontRegular))
	srcFile, err := resources.Open(filepath.Join("res/fonts", fontRegular))
	if err != nil {
		return "", err
	}
	if _, err = io.Copy(fontRegularFile, srcFile); err != nil {
		return "", err
	}
	defer fontRegularFile.Close()

	fontBoldFile, _ := os.Create(filepath.Join(tmpDir, fontBold))
	srcFile, err = resources.Open(filepath.Join("res/fonts", fontBold))
	if err != nil {
		return "", err
	}
	if _, err = io.Copy(fontBoldFile, srcFile); err != nil {
		return "", err
	}
	defer fontBoldFile.Close()

	dialRaster, err := os.Create(filepath.Join(tmpDir, "dial_raster.png"))
	if err != nil {
		return "", err
	}
	defer dialRaster.Close()

	pngFinalOutput, err := os.Create(filepath.Join(tmpDir, "final_output.png"))
	if err != nil {
		return "", err
	}
	defer pngFinalOutput.Close()

	finalOutput, err := os.Create(filepath.Join(tmpDir, "final_output.jpg"))
	if err != nil {
		return "", err
	}
	defer finalOutput.Close()

	// generate  and fill final output
	args := make([]string, 0)
	args = append(args, "-size", fmt.Sprintf("%dx%d", size, size))
	args = append(args, fmt.Sprintf("xc:%s", p.Palette.Background))
	args = append(args, pngFinalOutput.Name())
	if err = runScript("magick", args); err != nil {
		return "", err
	}

	// converts the logo with color to raster
	args = make([]string, 0)
	args = append(args, "-density", "1200")
	args = append(args, "-background", "none")
	args = append(args, filepath.Join(tmpDir, logoTemplate))
	args = append(args, "-scale", fmt.Sprintf("%fx", (size/2-padding)))
	args = append(args, dialRaster.Name())
	if err = runScript("magick", args); err != nil {
		return "", err
	}

	// composite dial and final output
	args = make([]string, 0)
	args = append(args, dialRaster.Name())
	args = append(args, pngFinalOutput.Name())
	args = append(args, "-colorspace", "sRGB")
	args = append(args, "-gravity", getDialGravity(p.Moment))
	args = append(args, "-geometry", fmt.Sprintf("+%f+%f", padding, padding))
	args = append(args, pngFinalOutput.Name())
	if err = runScript("composite", args); err != nil {
		return "", err
	}

	// Write date, moment and time
	formattedDate := fmt.Sprintf("%s/%s/%s", utils.PadDateTime(p.ScheduleTime.Day()), utils.PadDateTime(int(p.ScheduleTime.Month())), utils.PadDateTime(int(p.ScheduleTime.Year())))
	startTime := fmt.Sprintf("%s:%s", utils.PadDateTime(p.ScheduleTime.Hour()), utils.PadDateTime(p.ScheduleTime.Minute()))
	endTime := fmt.Sprintf("%s:%s", utils.PadDateTime(p.ScheduleTime.Add(p.Duration).Hour()), utils.PadDateTime(p.ScheduleTime.Add(p.Duration).Minute()))

	args = make([]string, 0)
	args = append(args, pngFinalOutput.Name())
	args = append(args, "-font", fontRegularFile.Name())
	args = append(args, "-pointsize", fmt.Sprintf("%d", normalFontSize))
	args = append(args, "-fill", p.Color)
	args = append(args, "-gravity", getScheduleGravity(p.Moment))
	if p.Moment == "afternoon" {
		args = append(args, "-annotate", fmt.Sprintf("+%f+%f", size*0.525, padding))
	} else if p.Moment == "evening" {
		args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding*3))
	} else {
		args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding))
	}
	args = append(args, fmt.Sprintf("%s · %s\n%s—%s", formattedDate, utils.Capitalize(p.Moment), startTime, endTime))
	args = append(args, pngFinalOutput.Name())
	if err = runScript("magick", args); err != nil {
		return "", err
	}

	// Write title and wrap text to stay inside caption box
	args = make([]string, 0)
	args = append(args, pngFinalOutput.Name())
	args = append(args, "-background", "transparent")
	args = append(args, "-fill", p.Color)
	args = append(args, "-font", fontRegularFile.Name())
	args = append(args, "-pointsize", fmt.Sprintf("%d", largeFontSize))
	args = append(args, "-interline-spacing", "-20")
	args = append(args, "-kerning", "-1")
	args = append(args, "-size", fmt.Sprintf("%dx%d", size/2, size/2))
	args = append(args, fmt.Sprintf("caption:%s", p.Title))
	args = append(args, "-gravity", getTitleGravity(p.Moment))
	if p.Moment == "evening" {
		args = append(args, "-geometry", fmt.Sprintf("+%d+%f", size/2, size/2+padding*2.5))
	} else if p.Moment == "afternoon" {
		args = append(args, "-geometry", fmt.Sprintf("+%f+%f", size*0.525, padding*2.5))
	} else {
		args = append(args, "-geometry", fmt.Sprintf("+%f+%f", padding, padding*2.5))
	}
	args = append(args, "+repage")
	args = append(args, "-composite")
	args = append(args, pngFinalOutput.Name())
	if err = runScript("magick", args); err != nil {
		return "", err
	}

	// write artist
	args = make([]string, 0)
	args = append(args, pngFinalOutput.Name())
	args = append(args, "-font", fontRegularFile.Name())
	args = append(args, "-pointsize", fmt.Sprintf("%d", normalFontSize))
	args = append(args, "-fill", p.Color)
	args = append(args, "-gravity", getArtistGravity(p.Moment))
	if p.Moment == "afternoon" {
		args = append(args, "-annotate", fmt.Sprintf("+%f+%f", size*0.525, padding))
	} else if p.Moment == "evening" {
		args = append(args, "-annotate", fmt.Sprintf("+%d+%f", size/2, padding*2))
	} else {
		args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding))
	}
	args = append(args, p.Artist)
	args = append(args, pngFinalOutput.Name())
	if err = runScript("magick", args); err != nil {
		return "", err
	}

	// Write dialradio tag
	args = make([]string, 0)
	args = append(args, pngFinalOutput.Name())
	args = append(args, "-font", fontRegularFile.Name())
	args = append(args, "-pointsize", fmt.Sprintf("%d", normalFontSize))
	args = append(args, "-fill", p.Color)
	args = append(args, "-gravity", getURLGravity(p.Moment))
	args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding))
	args = append(args, dialradiotag)
	args = append(args, pngFinalOutput.Name())
	if err = runScript("magick", args); err != nil {
		return "", err
	}

	// Convert png to jpg
	args = make([]string, 0)
	args = append(args, pngFinalOutput.Name())
	args = append(args, "-background", p.Palette.Background)
	args = append(args, "-flatten")
	args = append(args, "-alpha", "deactivate")
	args = append(args, finalOutput.Name())
	err = runScript("convert", args)
	if err != nil {
		return "", err
	}

	// output file to destination
	finalDestination := filepath.Join(config.ImageDir(), fmt.Sprintf("%d_%s_%s_cover.jpg", p.ID, p.Slug, p.Moment))
	err = utils.CopyFile(finalOutput.Name(), finalDestination)
	return finalDestination, err
}

func GenerateTracklistImages(p crate.Playlist) ([]string, error) {
	images := make([]string, 0)
	if len(p.Tracks) == 0 {
		return images, fmt.Errorf("no tracks on playlist")
	}

	const TRACKS_PER_IMAGE = 18
	for i := 0; i < len(p.Tracks); i += TRACKS_PER_IMAGE {
		tmp := p
		tmp.Tracks = p.Tracks[i:min(len(p.Tracks), i+TRACKS_PER_IMAGE)]
		tracklistImage, err := generateTracklistImage(tmp, i/TRACKS_PER_IMAGE)
		if err != nil {
			return images, err
		}
		images = append(images, tracklistImage)
	}

	return images, nil
}

func generateTracklistImage(p crate.Playlist, i int) (string, error) {

	// create folder and output file
	hash := utils.RandString(8)
	tmpDir := filepath.Join(config.ImageDir(), fmt.Sprintf("tracklist_%s", hash))
	err := os.Mkdir(tmpDir, os.ModePerm)
	if err != nil {
		return "", err
	}
	defer func() {
		err := os.RemoveAll(tmpDir)
		if err != nil {
			log.Error().Err(err).Msg("[GenerateImage] failed to remove tmp dir")
		}
	}()

	fontRegularFile, _ := os.Create(filepath.Join(tmpDir, fontRegular))
	srcFile, err := resources.Open(filepath.Join("res/fonts", fontRegular))
	if err != nil {
		return "", err
	}
	_, err = io.Copy(fontRegularFile, srcFile)
	if err != nil {
		return "", err
	}

	fontBoldFile, _ := os.Create(filepath.Join(tmpDir, fontBold))
	srcFile, err = resources.Open(filepath.Join("res/fonts", fontBold))
	if err != nil {
		return "", err
	}
	_, err = io.Copy(fontBoldFile, srcFile)
	if err != nil {
		return "", err
	}
	defer srcFile.Close()

	tracklistTile, err := os.Create(filepath.Join(tmpDir, "tracklist.png"))
	if err != nil {
		return "", err
	}
	defer tracklistTile.Close()

	tracklistText, err := os.Create(filepath.Join(tmpDir, "tracklist.txt"))
	if err != nil {
		return "", err
	}
	defer tracklistText.Close()

	finalOutput, err := os.Create(filepath.Join(tmpDir, "final_output.jpg"))
	if err != nil {
		return "", err
	}
	defer finalOutput.Close()

	// Create png file
	args := []string{}
	args = append(args, "-size", fmt.Sprintf("%dx%d", size, size))
	args = append(args, fmt.Sprintf("xc:%s", p.Palette.Background))
	args = append(args, tracklistTile.Name())
	err = runScript("magick", args)
	if err != nil {
		return "", err
	}

	// Create jpg file
	args = make([]string, 0)
	args = append(args, "-size", fmt.Sprintf("%dx%d", size, size))
	args = append(args, fmt.Sprintf("xc:%s", p.Palette.Background))
	args = append(args, finalOutput.Name())
	err = runScript("magick", args)
	if err != nil {
		return "", err
	}

	var tracks []string
	for i, t := range p.Tracks {
		if i < 21 {
			tracks = append(tracks, fmt.Sprintf("%s - %s", t.Artist, t.Title))
		} else {
			break
		}
	}
	tracklist := strings.Join(tracks, "\n")
	tracklistText.WriteString(tracklist)

	args = make([]string, 0)
	args = append(args, tracklistTile.Name())
	args = append(args, "-size", fmt.Sprintf("%fx%f", size-(padding*2), size-(float32(normalFontSize)*6.5)))
	args = append(args, "-background", p.Palette.Background)
	args = append(args, "-gravity", "northwest")

	args = append(args, "-pointsize", fmt.Sprintf("%d", normalFontSize))
	args = append(args, "-fill", p.Color)
	args = append(args, fmt.Sprintf("caption:@%s", tracklistText.Name()))
	args = append(args, "-flatten", tracklistTile.Name())
	err = runScript("magick", args)
	if err != nil {
		return "", err
	}

	args = make([]string, 0)
	args = append(args, tracklistTile.Name())
	args = append(args, finalOutput.Name())
	args = append(args, "-gravity", "northwest")
	args = append(args, "-geometry", fmt.Sprintf("+%f+%f", padding, padding*4))
	args = append(args, finalOutput.Name())
	err = runScript("composite", args)
	if err != nil {
		return "", err
	}

	args = make([]string, 0)
	args = append(args, finalOutput.Name())
	args = append(args, "-font", fontRegularFile.Name())
	args = append(args, "-pointsize", fmt.Sprintf("%d", normalFontSize))
	args = append(args, "-fill", p.Palette.Foreground)
	args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding*2), "Tracklist")
	args = append(args, "-gravity", "southwest")
	args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, float32(padding)*2.5), fmt.Sprintf("%s/%s/%s - %s", utils.PadDateTime(p.ScheduleTime.Day()), utils.PadDateTime(int(p.ScheduleTime.Month())), utils.PadDateTime(p.ScheduleTime.Year()), utils.Capitalize(p.Moment)))
	args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding), fmt.Sprintf("%s:%s - %s:%s", utils.PadDateTime(p.ScheduleTime.Hour()), utils.PadDateTime(p.ScheduleTime.Minute()), utils.PadDateTime(p.ScheduleTime.Add(p.Duration).Hour()), utils.PadDateTime(p.ScheduleTime.Add(p.Duration).Minute())))
	args = append(args, finalOutput.Name())
	err = runScript("magick", args)
	if err != nil {
		return "", err
	}

	args = make([]string, 0)
	args = append(args, finalOutput.Name())
	args = append(args, "-font", fontRegularFile.Name())
	args = append(args, "-pointsize", fmt.Sprintf("%d", normalFontSize))
	args = append(args, "-fill", p.Color)
	args = append(args, "-gravity", "southeast")
	args = append(args, "-annotate", fmt.Sprintf("+%f+%f", padding, padding), "dialradio.live")
	args = append(args, finalOutput.Name())
	err = runScript("magick", args)
	if err != nil {
		return "", err
	}

	finalDestination := filepath.Join(config.ImageDir(), fmt.Sprintf("%d_%s_%s_tracklist_%d.jpg", p.ID, p.Slug, p.Moment, i+1))
	err = utils.CopyFile(finalOutput.Name(), finalDestination)

	return finalDestination, err
}

func runScript(command string, args []string) error {
	cmd := exec.Command(command, args...)
	suffix := utils.RandString(8)

	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_%s_%s.log", y, utils.PadDateTime(int(m)), utils.PadDateTime(d), utils.PadDateTime(h), utils.PadDateTime(min), utils.PadDateTime(sec), command, suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "magick", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	log.Debug().Strs("args", args).Str("command", command).Msg("[runScript]")
	if err != nil {
		log.Error().Err(err).Msg("[runScript] error starting")
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[runScript] error completing")
		if log.Logger.GetLevel() == zerolog.TraceLevel {
			bytes, _ := os.ReadFile(logfile.Name())
			fmt.Println(string(bytes))
			log.Info().Str("log", string(bytes))
		}
		return err
	}
	return nil
}

func getTitleGravity(m string) string {
	switch m {
	case "morning":
		return "northwest"
	case "afternoon":
		return "northwest"
	case "evening":
		return "northwest"
	case "night":
		return "northwest"
	}

	log.Warn().Str("moment", m).Msg("[getTitleArtistGravity] wrong moment")
	return "southwest"
}

func getArtistGravity(m string) string {
	switch m {
	case "morning":
		return "northwest"
	case "afternoon":
		return "northwest"
	case "evening":
		return "west"
	case "night":
		return "northwest"
	}

	log.Warn().Str("moment", m).Msg("[getTitleArtistGravity] wrong moment")
	return "southwest"
}

func getScheduleGravity(m string) string {
	switch m {
	case "morning":
		return "southwest"
	case "afternoon":
		return "southwest"
	case "evening":
		return "west"
	case "night":
		return "southwest"
	}

	log.Warn().Str("moment", m).Msg("[getScheduleGravity] wrong moment")
	return "southwest"
}

func getURLGravity(m string) string {
	switch m {
	case "morning":
		return "northeast"
	case "afternoon":
		return "northwest"
	case "evening":
		return "northeast"
	case "night":
		return "southeast"
	}

	log.Warn().Str("moment", m).Msg("[getScheduleGravity] wrong moment")
	return "southwest"
}

func getDialGravity(m string) string {
	switch m {
	case "morning":
		return "southeast"
	case "afternoon":
		return "southwest"
	case "evening":
		return "northwest"
	case "night":
		return "northeast"
	}

	log.Warn().Str("moment", m).Msg("[getDialGravity] wrong moment")
	return "southwest"
}
