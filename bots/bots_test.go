package bots

import (
	"path/filepath"
	"runtime"
	"station/config"
	"testing"

	"github.com/spf13/viper"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

func setup() func(t *testing.T) {
	viper.Set("STATION_MODE", "test")

	c, err := config.Init(viper.GetString("STATION_CONFIG_PATH"))
	if err != nil {
		panic(err)
	}
	c.Bots.CanPostBotTelegram = true

	return func(t *testing.T) {}
}
