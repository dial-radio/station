package bots

import (
	"os"
	"path/filepath"
	"station/config"
	"station/crate"
	"testing"
	"time"

	"github.com/gosimple/slug"
)

func TestGenerateCoverImage(t *testing.T) {
	teardown := setup()
	defer teardown(t)

	type args struct {
		p crate.Playlist
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "test morning",
			args: args{
				p: crate.Playlist{
					ID:           666,
					Artist:       "One T & Cool T",
					Title:        "Magic Key",
					Slug:         slug.Make("Magic Key"),
					ScheduleTime: time.Now().Add(1 * time.Hour),
					Duration:     174 * time.Minute,
					Palette: crate.Palette{
						Background: "#8EB3BF",
						Foreground: "#6C919E",
					},
					Moment: "morning",
					Color:  "#3173C0",
				},
			},
			want:    filepath.Join(config.ImageDir(), "666_magic-key_morning_cover.jpg"),
			wantErr: false,
		},
		{
			name: "test afternoon",
			args: args{
				p: crate.Playlist{
					ID:           666,
					Artist:       "OutKast",
					Title:        "Prototype",
					Slug:         slug.Make("Prototype"),
					ScheduleTime: time.Now().Add(-1 * time.Hour),
					Duration:     174 * time.Minute,
					Palette: crate.Palette{
						Background: "#f9dd48",
						Foreground: "#F5EDA4",
					},
					Moment: "afternoon",
					Color:  "#77A4DA",
				},
			},
			want:    filepath.Join(config.ImageDir(), "666_prototype_afternoon_cover.jpg"),
			wantErr: false,
		},
		{
			name: "test evening",
			args: args{
				p: crate.Playlist{
					ID:           666,
					Artist:       "Floorplan",
					Title:        "Never Grow Old",
					Slug:         slug.Make("Never Grow Old"),
					ScheduleTime: time.Now().Add(1 * time.Hour),
					Duration:     374 * time.Minute,
					Palette: crate.Palette{
						Background: "#d06189",
						Foreground: "#f47052",
					},
					Moment: "evening",
					Color:  "#FF92D3",
				},
			},
			want:    filepath.Join(config.ImageDir(), "666_never-grow-old_evening_cover.jpg"),
			wantErr: false,
		},
		{
			name: "test night",
			args: args{
				p: crate.Playlist{
					ID:           666,
					Artist:       "Her",
					Title:        "Helwa Ya Baladii",
					Slug:         slug.Make("Helwa Ya Baladii"),
					ScheduleTime: time.Now().Add(1 * time.Hour),
					Duration:     174 * time.Minute,
					Palette: crate.Palette{
						Background: "#262626",
						Foreground: "#565656",
					},
					Moment: "night",
					Color:  "#F64141",
				},
			},
			want:    filepath.Join(config.ImageDir(), "666_helwa-ya-baladii_night_cover.jpg"),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GenerateCoverImage(tt.args.p)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateImage() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GenerateImage() = %v, want %v", got, tt.want)
			}

			file, err := os.Stat(got)
			if err != nil {
				t.Error(err)
			}

			if file.Size() == 0 {
				t.Error(err)
			}
		})
	}
}

func TestGenerateTracklistImages(t *testing.T) {
	teardown := setup()
	defer teardown(t)

	type args struct {
		p crate.Playlist
	}
	tests := []struct {
		name    string
		args    args
		want    []string
		wantErr bool
	}{
		{
			name: "should generate tracklist",
			args: args{
				crate.Playlist{
					ID:           666,
					Artist:       "test",
					Title:        "test title",
					Slug:         slug.Make("test title"),
					ScheduleTime: time.Now().Add(1 * time.Hour),
					Duration:     174 * time.Minute,
					Palette: crate.Palette{
						Background: "#8eb3bf",
						Foreground: "#6c919e",
					},
					Color:  "#546695",
					Moment: "morning",
					Tracks: []crate.Track{
						{Artist: "AG-VO",
							Title: "1 - Intro"},
						{Artist: "Slowfoam",
							Title: "2 - Evapotranspiration"},
						{Artist: "Luke Sanger",
							Title: "3 - Wildlife"},
						{Artist: "Be Hedayat I",
							Title: "4 - Vox Popul"},
						{Artist: "ju ca",
							Title: "5 - but echoing of you but echoing of you but echoing of you but echoing of you"},
						{Artist: "Slow Attack Ensemble",
							Title: "6 - Recurring After Imag"},
						{Artist: "John McGuire",
							Title: "7 - -Vanishing Point"},
						{Artist: "Tujiko Noriko",
							Title: "8 - A Meeting at the Space station, A Meeting at the Space station"},
						{Artist: "Fila Brasillia",
							Title: "9 - New Chaos "},
						{Artist: "TBA (Natalie Beridze)",
							Title: "10 - Xirugmost"},
						{Artist: "UMAN",
							Title: "11 - Человечност"},
						{Artist: "Carlos Niño, Sam Gendel and Nate Mercereau",
							Title: "12 - for the Shapalaceer"},
						{Artist: "Bongo Entp.",
							Title: "13 - Lujón"},
						{Artist: "Enigma",
							Title: "14 - Principles of Lut: Find Love"},
						{Artist: "Willy Baldarou",
							Title: "15 - Mamb"},
						{Artist: "Yu Su",
							Title: "16 - Touch Me No"},
						{Artist: "Hirono Nishiyama",
							Title: "17 - Night Singin"},
						{Artist: "AG-VO",
							Title: "18 - Intro once more"},
						{Artist: "Slowfoam",
							Title: "19 - Evapotranspiration"},
						{Artist: "Luke Sanger",
							Title: "20 - Wildlife"},
						{Artist: "Be Hedayat I",
							Title: "21 - -Vox Popul"},
						{Artist: "ju ca",
							Title: "22 - but echoing of you ou ou"},
						{Artist: "Slow Attack Ensemble",
							Title: "23 - Recurring After Imag"},
					},
				},
			},
			want:    []string{filepath.Join(config.ImageDir(), "666_test-title_morning_tracklist_1.jpg"), filepath.Join(config.ImageDir(), "666_test-title_morning_tracklist_2.jpg")},
			wantErr: false,
		},
		{
			name: "should generate unicode tracklist",
			args: args{
				crate.Playlist{
					ID:           667,
					Artist:       "عمر",
					Title:        "سودانية",
					Slug:         slug.Make("سودانية"),
					ScheduleTime: time.Now().Add(1 * time.Hour),
					Duration:     174 * time.Minute,
					Palette: crate.Palette{
						Background: "#d06189",
						Foreground: "#f47052",
					},
					Color:  "#FF92D3",
					Moment: "evening",
					Tracks: []crate.Track{
						{Artist: "Весна",
							Title: "4 Позиции Бруно"},
						{Artist: "Порез на Собаке",
							Title: "Мой Секретный Жук"},
						{Artist: "Grazhdanskaya Oborona",
							Title: "Мёртвые"},
						{Artist: "Егор и Опизденевшие",
							Title: "Свобода"},
						{Artist: "Yanka",
							Title: "От большого ума"},
						{Artist: "Хадн дадн",
							Title: "Храмомама"},
						{Artist: "Zvuki Mu",
							Title: "Butylka vodki"},
						{Artist: "Auktyon",
							Title: "Зимы не будет"},
					},
				},
			},
			want:    []string{filepath.Join(config.ImageDir(), "667_swdny_evening_tracklist_1.jpg")},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GenerateTracklistImages(tt.args.p)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateTracklist() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if len(got) != len(tt.want) {
				t.Errorf("GenerateTracklist() = %v, want %v", len(got), len(tt.want))
			}

			for i := 0; i < len(tt.want); i++ {
				if got[i] != tt.want[i] {
					t.Errorf("GenerateTracklist() = %v, want %v", got[i], tt.want[i])
				}
			}
		})
	}
}
