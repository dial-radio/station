package bots

import (
	"path/filepath"
	"station/crate"
	"testing"
	"time"

	"github.com/joho/godotenv"
	"github.com/spf13/viper"
)

func Test_postTelegram(t *testing.T) {
	if viper.GetString("STATION_TELEGRAM_BOT_KEY") == "" {
		if err := godotenv.Load(filepath.Join(Basepath, "../.secrets")); err != nil {
			t.Log("failed to load secrets env")
		}
	}
	teardown := setup()
	defer teardown(t)

	type args struct {
		payload    crate.Playlist
		coverImage string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "succesfully public post to telegram",
			args: args{
				payload: crate.Playlist{
					ID: 0,
					Palette: crate.Palette{
						Background: "#000",
						Foreground: "#fff",
					},
					ScheduleTime: time.Now(),
					Duration:     50000000,
					Moment:       "morning",
					Artist:       "telegram test artist",
					Title:        "telegram test playlist", InstaHandle: "",
					Image: "fallback.png"},
				coverImage: "",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := postTelegram(tt.args.payload, tt.args.coverImage); (err != nil) != tt.wantErr {
				t.Errorf("postTelegram() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPostReceivedPlaylist(t *testing.T) {
	if viper.GetString("STATION_TELEGRAM_BOT_KEY") == "" {
		if err := godotenv.Load(filepath.Join(Basepath, "../.secrets")); err != nil {
			t.Log("failed to load secrets env")
		}
	}
	teardown := setup()
	defer teardown(t)

	type args struct {
		payload crate.Playlist
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "succesfully public post to telegram",
			args: args{
				payload: crate.Playlist{
					ID: 0,
					Palette: crate.Palette{
						Background: "#000",
						Foreground: "#fff",
					},
					ScheduleTime: time.Now(),
					Duration:     50000000,
					Moment:       "morning",
					Artist:       "telegram test artist",
					Title:        "telegram test playlist", InstaHandle: "",
					Image: "fallback.png"},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := PostReceivedPlaylist(tt.args.payload); (err != nil) != tt.wantErr {
				t.Errorf("PostReceivedPlaylist() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
