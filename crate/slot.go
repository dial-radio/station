package crate

import (
	"fmt"
	"math"
	"station/config"
	"station/utils"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

// the Slot represents an upcoming moment during which Playlists will be played
// new Slots are created by the liquidsoap process, and automatically fill themselves with playlists
type Slot struct {
	ID        uint           `gorm:"primarykey"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deletedAt"`
	Playlists []Playlist     `gorm:"many2many:slot_playlists;" json:"playlists"`
	QueueTime time.Time      `json:"queueTime"`
	Duration  time.Duration  `json:"duration"`
	Moment    string         `gorm:"notnull" json:"moment"`
}

// -- slots
func GetAllSlots(page int) ([]Slot, error) {
	var slots []Slot
	err := store.db.Model(&Slot{}).Limit(config.Api().SlotPageSize).Offset(page * config.Api().SlotPageSize).Order("queue_time desc").Find(&slots).Error
	if err != nil {
		return slots, err
	}

	for i, slot := range slots {
		var playlists []Playlist
		err := store.db.Model(&slot).Order("schedule_time asc").Association("Playlists").Find(&playlists)
		slots[i].Playlists = append(slot.Playlists, playlists...)

		if err != nil {
			return slots, err
		}
	}

	return slots, nil
}

// GetSlotsOnRotation gets the queue and returns only the first four items
func GetSlotsOnRotation() ([]Slot, error) {
	var rotating []Slot
	queued, err := GetQueuedSlots()
	if err != nil {
		return rotating, nil
	}

	min := int(math.Min(float64(len(queued)), 4.0))
	rotating = queued[0:min]

	return rotating, nil
}

// GetSchedule returns all slots, starting from the current day, and up until the last queued slot
func GetSchedule() ([]Slot, error) {
	var slots []Slot
	var scheduled []Slot
	tx := store.db.Model(&Slot{}).Order("queue_time asc").Find(&slots)

	today := utils.GetTodayDate()
	today = today.Add(time.Minute * -1) // we offset by 1min backwards to grab the midnight slot
	for _, s := range slots {
		if s.QueueTime.After(today) {
			var playlists []Playlist
			err := store.db.Model(&s).Order("schedule_time asc").Association("Playlists").Find(&playlists)
			if err != nil {
				return slots, err
			}

			for i := range playlists {
				p, err := GetPaletteByID(fmt.Sprintf("%d", playlists[i].PaletteID))
				if err != nil {
					return slots, err
				}
				playlists[i].Palette = p
			}

			s.Playlists = playlists

			scheduled = append(scheduled, s)
		}
	}
	return scheduled, tx.Error
}

// GetQueuedSlots returns all slots that are either currently playing, or queued to play
func GetQueuedSlots() ([]Slot, error) {
	var slots []Slot
	var queued []Slot
	tx := store.db.Model(&Slot{}).Order("queue_time asc").Find(&slots)
	current_timeslot := time.Now().Add(-time.Hour * 6)
	for _, s := range slots {
		if s.QueueTime.After(current_timeslot) {
			var playlists []Playlist
			err := store.db.Model(&s).Order("schedule_time asc").Association("Playlists").Find(&playlists)
			if err != nil {
				return slots, err
			}
			for i := range playlists {
				p, err := GetPaletteByID(fmt.Sprintf("%d", playlists[i].PaletteID))
				if err != nil {
					return slots, err
				}
				playlists[i].Palette = p
			}
			s.Playlists = playlists
			queued = append(queued, s)
		}
	}
	return queued, tx.Error
}

// GetQueuedSlotsByMoment returns a queue of upcoming Slots for a specific Moment
func GetQueuedSlotsByMoment(moment string) ([]Slot, error) {
	var slots []Slot
	var queued []Slot
	tx := store.db.Model(&Slot{}).Where("moment = ?", moment).Order("queue_time asc").Find(&slots)
	current_timeslot := time.Now().Add(-time.Hour * 6)
	for _, s := range slots {
		if s.QueueTime.After(current_timeslot) {
			var playlists []Playlist
			err := store.db.Model(&s).Order("schedule_time asc").Association("Playlists").Find(&playlists)
			if err != nil {
				return slots, err
			}
			s.Playlists = playlists
			queued = append(queued, s)
		}
	}
	return queued, tx.Error
}

func GetPastSlots() ([]Slot, error) {
	var slots []Slot
	var past []Slot
	tx := store.db.Model(&Slot{}).Order("queue_time asc").Find(&slots)
	current_timeslot := time.Now().Add(-time.Hour * 6)
	for _, s := range slots {
		if s.QueueTime.Before(current_timeslot) {
			var playlists []Playlist
			err := store.db.Model(&s).Order("schedule_time asc").Association("Playlists").Find(&playlists)
			s.Playlists = playlists

			if err != nil {
				return slots, err
			}
			past = append(past, s)
		}
	}
	return past, tx.Error
}

func GetSlotsByMoment(moment string) ([]Slot, error) {
	var slots []Slot
	tx := store.db.Model(&Slot{}).Where("moment = ?", moment).Order("queue_time asc").Find(&slots)
	return slots, tx.Error
}

func CreateSlot(slot Slot) (Slot, error) {
	err := store.db.Create(&slot).Error
	return slot, err
}

func EditSlot(slot Slot) (Slot, error) {
	err := store.db.Updates(&slot).Error
	return slot, err
}

// EditSlotPlaylists updates both the slot and the playlist associations
func EditSlotPlaylists(slot Slot) (Slot, error) {
	err := store.db.Updates(&slot).Error
	if err != nil {
		return slot, err
	}
	err = store.db.Model(&slot).Association("Playlists").Replace(slot.Playlists)
	return slot, err
}

func GetSlot(id string) (Slot, error) {
	var slot Slot
	_, err := strconv.Atoi(id)
	if err != nil {
		return slot, err
	}
	err = store.db.Model(&Slot{}).Where("id = ?", id).First(&slot).Error
	if err != nil {
		return slot, err
	}

	var playlists []Playlist
	err = store.db.Model(&slot).Order("schedule_time asc").Association("Playlists").Find(&playlists)
	slot.Playlists = playlists

	return slot, err
}

func DeleteSlot(id string) (string, error) {
	slot, err := GetSlot(id)
	if err != nil {
		return "", err
	}

	playlists := slot.Playlists

	// remove associations
	if err := store.db.Model(&slot).Association("Playlists").Clear(); err != nil {
		return "", err
	}

	// update playlist status
	for _, playlist := range playlists {
		playlist.Status, err = GetStatusFromSlots(playlist)
		if err != nil {
			return "", err
		}

		if _, err = EditPlaylist(playlist); err != nil {
			return "", err
		}
	}

	// check if the slot is in the future
	if slot.QueueTime.After(time.Now()) {
		queued, err := GetQueuedSlotsByMoment(slot.Moment)
		if err != nil {
			return "", err
		}

		isSlotAfter := false
		for _, q := range queued {

			if isSlotAfter {
				q.QueueTime = q.QueueTime.Add(-time.Hour * 24) // TODO: replace by calculating the appropriate time (i.e. starting from slot's QueueTime, finding the next correct queueTime and setting it)
				_, err := EditSlot(q)
				if err != nil {
					return "", err
				}
			}

			if q.ID == slot.ID {
				isSlotAfter = true
			}
		}
	}

	tx := store.db.Delete(&Slot{}, id)
	if tx.Error != nil {
		return "0", tx.Error
	} else if tx.RowsAffected == 0 {
		return "0", fmt.Errorf("could not find row %s", id)
	}
	return id, nil
}

// GetStatusFromSlots determines the status of a playlist based on the slot queue
func GetStatusFromSlots(playlist Playlist) (string, error) {
	queued, err := GetQueuedSlotsByMoment(playlist.Moment)
	if err != nil {
		return "", err
	}

	for i, q := range queued {
		for _, p := range q.Playlists {
			if p.ID == playlist.ID {
				if i == 0 {
					return utils.OnRotation, nil
				} else {
					return utils.Scheduled, nil
				}
			}
		}
	}

	return utils.InLibrary, nil
}

// Gets the slot currently playing (first of queue)
func GetSlotOnAir() (Slot, error) {
	var slot Slot
	slots, err := GetQueuedSlots()
	if err != nil {
		return slot, err
	}

	slot = slots[0]

	var playlists []Playlist
	err = store.db.Model(&slot).Order("schedule_time asc").Association("Playlists").Find(&playlists)
	slot.Playlists = playlists

	return slot, err
}

// Gets the slot that just played (last of past)
func GetPreviousSlot() (Slot, error) {
	slots, err := GetPastSlots()
	if err != nil {
		return Slot{}, err
	}

	if len(slots) > 0 {
		return slots[len(slots)-1], err
	} else {
		return Slot{}, gorm.ErrRecordNotFound
	}
}

// GetNextSlot returns the next candidate slot from the schedule: matches Moment and queue_time > time.Now()
func GetNextSlot(moment string) (Slot, error) {
	var next Slot
	var all_slots []Slot
	res := store.db.Model(&Slot{}).Where("moment = ?", moment).Order("queue_time asc").Find(&all_slots)
	if res.Error != nil {
		log.Error().Err(res.Error).Msg("[GetNextSlot] candidates not found")
		return next, res.Error
	}

	found := false
	current_timeslot := time.Now()
	for _, s := range all_slots {
		if s.QueueTime.After(current_timeslot) {
			next = s
			found = true
			break
		}
	}

	if !found {
		log.Error().Err(res.Error).Str("moment", moment).Msg("[GetNextSlot] not found")
		return next, fmt.Errorf("next slot not found")
	}

	var playlists []Playlist
	err := store.db.Model(&next).Order("schedule_time asc").Association("Playlists").Find(&playlists)
	next.Playlists = playlists

	log.Debug().Uint("ID", next.ID).Str("queue_time", next.QueueTime.String()).Str("moment", moment).Int("playlists", len(next.Playlists)).Int("slots", int(res.RowsAffected)).Msg("[GetNextSlot] found")
	return next, err
}
