package crate

import (
	"errors"
	"fmt"
	"station/utils"
	"strconv"
	"time"

	"gorm.io/gorm"
)

type Palette struct {
	ID         uint           `gorm:"primarykey"`
	CreatedAt  time.Time      `json:"createdAt"`
	UpdatedAt  time.Time      `json:"updatedAt"`
	DeletedAt  gorm.DeletedAt `gorm:"index" json:"deletedAt"`
	Moment     string         `json:"moment"`
	Background string         `validate:"hexcolor" json:"background" gorm:"default:#eee"`
	Foreground string         `validate:"hexcolor" json:"foreground" gorm:"default:#8f8f8f"`
	Highlight  string         `validate:"hexcolor" json:"highlight" gorm:"default:#000"`
}

func (p *Palette) BeforeCreate(tx *gorm.DB) error {
	existing := store.db.Model(&Palette{}).Where("background = ? AND foreground = ? AND highlight = ?", p.Background, p.Foreground, p.Highlight)
	if existing.RowsAffected != 0 {
		return errors.New("palette already exists")
	}

	return nil
}

func CreatePalette(p Palette) (Palette, error) {
	err := store.db.Create(&p).Error
	return p, err
}

func GetAllPalettes() ([]Palette, error) {
	var palettes []Palette
	tx := store.db.Model(&Palette{}).Find(&palettes)
	return palettes, tx.Error
}

// GetPalette takes a playlist color and initializes a palette with the base moment colors
// If such a palette exists, it returns the palette, otherwise it creates it and returns it
func GetPalette(highlight string, moment string) (Palette, error) {
	if !utils.IsValidMoment(moment) {
		return Palette{}, fmt.Errorf("the moment is not valid: %s", moment)
	}

	var p Palette
	if highlight == "" {
		p = GetDefaultPalette(moment)
	} else {
		p = Palette{
			Moment:     moment,
			Background: GetDefaultColor(moment, COLOR_BACKGROUND).Value,
			Foreground: GetDefaultColor(moment, COLOR_FOREGROUND).Value,
			Highlight:  highlight,
		}
	}

	existing, err := GetPaletteByColor(p.Background, p.Foreground, p.Highlight)
	if err == gorm.ErrRecordNotFound {
		return CreatePalette(p)
	} else {
		return existing, err
	}
}

func GetPaletteByID(id string) (Palette, error) {
	var palette Palette
	_, err := strconv.Atoi(id)
	if err != nil {
		return palette, err
	}
	err = store.db.Model(&Palette{}).Where("id = ?", id).First(&palette).Error
	return palette, err
}

func GetPaletteByColor(bg string, fg string, hg string) (Palette, error) {
	var palette Palette
	err := store.db.Model(&Palette{}).Where("background = ? AND foreground = ? AND highlight = ?", bg, fg, hg).First(&palette).Error
	return palette, err
}

// RemovePalette first sets all existing Playlists with this Palette to the default palette, then deletes the palette
func RemovePalette(id uint) error {
	var playlists []Playlist
	playlists, err := GetPlaylistsByPalette(id)
	if err != nil {
		return err
	}

	for _, p := range playlists {
		p.Palette = GetDefaultPalette(p.Moment)

		_, err := EditPlaylist(p)
		if err != nil {
			return err
		}
	}

	return DeletePalette(id)
}

func DeletePalette(id uint) error {
	tx := store.db.Delete(&Palette{}, id)
	if tx.Error != nil {
		return tx.Error
	} else if tx.RowsAffected == 0 {
		return fmt.Errorf("could not find palette to delete %d", id)
	}
	return nil
}

// GetDefaultPalette looks for a specific exisiting palette to be used if there are no others
func GetDefaultPalette(moment string) Palette {
	return Palette{
		Moment:     moment,
		Background: GetDefaultColor(moment, COLOR_BACKGROUND).Value,
		Foreground: GetDefaultColor(moment, COLOR_FOREGROUND).Value,
		Highlight:  GetDefaultColor(moment, COLOR_HIGHLIGHT).Value,
	}
}

// IsDefaultPalette returns true if a palette has the GORM default colors or empty colors
func IsDefaultPalette(p Palette) bool {
	return (p.Background == "#eee" && p.Foreground == "#8f8f8f" && p.Highlight == "#000") || (p.Background == "" && p.Foreground == "" && p.Highlight == "")
}
