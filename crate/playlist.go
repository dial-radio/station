package crate

import (
	"fmt"
	"os"
	"path/filepath"
	"station/config"
	"station/utils"
	"time"

	"github.com/google/uuid"
	"github.com/gosimple/slug"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

// the playlist is what is submitted by a user, is made up of Tracks and is fetched by a Slot
type Playlist struct {
	ID           uint           `gorm:"primarykey"`
	CreatedAt    time.Time      `json:"createdAt"`
	UpdatedAt    time.Time      `json:"updatedAt"`
	DeletedAt    gorm.DeletedAt `gorm:"index" json:"deletedAt"`
	UUID         uuid.UUID      `gorm:"type:uuid"`
	LastPlayedAt time.Time      `json:"lastPlayedAt"`
	Description  string         `form:"description" json:"description"`
	Email        string         `form:"email" json:"email" validate:"required,email"`
	Color        string         `gorm:"default:#F2F0E5" form:"color" json:"color"`
	Link         string         `form:"link" json:"link"`
	InstaHandle  string         `form:"insta_handle" json:"instaHandle"`
	Moment       string         `form:"moment" json:"moment" validate:"required"`
	URL          string         `gorm:"index" form:"url" json:"url" validate:"required"`
	Status       string         `gorm:"default:pending" json:"status"`
	Type         string         `gorm:"default:contribution" json:"type"`
	Artist       string         `gorm:"default:unknown" form:"artist" json:"artist" validate:"required,gte=0,lte=24"`
	Title        string         `gorm:"default:untitled" form:"title" json:"title" validate:"required,gte=0,lte=48"`
	Slug         string         `json:"slug"`
	Filename     string         `form:"filename" json:"filename"`
	Tracks       []Track        `gorm:"foreignKey:PlaylistID;references:ID" json:"tracks"`
	Image        string         `json:"image"`
	PlayCount    int            `json:"playCount"`
	Slots        []Slot         `gorm:"many2many:slot_playlists;" json:"slots"`
	ScheduleTime time.Time      `json:"scheduleTime"` // the time within a slot at which the playlist will start
	Duration     time.Duration  `json:"duration"`
	PaletteID    uint           `json:"paletteID"`
	Palette      Palette        `json:"palette"`
}

func (p *Playlist) BeforeCreate(tx *gorm.DB) (err error) {
	if p.UUID == uuid.Nil {
		p.UUID = uuid.New()
	}

	if p.Title == "" {
		p.Title = fmt.Sprintf("untitled #%s (%s)", p.UUID.String()[:8], p.Moment)
	}

	return nil
}

func (p *Playlist) AfterCreate(tx *gorm.DB) (err error) {
	if p.Slug == "" {
		p.Slug = fmt.Sprintf("%d-%s", p.ID, slug.Make(p.Title))
	}
	tx.Model(p).Update("slug", p.Slug)

	if p.Filename == "" {
		p.Filename = GenerateFilename(p, config.Audio().Format)
	}
	tx.Model(p).Update("filename", p.Filename)

	return
}

// generateFilename returns a string for the audio file attached to a playlist, given a playlist title and moment
func GenerateFilename(p *Playlist, format string) string {
	title := slug.Make(p.Title)
	hash := p.UUID.String()[:8]
	fn := fmt.Sprintf("%d_%s_%s_%s.%s", p.ID, title, p.Moment, hash, format)
	return fn
}

// GetPlaylistsBy is a generic interface for playlists queries
func GetPlaylistsBy(key string, value string, playlistType string, page int) ([]Playlist, error) {
	var playlists []Playlist
	var err error

	page_size := config.Api().PlaylistPageSize
	page_offset := 0
	if page == -1 { // do not limit results
		page_size = -1
		page_offset = -1
	} // else paginate

	if key == "" && value == "" { // get all playlists
		if playlistType == utils.Selection || playlistType == utils.Contribution {
			err = store.db.Limit(page_size).Offset(page_offset).Model(&Playlist{}).Where("type = ?", playlistType).Find(&playlists).Error
		} else {
			err = store.db.Limit(page_size).Offset(page_offset).Model(&Playlist{}).Find(&playlists).Error
		}
	} else { // filter by key
		err = store.db.Limit(page_size).Offset(page_offset).Model(&Playlist{}).Where(fmt.Sprintf("%s = ?", key), value).Where("type = ?", playlistType).Find(&playlists).Error
	}
	if err != nil {
		return playlists, err
	}

	for i, p := range playlists {
		err := store.db.Model(&p).Association("Palette").Find(&playlists[i].Palette)
		if err != nil {
			return playlists, err
		}
	}

	return playlists, err
}

func GetPagedContributionPlaylists(page int) ([]Playlist, error) {
	return GetPlaylistsBy("", "", utils.Contribution, page)
}

// GetContributionPlaylists returns all non-Selection playlists
func GetContributionPlaylists() ([]Playlist, error) {
	return GetPlaylistsBy("", "", utils.Contribution, -1)
}

// GetSelectionPlaylists returns only the Selection playlists
func GetSelectionPlaylists() ([]Playlist, error) {
	return GetPlaylistsBy("", "", utils.Selection, -1)
}

// GetPlaylistsByMoment returns the unpaged contribution playlists by moment, without tracks or slots
func GetPlaylistsByMoment(moment string) ([]Playlist, error) {
	return GetPlaylistsBy("moment", moment, utils.Contribution, -1)
}

// GetPlaylistsbyStatus returns the unpaged contribution playlists by status, without tracks
func GetPlaylistsByStatus(status string) ([]Playlist, error) {
	return GetPlaylistsBy("status", status, utils.Contribution, -1)
}

func GetPlaylistsByPalette(id uint) ([]Playlist, error) {
	return GetPlaylistsBy("palette_id", fmt.Sprintf("%d", id), utils.Contribution, -1)
}

// GetLeastPlayedPlaylists returns all Playlists for a given Moment and which can be played.  It prioritizes the least played Playlists by ordering the query by ascending LastPlayedAt and ascending CreatedAt (i.e. get the oldest first), and only selects those that are utils.InLibrary
func GetLeastPlayedPlaylists(moment string) ([]Playlist, error) {
	var playlists []Playlist
	tx := store.db.Preload("Tracks").Model(&Playlist{}).Where("moment = ? and status = ? and type != ?", moment, utils.InLibrary, utils.Selection).Order("last_played_at asc").Order("created_at asc").Find(&playlists)
	return playlists, tx.Error
}

func GetPlaylistBy(column string, value string, includeTracks bool) (Playlist, error) {
	var p Playlist
	if includeTracks {
		err := store.db.Model(&Playlist{}).Preload("Tracks").Where(fmt.Sprintf("%s = ?", column), value).First(&p).Error
		if err != nil {
			return p, err
		}
	} else {
		err := store.db.Model(&Playlist{}).Where(fmt.Sprintf("%s = ?", column), value).First(&p).Error
		if err != nil {
			return p, err
		}
	}

	err := store.db.Model(&p).Association("Palette").Find(&p.Palette)
	return p, err
}

func FindPlaylist(query string) ([]Playlist, error) {
	var p []Playlist
	val := fmt.Sprintf("%%%s%%", query)
	err := store.db.Model(&Playlist{}).Where("title LIKE ?", val).Or("artist LIKE ?", val).Or("url LIKE ?", val).Or("description LIKE ?", val).Find(&p).Error
	return p, err
}

func CreatePlaylist(playlist Playlist) (Playlist, error) {
	err := store.db.Create(&playlist).Error
	return playlist, err
}

func EditPlaylist(new Playlist) (Playlist, error) {
	var old Playlist
	tx := store.db.Model(&Playlist{}).Where("id = ?", new.ID).First(&old)
	if tx.Error != nil {
		return Playlist{}, tx.Error
	} else if tx.RowsAffected == 0 {
		return Playlist{}, fmt.Errorf("could not find playlist with ID: %d", new.ID)
	}

	// if the moment is different, handle file name change
	if old.Moment != new.Moment {
		new.Filename = GenerateFilename(&new, config.Audio().Format)
		new_path := filepath.Join(config.LibraryDir(), new.Moment, new.Filename)
		old_path := filepath.Join(config.LibraryDir(), old.Moment, old.Filename)
		err := os.Rename(old_path, new_path)
		if err != nil {
			return Playlist{}, err
		}
	}

	err := store.db.Updates(&new).Error
	if err != nil {
		return Playlist{}, err
	}

	updated, err := GetPlaylistBy("id", fmt.Sprintf("%d", new.ID), true)
	return updated, err
}

// EditPlaylistTracks updates both the playlist and the tracks associations
func AppendPlaylistTracks(playlist Playlist, track Track) (Playlist, error) {
	err := store.db.Updates(&playlist).Error
	if err != nil {
		return playlist, err
	}
	err = store.db.Model(&playlist).Association("Tracks").Append(&track)
	return playlist, err
}

func ReplacePlaylistTracks(playlist Playlist) (Playlist, error) {
	err := store.db.Model(&playlist).Association("Tracks").Replace(playlist.Tracks)
	return playlist, err
}

func DeletePlaylist(id string) (string, error) {
	var p Playlist
	err := store.db.Model(Playlist{}).Where("id = ?", id).First(&p).Error
	if err != nil {
		return id, err
	}
	tx := store.db.Delete(&Playlist{}, id)
	if tx.Error != nil {
		return "0", tx.Error
	} else if tx.RowsAffected == 0 {
		return "0", fmt.Errorf("could not find playlist with ID: %s", id)
	}
	log.Warn().Str("filename", p.Filename).Msg("[DeletePlaylist] removing file")
	err = os.Remove(filepath.Join(config.LibraryDir(), p.Moment, p.Filename))
	return id, err
}
