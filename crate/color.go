package crate

import (
	"fmt"
	"strconv"
	"time"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

const (
	COLOR_BACKGROUND = "background"
	COLOR_FOREGROUND = "foreground"
	COLOR_HIGHLIGHT  = "highlight"
)

const (
	defaultForeground = "#8f8f8f"
	defaultBackground = "#eee"
	defaultHighlight  = "#111"
)

type Color struct {
	ID        uint           `gorm:"primarykey"`
	CreatedAt time.Time      `json:"createdAt"`
	UpdatedAt time.Time      `json:"updatedAt"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deletedAt"`
	Moment    string         `json:"moment" form:"moment"`
	Type      string         `json:"type"`
	Value     string         `validate:"hexcolor" json:"value" form:"value" gorm:"default:#727272"`
}

func CreateColor(c Color) (Color, error) {
	err := store.db.Create(&c).Error
	return c, err
}

func GetAllColors() ([]Color, error) {
	var colors []Color
	tx := store.db.Model(&Color{}).Find(&colors)
	return colors, tx.Error
}

func GetColor(id string) (Color, error) {
	var color Color
	_, err := strconv.Atoi(id)
	if err != nil {
		return color, err
	}
	err = store.db.Model(&Color{}).Where("id = ?", id).First(&color).Error
	return color, err
}

func DeleteColor(id uint) error {
	tx := store.db.Delete(&Color{}, id)
	if tx.Error != nil {
		return tx.Error
	} else if tx.RowsAffected == 0 {
		return fmt.Errorf("could not find color to delete %d", id)
	}
	return nil
}

// GetDefaultColor takes a moment and type and returns the first color
func GetDefaultColor(m string, t string) Color {
	var c Color
	err := store.db.Model(&Color{}).Where("moment = ? AND type = ?", m, t).First(&c).Error
	if err != nil {
		log.Error().Err(err).Str("moment", m).Str("type", t).Msg("[GetDefaultColor]")
		switch t {
		case COLOR_BACKGROUND:
			return Color{Moment: m, Value: defaultBackground}
		case COLOR_FOREGROUND:
			return Color{Moment: m, Value: defaultForeground}
		case COLOR_HIGHLIGHT:
			return Color{Moment: m, Value: defaultHighlight}
		}
	}
	return c
}
