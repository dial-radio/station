package crate

import (
	"station/config"
	"station/utils"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

// Init checks for whether each Moment has a Slot scheduled, and if each of these Slots are properly queued (e.g. have files on rotation). It then airs a Slot based on the station Moment
func initSlots() error {

	var moments = []string{utils.Morning, utils.Afternoon, utils.Evening, utils.Night}
	today := utils.GetTodayDate()
	for i, m := range moments {
		offset := time.Hour * time.Duration((i+1)*6)
		p, err := GetPlaylistsByMoment(m)
		if err != nil {
			panic(err)
		}
		s := Slot{
			Moment:    m,
			QueueTime: today.Add(offset),
			Playlists: []Playlist{p[0]},
		}
		if _, err := CreateSlot(s); err != nil {
			log.Error().Err(err).Msg("[Init] populate queue slots")
			return err
		}
	}

	return nil
}

func setup(runFixtures bool, runInit bool) func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	if err := OpenDB(utils.Memory, true); err != nil {
		panic(err)
	}

	if runFixtures {
		if err := RunFixtures(); err != nil {
			panic(err)
		}
	}

	// should replace init with some sort of slot creation mechanism, or even just move init here
	if runInit {
		if err := initSlots(); err != nil {
			panic(err)
		}
	}

	return func(t *testing.T) {}
}

func TestDBSetup(t *testing.T) {
	t.Run("open DB with no arguments", func(t *testing.T) {
		err := OpenDB("", true)
		require.Nil(t, err)
	})
}
