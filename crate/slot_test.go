package crate

import (
	"fmt"
	"station/utils"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestSlotOperations(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Get slot", func(t *testing.T) {
		slt, err := GetSlot("1")
		require.Nil(t, err)
		assert.NotEqual(t, "", slt.Moment)
		assert.Greater(t, len(slt.Playlists), 0)
	})

	t.Run("Get slot with wrong ID", func(t *testing.T) {
		slt, err := GetSlot("666")
		require.NotNil(t, err)
		assert.Equal(t, uint(0), slt.ID)
	})

	t.Run("Get all slots", func(t *testing.T) {
		page := 0
		slt, err := GetAllSlots(page)
		require.Nil(t, err)
		assert.NotEqual(t, 0, len(slt))
		assert.NotEqual(t, "", slt[0].Moment)
	})
}

func TestDeleteSlotOperations(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)
	var deleteSlot Slot

	t.Run("Delete slot", func(t *testing.T) {
		slots, err := GetQueuedSlots()
		if err != nil {
			t.Error(err)
		}

		deleteSlot = slots[len(slots)-1]
		playlists := slots[len(slots)-1].Playlists
		followingSlot, err := CreateSlot(Slot{
			Moment:    slots[len(slots)-1].Moment,
			QueueTime: slots[len(slots)-1].QueueTime.Add(time.Hour * 24),
		})
		if err != nil {
			t.Error(err)
		}

		id, err := DeleteSlot(fmt.Sprint(deleteSlot.ID))
		require.Nil(t, err)
		assert.Equal(t, fmt.Sprint(deleteSlot.ID), id)

		// check that the playlists are indeed InLibrary
		for _, playlist := range playlists {
			p, err := GetPlaylistBy("id", fmt.Sprint(playlist.ID), false)
			require.Nil(t, err)
			assert.Equal(t, utils.InLibrary, p.Status)
		}

		// // check the new slot has been pushed back 24h
		followingSlot, err = GetSlot(fmt.Sprint(followingSlot.ID))
		require.Nil(t, err)
		assert.True(t, followingSlot.QueueTime.Day() == deleteSlot.QueueTime.Day())

	})

	t.Run("Delete non-existing slot ID", func(t *testing.T) {
		_, err := DeleteSlot(fmt.Sprint(deleteSlot.ID))
		require.NotNil(t, err)
	})
}

func TestSlotEdit(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Edit slot", func(t *testing.T) {
		new_slot := Slot{}
		new_slot.ID = 1
		new_slot.Moment = utils.Evening
		slt, err := EditSlot(new_slot)
		require.Nil(t, err)
		assert.Equal(t, utils.Evening, slt.Moment)
	})

	t.Run("Get slot", func(t *testing.T) {
		slt, err := GetSlot("1")
		require.Nil(t, err)
		assert.Equal(t, utils.Evening, slt.Moment)
	})
}

func TestSlotsQueries(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Get queued slots", func(t *testing.T) {
		queued, err := GetQueuedSlots()
		require.Nil(t, err)
		assert.Greater(t, len(queued), 0)
	})

	t.Run("Get scheduled slots", func(t *testing.T) {
		queued, err := GetQueuedSlots()
		require.Nil(t, err)

		scheduled, err := GetSchedule()
		require.Nil(t, err)
		assert.GreaterOrEqual(t, len(scheduled), len(queued))
	})

	t.Run("Get queued slots by moment", func(t *testing.T) {
		slts, err := GetQueuedSlotsByMoment(utils.Evening)
		require.Nil(t, err)
		assert.Greater(t, len(slts), 0)
		assert.Greater(t, len(slts[0].Playlists), 0)
	})

	t.Run("Get past slots", func(t *testing.T) {
		_, err := GetPastSlots()
		require.Nil(t, err)
	})

	t.Run("Get slot on air", func(t *testing.T) {
		slt, err := GetSlotOnAir()
		require.Nil(t, err)
		assert.NotEqual(t, uint(0), slt.ID)
	})

	t.Run("Get next slot", func(t *testing.T) {
		slt, err := GetNextSlot(utils.Night)
		require.Nil(t, err)
		assert.NotEqual(t, uint(0), slt.ID)
	})

}
