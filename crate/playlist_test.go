package crate

import (
	"fmt"
	"station/utils"
	"testing"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

const EXPECTED_PLAYLIST_TOTAL = 12

func TestGetPlaylistOperations(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	var playlist Playlist
	var err error
	t.Run("Get playlist", func(t *testing.T) {
		playlist, err = GetPlaylistBy("id", "1", true)
		require.Nil(t, err)
		assert.Equal(t, "fixture morning 1", playlist.Description)
		assert.Equal(t, 1, len(playlist.Tracks))
	})

	t.Run("Get playlist by URL", func(t *testing.T) {
		playlist, err = GetPlaylistBy("url", playlist.URL, false)
		require.Nil(t, err)
		assert.Equal(t, "fixture morning 1", playlist.Description)
		assert.Equal(t, 0, len(playlist.Tracks))
	})

	t.Run("Get non-existing playlist by URL", func(t *testing.T) {
		playlist, err = GetPlaylistBy("url", "/tmp/radio/test_long.ogg", false)
		require.NotNil(t, err)
	})

	t.Run("Get playlist with wrong ID", func(t *testing.T) {
		wrong, err := GetPlaylistBy("id", "666", true)
		require.NotNil(t, err)
		assert.Equal(t, uint(0), wrong.ID)
	})

	t.Run("Get all playlists", func(t *testing.T) {
		playlists, err := GetContributionPlaylists()
		require.Nil(t, err)
		assert.Equal(t, EXPECTED_PLAYLIST_TOTAL, len(playlists))
		assert.Equal(t, utils.Morning, playlists[0].Moment)
		assert.NotEqual(t, 0, playlists[0].Palette.ID)
	})

	t.Run("Get all playlists by moment", func(t *testing.T) {
		playlists, err := GetPlaylistsByMoment(utils.Morning)
		require.Nil(t, err)
		assert.Equal(t, 3, len(playlists))
		assert.Equal(t, utils.Morning, playlists[0].Moment)
	})

	t.Run("Get least played playlists by moment", func(t *testing.T) {
		playlists, err := GetLeastPlayedPlaylists(utils.Morning)
		require.Nil(t, err)
		require.GreaterOrEqual(t, len(playlists), 1)
		assert.Equal(t, utils.Morning, playlists[0].Moment)
	})
}

func TestDeletePlaylistOperations(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Delete playlist", func(t *testing.T) {
		id, err := DeletePlaylist("1")
		require.Nil(t, err)
		assert.Equal(t, "1", id)
	})

	t.Run("Delete playlist with wrong ID", func(t *testing.T) {
		_, err := DeletePlaylist("1")
		require.NotNil(t, err)
	})
}

func TestPlaylistEdit(t *testing.T) {
	teardown := setup(true, true)
	defer teardown(t)

	t.Run("Edit playlist with same moment", func(t *testing.T) {
		existing, err := GetPlaylistBy("id", "1", true)
		require.Nil(t, err)

		new_playlist := Playlist{}
		new_playlist.ID = 1
		new_playlist.Description = "edited"
		new_playlist.Moment = existing.Moment

		playlist, err := EditPlaylist(new_playlist)
		require.Nil(t, err)
		assert.Equal(t, "edited", playlist.Description)
	})

	t.Run("Get playlist", func(t *testing.T) {
		playlist, err := GetPlaylistBy("id", "1", true)
		require.Nil(t, err)
		assert.Equal(t, "edited", playlist.Description)
	})
}

func TestPlaylist_Filename(t *testing.T) {
	var s Playlist
	s.Title = `the new \ way of / doing . science`
	s.ID = 666
	s.UUID = uuid.New()
	s.Moment = utils.Night
	want := fmt.Sprintf("666_the-new-way-of-doing-science_night_%s.ogg", s.UUID.String()[:8])

	t.Run("check correct filename generation", func(t *testing.T) {
		s.Filename = GenerateFilename(&s, "ogg")
		assert.Equal(t, want, s.Filename)
	})
}
