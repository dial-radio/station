package crate

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"station/config"
	"station/utils"
	"time"

	"github.com/gosimple/slug"
	"github.com/rs/zerolog/log"
	"gopkg.in/yaml.v2"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

type Storage struct {
	db *gorm.DB
}

var store = Storage{
	db: &gorm.DB{},
}

// OpenDB takes the name of the database file to open, and a boolean determining whether we should auto-migrate models
func OpenDB(db_name string, shouldAutoMigrate bool) error {
	var err error

	if db_name == utils.Memory || db_name == "" {
		if db_name == "" {
			log.Warn().Str("default", config.Conf().DBPath).Msg("[OpenDB] db_name not set, opening in memory")
		}

		store.db, err = gorm.Open(sqlite.Open("file::memory:?cache=shared&busy_timeout=5000"), &gorm.Config{Logger: logger.Default.LogMode(logger.Silent)})
		if err != nil {
			panic(err)
		}
		actualDB, _ := store.db.DB()
		actualDB.SetMaxOpenConns(1)

		err = store.db.AutoMigrate(&Slot{}, &Playlist{}, &Track{}, &Palette{}, &Color{})
		if err != nil {
			return err
		}

		return nil
	}

	log.Info().Str("db_path", db_name).Msg("[OpenDB] connecting to")
	c := &gorm.Config{Logger: logger.Default.LogMode(logger.Warn)}
	if config.Conf().APIMode == "release" {
		c.Logger = logger.Default.LogMode(logger.Silent)
	}

	store.db, err = gorm.Open(sqlite.Open(db_name), c)
	if err != nil {
		return err
	}

	sqlDB, err := store.db.DB()
	if err != nil {
		return err
	}

	sqlDB.SetMaxIdleConns(0)
	sqlDB.SetMaxOpenConns(200)
	sqlDB.SetConnMaxLifetime(time.Minute * 10)

	if shouldAutoMigrate {
		log.Warn().Msg("[OpenDB] automigrating")
		if err := store.db.AutoMigrate(&Slot{}, &Playlist{}, &Track{}, &Palette{}, &Color{}); err != nil {
			return err
		}
	}
	return nil
}

func Migrate(target string) error {
	if target == "slug" {
		playlists, err := GetContributionPlaylists()
		if err != nil {
			return err
		}
		selections, err := GetSelectionPlaylists()
		if err != nil {
			return err
		}

		all := append(playlists, selections...)
		for _, p := range all {
			if p.Slug == "" {
				p.Slug = fmt.Sprintf("%d-%s", p.ID, slug.Make(p.Title))
			}

			_, err := EditPlaylist(p)
			if err != nil {
				return err
			}
		}
	} else {
		log.Warn().Str("target", target).Msg("[Migrate] unknown target")
	}

	return nil
}

// -------------------------
//
// FIXTURES
//
// -------------------------

func RunFixtures() error {
	log.Info().Msg("[RunFixtures] running fixtures...")

	tables := []string{"slots", "playlists", "slot_playlists", "tracks", "palettes", "colors"}
	for _, t := range tables {
		if err := store.db.Migrator().DropTable(t); err != nil {
			return err
		}
	}

	err := store.db.AutoMigrate(&Track{}, &Playlist{}, &Slot{}, &Palette{}, &Color{})
	if err != nil {
		return err
	}

	//-- run colors fixtures
	if err := CreateColorFixtures(); err != nil {
		return err
	}

	//-- run playlists fixtures
	{

		bytes, err := os.ReadFile(filepath.Join(Basepath, "fixtures", config.Station().FixturesFiles))
		if err != nil {
			return err
		}

		playlists := make([]Playlist, 0)
		err = yaml.Unmarshal(bytes, &playlists)
		if err != nil {
			return err
		}

		for _, playlist := range playlists {
			playlist.Status = utils.InLibrary
			for _, t := range playlist.Tracks {
				playlist.Duration += t.Duration
			}

			playlist.Palette = GetDefaultPalette(playlist.Moment)
			err = store.db.Create(&playlist).Error
			if err != nil {
				return err
			}

			src := filepath.Join(Basepath, "fixtures/playlists/", playlist.Moment, playlist.Filename)
			dst := filepath.Join(config.LibraryDir(), playlist.Moment, playlist.Filename)
			err = utils.CopyFile(src, dst)
			if err != nil {
				return err
			}
		}
	}

	//-- copy the fallbacks
	src := filepath.Join(Basepath, "../files/fallback.ogg")
	dst := filepath.Join(config.SelectionsDir(), "fallback.ogg")
	err = utils.CopyFile(src, dst)
	if err != nil {
		panic(err)
	}

	moments := []string{"morning", "afternoon", "evening", "night"}
	for _, m := range moments {
		src = filepath.Join(Basepath, fmt.Sprintf("../files/fallback_%s.png", m))
		dst = filepath.Join(config.ImageDir(), fmt.Sprintf("fallback_%s.png", m))
		err = utils.CopyFile(src, dst)
		if err != nil {
			panic(err)
		}
	}

	return nil
}

// createColors creates the default colors in the database
func CreateColorFixtures() error {
	morningColors := []Color{{
		Moment: utils.Morning,
		Type:   COLOR_BACKGROUND,
		Value:  "#8eb3bf",
	}, {
		Moment: utils.Morning,
		Type:   COLOR_FOREGROUND,
		Value:  "#6c919e",
	}, {
		Moment: utils.Morning,
		Type:   COLOR_HIGHLIGHT,
		Value:  "#546695",
	}}

	if err := store.db.Create(&morningColors).Error; err != nil {
		return err
	}

	afternoonColors := []Color{
		{
			Moment: utils.Afternoon,
			Type:   COLOR_BACKGROUND,
			Value:  "#f9dd48",
		},
		{
			Moment: utils.Afternoon,
			Type:   COLOR_FOREGROUND,
			Value:  "#F5EDA4",
		},
		{
			Moment: utils.Afternoon,
			Type:   COLOR_HIGHLIGHT,
			Value:  "#77A4DA",
		}}

	if err := store.db.Create(&afternoonColors).Error; err != nil {
		return err
	}

	eveningColors := []Color{
		{
			Moment: utils.Evening,
			Type:   COLOR_BACKGROUND,
			Value:  "#d06189",
		},
		{
			Moment: utils.Evening,
			Type:   COLOR_FOREGROUND,
			Value:  "#f47052",
		},
		{
			Moment: utils.Evening,
			Type:   COLOR_HIGHLIGHT,
			Value:  "#FF92D3",
		}}

	if err := store.db.Create(&eveningColors).Error; err != nil {
		return err
	}

	nightColors := []Color{
		{
			Moment: utils.Night,
			Type:   COLOR_BACKGROUND,
			Value:  "#262626",
		},
		{
			Moment: utils.Night,
			Type:   COLOR_FOREGROUND,
			Value:  "#565656",
		},
		{
			Moment: utils.Night,
			Type:   COLOR_HIGHLIGHT,
			Value:  "#F64141",
		}}

	if err := store.db.Create(&nightColors).Error; err != nil {
		return err
	}

	return nil
}
