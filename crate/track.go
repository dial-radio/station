package crate

import (
	"time"

	"gorm.io/gorm"
)

type Track struct {
	ID          uint           `gorm:"primarykey"`
	CreatedAt   time.Time      `json:"createdAt"`
	UpdatedAt   time.Time      `json:"updatedAt"`
	DeletedAt   gorm.DeletedAt `gorm:"index" json:"deletedAt"`
	Duration    time.Duration  `gorm:"default:0" json:"duration"`
	Artist      string         `gorm:"default:unkown artist" form:"artist" json:"artist" validate:"lte=100"`
	Title       string         `gorm:"default:untitled" form:"title" json:"title" validate:"lte=100"`
	Filename    string         `json:"filename"` // full path
	PlaylistID  uint           `json:"playlistID"`
	Playlist    Playlist       `gorm:"foreignKey:PlaylistID;references:ID" json:"-"`
	Type        string         `gorm:"default:contribution" json:"type"`
	Contributor string         `gorm:"default:Dial Team" json:"contributor"`
	Moment      string         `json:"moment" validate:"required"`
	PlayCount   int            `json:"playCount"`
}
