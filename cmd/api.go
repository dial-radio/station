package cmd

import (
	"context"
	"os"
	"os/signal"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/library"
	"station/station"
	"station/utils"
	"syscall"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// apiCmd represents the api command
var apiCmd = &cobra.Command{
	Use:   "api",
	Short: "Starts the API",
	Long:  `Station API starts an HTTP server communicating with any public clients and the stream broadcaster.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.InfoLevel)
		log.Info().Str("version", "1.0").Msg("Radio station")

		if err := config.SyncWithBroadcaster(); err != nil {
			log.Error().Err(err).Msg("[api]")
		}

		// start database
		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}

		if config.Conf().Station.RunFixtures && config.Conf().APIMode != "release" {
			if err := crate.RunFixtures(); err != nil {
				return err
			}
		}

		// start the station
		err := station.Init()
		if err != nil {
			log.Error().Err(err).Msg("[main] failed to init station")
			return err
		}

		// start HTTP server
		server := library.Start()

		ch := make(chan os.Signal, 3)
		signal.Notify(ch, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)
		<-ch // block until signal received

		cleanup_paths := []string{
			filepath.Join(config.ExtractDir()),
		}
		utils.CleanupDirs(cleanup_paths)

		log.Info().Msg("[main] shutting down server")
		server.Shutdown(context.Background())

		time.Sleep(1 * time.Second)
		return nil
	},
}

func init() {
	rootCmd.AddCommand(apiCmd)
}
