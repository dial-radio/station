package cmd

import (
	"station/config"
	"station/crate"
	"station/station"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

var format string
var encoder string

// encodeCmd represents the encode command
var encodeCmd = &cobra.Command{
	Use:     "encode [id id id id] [--format --encoder]",
	Short:   "Encodes an arbitrary number of playlists to a specific format. Defaults to the format and encoder found in the configuration file.",
	Long:    ``,
	Example: `station playlist encode 666 667 2045 --format ogg --encoder libvorbis`,
	Args:    cobra.MinimumNArgs(1),
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.InfoLevel)

		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}

		for _, arg := range args {
			playlist, err := crate.GetPlaylistBy("id", arg, false)
			if err != nil {
				return err
			}

			if err := station.Encode(&playlist, encoder, format); err != nil {
				return err
			}
		}
		return nil
	},
}

func init() {
	playlistCmd.AddCommand(encodeCmd)

	encodeCmd.Flags().StringVar(&format, "format", "", "Format to encode to (e.g. ogg, mp3, aac). Defaults to config.Audio().Format")
	encodeCmd.Flags().StringVar(&encoder, "encoder", "", "Encoder to use (e.g. libvorbis, libmp3lame, libfdk_aac). Defaults to config.Audio().Encoder")
}
