/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"station/config"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Displays the current configuration",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		format := cmd.Flag("format")

		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.Disabled)

		conf := config.Conf()
		switch format.Value.String() {
		case "raw":
			fmt.Printf("%+v", conf)
		case "json":
			asJson, err := json.Marshal(conf)
			if err != nil {
				return err
			}

			os.Stdout.Write(asJson)

		case "pretty":
			indent, err := json.MarshalIndent(conf, "", "  ")
			if err != nil {
				return err
			}

			fmt.Println(string(indent))
		}

		return nil
	},
}

func init() {
	showCmd.AddCommand(configCmd)
	configCmd.Flags().StringP("format", "f", "raw", "output format: raw, pretty, json")
}
