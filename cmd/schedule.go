package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"station/config"
	"station/crate"
	"station/station"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// scheduleCmd represents the schedule command
var scheduleCmd = &cobra.Command{
	Use:   "schedule",
	Short: "Displays the schedule",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.Disabled)
		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}

		format := cmd.Flag("format")
		schedule, err := station.GetSchedule()
		if err != nil {
			return nil
		}
		switch format.Value.String() {
		case "raw":
			fmt.Printf("%+v", schedule)
		case "json":
			asJson, err := json.Marshal(schedule)
			if err != nil {
				return err
			}
			os.Stdout.Write(asJson)
		case "pretty":
			indent, err := json.MarshalIndent(schedule, "", "  ")
			if err != nil {
				return err
			}
			fmt.Println(string(indent))
		}

		return nil
	},
}

func init() {
	showCmd.AddCommand(scheduleCmd)
	scheduleCmd.Flags().StringP("format", "f", "raw", "output format: raw, pretty, json")
}
