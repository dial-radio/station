package cmd

import (
	"os"
	"station/config"
	"time"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "station",
	Short: "A CLI interface to manage a Dial Radio instance.",
	Long:  `Dial Radio is a worldwide playlist broadcasting system. The CLI allows for the access and modification of the schedule, slots and playlists of the instance.`,
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

var debug bool
var configPath string

func init() {
	rootCmd.PersistentFlags().BoolVar(&debug, "debug", false, "toggles debug output")
	rootCmd.PersistentFlags().StringVarP(&configPath, "config", "c", "", "specify a path to config.toml")
}

// initConf checks for a possible user-provided path to find the config file
// if so, writes it to the env
// then loads the config
func initConf() error {
	_, err := config.Init(configPath)
	return err
}

// initLogs sets up the logging output, and the default log level
func initLogs(defaultLogLevel zerolog.Level) {
	output := zerolog.ConsoleWriter{Out: os.Stdout, TimeFormat: time.RFC3339}
	log.Logger = log.Output(output)

	if config.Conf().LogLevel == "debug" || debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(defaultLogLevel)
	}
}
