package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"station/config"
	"station/crate"
	"strings"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// playlistCmd represents the playlist command
var playlistCmd = &cobra.Command{
	Use:   "playlist",
	Short: "Operate on one or more playlists",
	Long:  ``,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.Disabled)

		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}
		if len(args) == 0 {
			return fmt.Errorf("missing id or query param :/")
		}
		format := cmd.Flag("format")
		showPlaylists(args, format.Value.String())
		return nil
	},
}

func showPlaylists(ids []string, format string) error {
	var results []crate.Playlist
	for _, arg := range ids {
		id := arg
		p, err := crate.GetPlaylistBy("id", string(id), true)
		if err != nil {
			return err
		}

		results = append(results, p)
	}

	if len(results) == 0 {
		fmt.Println("No playlists found :(")
	}

	switch format {
	case "raw":
		for _, r := range results {
			fmt.Printf("%+v", r)
		}
	case "json":
		asJson, err := json.Marshal(results)
		if err != nil {
			return err
		}

		os.Stdout.Write(asJson)
	case "pretty":
		var formatted []string
		for _, r := range results {
			f := fmt.Sprintf(" id:       \t%d\n title:   \t%s\n artist:\t%s\n moment:\t%s\n URL:       \t%s\n filename:\t%s\n status:\t%s\n duration:\t%s", r.ID, r.Title, r.Artist, r.Moment, r.URL, r.Filename, r.Status, r.Duration.String())
			formatted = append(formatted, f)
		}
		fmt.Println(strings.Join(formatted, "\n\n"))
	}

	return nil
}

func init() {
	rootCmd.AddCommand(playlistCmd)
	playlistCmd.Flags().StringP("format", "f", "pretty", "output format: raw, pretty, json")
}
