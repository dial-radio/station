/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"encoding/json"
	"fmt"
	"os"
	"station/config"
	"station/crate"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// findCmd represents the find command
// find should be the one that does the query, instead of show
var findCmd = &cobra.Command{
	Use:     "find",
	Short:   "Finds a playlist given a query string.",
	Args:    cobra.MaximumNArgs(1),
	Long:    ``,
	Example: `station playlist find "morning soundtrack"`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.Disabled)

		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}

		format := cmd.Flag("format")
		var results []crate.Playlist
		rawQuery := args[0]
		if rawQuery == "" {
			return fmt.Errorf("missing query param :/")
		}
		found, err := crate.FindPlaylist(rawQuery)
		if err != nil {
			return err
		}
		results = append(results, found...)

		switch format.Value.String() {
		case "raw":
			for _, r := range results {
				fmt.Printf("%+v", r)
			}
		case "json":
			asJson, err := json.Marshal(results)
			if err != nil {
				return err
			}

			os.Stdout.Write(asJson)
		case "pretty":
			for _, r := range results {
				output := fmt.Sprintf(" id:       \t%d\n title:   \t%s\n artist:\t%s\n moment:\t%s\n URL:       \t%s\n filename:\t%s\n status:\t%s\n duration:\t%s\n ", r.ID, r.Title, r.Artist, r.Moment, r.URL, r.Filename, r.Status, r.Duration.String())
				fmt.Println(output)
			}
			fmt.Printf("Found %d result(s).\n", len(results))

		}

		return nil
	},
}

func init() {
	playlistCmd.AddCommand(findCmd)
	findCmd.Flags().StringP("format", "f", "pretty", "output format: raw, pretty, json")
}
