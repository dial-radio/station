/*
Copyright © 2024 NAME HERE <EMAIL ADDRESS>
*/
package cmd

import (
	"fmt"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/station"
	"station/utils"

	"github.com/rs/zerolog"
	"github.com/spf13/cobra"
)

// prepareCmd represents the prepare command
var prepareCmd = &cobra.Command{
	Use:   "prepare [id]",
	Short: "Prepares a playlist for playback",
	Long: `Prepare takes the ID of an existing playlist and runs the through the preparations:
	
	1. copies the track files to a temporary folder
	2. encodes the track files
	3. concatenates the track files
	4. moves the full concatenated file to the appropriate library folder`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.InfoLevel)

		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}

		if len(args) != 1 {
			return fmt.Errorf("[main] missing the playlist id")
		}

		id := args[0]
		playlist, err := crate.GetPlaylistBy("id", string(id), true)
		if err != nil {
			return err
		}

		err = station.Prepare(&playlist)
		if err != nil {
			return err
		}
		cleanup_paths := []string{
			filepath.Join(config.ExtractDir()),
		}
		utils.CleanupDirs(cleanup_paths)

		return nil
	},
}

func init() {
	playlistCmd.AddCommand(prepareCmd)
}
