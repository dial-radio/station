package cmd

import (
	"station/config"
	"station/crate"
	"station/station"
	"station/utils"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/spf13/cobra"
)

// rotateCmd represents the rotate command
var rotateCmd = &cobra.Command{
	Use:   "rotate",
	Short: "Calls a rotation on the schedule",
	Long:  `A rotation signals that a given moment is over, and that the schedule should be updated. It creates a new slot for the given moment, archives playlists that were just played, and moves the upcoming playlists into rotation.`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if err := initConf(); err != nil {
			return err
		}
		initLogs(zerolog.InfoLevel)

		if err := crate.OpenDB(config.Conf().DBPath, true); err != nil {
			return err
		}
		if len(args) != 1 {
			log.Warn().Msg("[main] missing the rotate argument")
			log.Warn().Str("moment", utils.GetLocalMoment()).Msg("[main] defaulting to station current")
			err := station.Rotate(utils.GetLocalMoment())
			if err != nil {
				log.Error().Err(err).Msg("[main] could not rotate")
			}
			return err
		}

		moment := args[0]

		if !utils.IsValidMoment(moment) {
			log.Warn().Str("moment", moment).Msg("[main] rotate argument should be [morning, afternoon, evening, night]")
			log.Warn().Str("moment", utils.GetLocalMoment()).Msg("[main] defaulting to station current")
		} else {
			log.Info().Str("moment", moment).Msg("[main] rotating from")
			err := station.Rotate(moment)
			if err != nil {
				log.Error().Err(err).Msg("[main] could not rotate")
			}
			return err
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(rotateCmd)
}
