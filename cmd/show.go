package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// showCmd displays information on the config or the schedule
var showCmd = &cobra.Command{
	Use:     "show [playlist_id playlist_id playlist_id ...] [-f format]",
	Short:   "Displays station information",
	Long:    `Displays all the information on playlists, either directly by ID or via a query`,
	Example: `station show config|schedule -f json`,
	RunE: func(cmd *cobra.Command, args []string) error {
		if len(cmd.Commands()) < 3 {
			return fmt.Errorf("you must specify what to show")
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(showCmd)
	showCmd.Flags().StringP("format", "f", "pretty", "output format: raw, pretty, json")
}
