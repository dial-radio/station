# Station

This package takes care of the more complex operations, such as curation, scheduling and audio processing.

## Slot

The two main functions are `NewSlot` and `Curate`. The first calculates the next possible moment to create a Slot, given a specific Moment, and the second populates the Slot with available playlists.

## Playlist

The two main functions are `Validate` and `Prepare`.
`Validate` makes sure that the data provided to create a Playlist is valid, checking for URL formation and playlist duration.

`Prepare` starts the whole audio processing pipeline: copy, encode, concatenate
