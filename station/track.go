package station

import (
	"path/filepath"
	"station/crate"
	"station/utils"
	"strings"
	"unicode"

	"github.com/rs/zerolog/log"
)

// extractMetadata parses the file, and assigns the title and artist to the Track object
func extractMetadata(t *crate.Track) error {
	artist, title, err := utils.GetFileMetadata(t.Filename)
	if err != nil {
		log.Error().Err(err).Msg("[extractMetadata]")
	}

	if title != "" {
		t.Title = title
	} else {
		t.Title = filepath.Base(t.Filename)
	}

	if artist != "" {

		// remove any strange unicode characters (thx spotify)
		cleaned := strings.Map(func(r rune) rune {
			if unicode.IsPrint(r) {
				return r
			}
			return -1
		}, artist)

		if len(cleaned) != len(artist) {
			//if we had some invisible characters, then we need to replace each "data" instance by a ","
			cleaned = strings.ReplaceAll(cleaned, "data", ",")
		}

		t.Artist = cleaned
	} else {
		t.Artist = "unknown artist"
	}

	return nil
}
