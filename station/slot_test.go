package station

import (
	"fmt"
	"os"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
)

func setupSlot() func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	if err := crate.OpenDB(utils.Memory, true); err != nil {
		panic(err)
	}
	if err := crate.CreateColorFixtures(); err != nil {
		panic(err)
	}
	if err := Init(); err != nil {
		panic(err)
	}

	return func(t *testing.T) {}
}
func Test_followingSlot(t *testing.T) {
	teardown := setupSlot()
	defer teardown(t)
	type args struct {
		moment string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "gets next slot",
			args:    args{moment: "afternoon"},
			want:    utils.Afternoon,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := crate.GetNextSlot(tt.args.moment)
			if (err != nil) != tt.wantErr {
				t.Errorf("followingSlot() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got.Moment != tt.want {
				t.Errorf("followingSlot() = %v, want %v", got, tt.want)
			}

			if got.QueueTime.Before(time.Now()) {
				t.Errorf("followingSlot() queue time should be in the future = %v, want", got.QueueTime)
			}
		})
	}
}

// things to test in curate:
// - length of curated playlists
// - disposal of previous playlists files from the directories
// - whether the playlist added is indeed the least played
func TestSlot_Curate(t *testing.T) {
	teardown := setupPlaylist(true, true)
	defer teardown(t)

	// TODO: check that the curated playlists are indeed the ones expected
	// this might involve looping through all the playlists in a moment
	// to get the ID of the oldest one in library

	emptySlotNoPlaylists, err := crate.CreateSlot(crate.Slot{
		Moment:    utils.Night,
		QueueTime: time.Now(),
	})
	if err != nil {
		t.Error(err)
	}

	emptySlotWithPlaylists, err := crate.CreateSlot(crate.Slot{
		Moment:    utils.Afternoon,
		QueueTime: time.Now(),
	})
	if err != nil {
		t.Error(err)
	}

	existingSlot, err := crate.GetSlot("1")
	if err != nil {
		t.Error(err)
	}

	tests := []struct {
		name                   string
		slot                   crate.Slot
		expectedPlaylistLength int
		wantErr                bool
	}{
		{
			name:                   "curates an empty slot with no available playlists",
			slot:                   emptySlotNoPlaylists,
			wantErr:                false,
			expectedPlaylistLength: 1, // there is always at least a selection playlist
		},
		{
			name:                   "curates an empty slot with available playlists",
			slot:                   emptySlotWithPlaylists,
			wantErr:                false,
			expectedPlaylistLength: 2,
		},
		{
			name:                   "curates an existing slot",
			slot:                   existingSlot,
			wantErr:                false,
			expectedPlaylistLength: 2,
		},
	}
	for _, tt := range tests {
		oldPlaylists := tt.slot.Playlists
		t.Run(tt.name, func(t *testing.T) {

			if err := Curate(&tt.slot); (err != nil) != tt.wantErr {
				t.Errorf("Slot.Curate() error = %v, wantErr %v", err, tt.wantErr)
			}

			if len(tt.slot.Playlists) != tt.expectedPlaylistLength {
				t.Errorf("Slot.Curate() playlist len = %d, want %d", len(tt.slot.Playlists), tt.expectedPlaylistLength)
			}

			playlistType := tt.slot.Playlists[len(tt.slot.Playlists)-1].Type
			if playlistType != utils.Selection {
				t.Errorf("Slot.Curate() last playlist type = %s, want %s", playlistType, utils.Selection)
			}

			for _, old := range oldPlaylists {
				// check if playlists have been removed from rotation dir
				if old.Status == utils.OnRotation {
					files, err := os.ReadDir(filepath.Join(config.RotationDir(), old.Moment))
					if err != nil {
						t.Error(err)
					}

					for _, file := range files {
						if strings.Contains(file.Name(), old.Filename) {
							t.Errorf("Slot.Curate() file %s not removed from rotation dir", old.Filename)
						}
					}
				}

				for _, new := range tt.slot.Playlists {
					if new.ID == old.ID {
						t.Errorf("Slot.Curate() new playlist ID = %d, old playlist ID %d should not match", new.ID, old.ID)
					}
				}
			}
		})
	}
}

func TestSlot_Sort(t *testing.T) {
	teardown := setupPlaylist(true, true)
	defer teardown(t)

	rotationSlots, err := crate.GetSlotsOnRotation()
	if err != nil {
		t.Error(err)
	}
	rotationSlot, err := crate.GetSlot(fmt.Sprint(rotationSlots[0].ID))
	if err != nil {
		t.Error(err)
	}

	scheduledSlots, err := crate.GetQueuedSlots()
	if err != nil {
		t.Error(err)
	}
	scheduledSlot, err := crate.GetSlot(fmt.Sprint(scheduledSlots[len(scheduledSlots)-1].ID))
	if err != nil {
		t.Error(err)
	}

	tests := []struct {
		name    string
		slot    crate.Slot
		wantErr bool
	}{
		{
			name:    "testing sorting with slot on rotation",
			slot:    rotationSlot,
			wantErr: false,
		},
		{
			name:    "testing sorting with slot scheduled",
			slot:    scheduledSlot,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Sort(&tt.slot); (err != nil) != tt.wantErr {
				t.Errorf("Slot.Sort() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
