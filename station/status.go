package station

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/utils"
	"time"

	"github.com/rs/zerolog/log"
)

type Day struct {
	Date  time.Time    `json:"date"`
	Slots []crate.Slot `json:"slots"`
}

type Broadcaster struct {
	Moment   string  `json:"moment"`
	Loudness float32 `json:"loudness"`
	Track    struct {
		Title  string `json:"title"`
		Artist string `json:"artist"`
	} `json:"track"`
	Next struct {
		Title  string `json:"title"`
		Artist string `json:"artist"`
	} `json:"next"`
}

type Directory struct {
	Name     string   `json:"name"`
	Contents []string `json:"contents"`
}

type Status struct {
	Moment      string        `json:"moment"`
	Config      config.Config `json:"config"`
	Broadcaster Broadcaster   `json:"broadcaster"`
	Schedule    []Day         `json:"schedule"`
	Library     []Directory   `json:"library"`
	Rotation    []Directory   `json:"rotation"`
}

// GetStatus returns together the config, schedule and directory contents
func GetStatus() (Status, error) {

	schedule, err := GetSchedule()
	if err != nil {
		log.Error().Err(err).Msg("[GetStatus] get schedule")
	}
	broadcaster, err := GetBroadcaster()
	if err != nil {
		log.Error().Err(err).Msg("[GetStatus] get broadcaster")
	}

	status := Status{
		Moment:      utils.GetLocalMoment(),
		Schedule:    schedule,
		Broadcaster: broadcaster,
		Rotation:    make([]Directory, 0),
		Library:     make([]Directory, 0),
	}

	//-- rotation dir
	for _, m := range []string{utils.Night, utils.Morning, utils.Afternoon, utils.Evening} {
		d := Directory{Name: m}
		fs, err := os.ReadDir(filepath.Join(config.RotationDir(), m))
		if err != nil {
			return status, err
		}

		for _, f := range fs {
			d.Contents = append(d.Contents, f.Name())
		}

		status.Rotation = append(status.Rotation, d)
	}

	for _, m := range []string{utils.Night, utils.Morning, utils.Afternoon, utils.Evening} {
		d := Directory{Name: m}
		fs, err := os.ReadDir(filepath.Join(config.LibraryDir(), m))
		if err != nil {
			return status, err
		}

		for _, f := range fs {
			d.Contents = append(d.Contents, f.Name())
		}
		status.Library = append(status.Library, d)
	}

	return status, nil
}

func GetBroadcaster() (Broadcaster, error) {

	requestURL := fmt.Sprintf("http://%s:%s/status", config.Conf().BroadcasterHost, config.Conf().BroadcasterPort)
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		log.Error().Err(err).Msg("[getBroadcaster]")
		return Broadcaster{}, err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Error().Err(err).Msg("[getBroadcaster]")
		return Broadcaster{}, err
	}

	resBody, err := io.ReadAll(res.Body)
	if err != nil {
		log.Error().Err(err).Msg("[getBroadcaster]")
		return Broadcaster{}, err
	}

	var broadcaster Broadcaster
	err = json.Unmarshal(resBody, &broadcaster)
	if err != nil {
		log.Error().Err(err).Msg("[getBroadcaster]")
		return Broadcaster{}, err
	}

	return broadcaster, nil
}

// GetSchedule returns returns all queued slots, sorted by days, with the date and the exact playtime calculated. the slot currently on air is the first element of the queue
func GetSchedule() ([]Day, error) {
	var days []Day
	queued, err := crate.GetSchedule()
	if err != nil {
		return days, err
	}

	// sort by days
	for _, slot := range queued {
		hasFound := false
		for index, day := range days {
			if slot.QueueTime.Day() == day.Date.Day() {
				days[index].Slots = append(day.Slots, slot)
				hasFound = true
				break
			}
		}

		if !hasFound {
			newDay := Day{
				Date:  slot.QueueTime,
				Slots: []crate.Slot{slot},
			}
			days = append(days, newDay)
		}
	}

	return days, nil
}
