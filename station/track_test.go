package station

import (
	"station/crate"
	"testing"
)

func Test_extractMetadata(t *testing.T) {
	type args struct {
		t *crate.Track
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := extractMetadata(tt.args.t); (err != nil) != tt.wantErr {
				t.Errorf("extractMetadata() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
