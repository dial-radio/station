package station

import (
	"fmt"
	"os"
	"path/filepath"
	"sort"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

// NewSlot always checks if there are enough slots, or creates one, then curates playlists to accompany it
func NewSlot(moment string) (crate.Slot, error) {
	var slot crate.Slot
	queued, err := crate.GetQueuedSlotsByMoment(moment)
	if err != nil {
		return slot, err
	}

	log.Debug().Int("count", len(queued)).Msg("[NewSlot] queued slots")
	if len(queued) > config.Slots().MaxNumber {
		log.Error().Msg("[NewSlot] enough slots, skipping...")
		return slot, fmt.Errorf("reached max slot capacity for moment")
	}

	slots, err := crate.GetSlotsByMoment(moment)
	if err != nil {
		return slot, err
	}

	// given the slots we have, we either create a brand new queue_time, or we keep adding 24h to the last slot's queue_time until we get one in the future
	var queue_time time.Time
	if len(slots) > 0 {
		queue_time, err = getNextQueueTime(moment, slots)
		if err != nil {
			return slot, err
		}
	} else {
		queue_time = utils.GetTodayDate()

		switch moment {
		case utils.Morning:
			queue_time = queue_time.Add(time.Hour * 6)
		case utils.Afternoon:
			queue_time = queue_time.Add(time.Hour * 12)
		case utils.Evening:
			queue_time = queue_time.Add(time.Hour * 18)
		}

	}

	var new = crate.Slot{
		Moment:    moment,
		QueueTime: queue_time,
	}

	created, err := crate.CreateSlot(new)
	if err != nil {
		log.Error().Err(err).Msg("[NewSlot] create slot")
		return created, err
	}

	err = Curate(&created)
	if err != nil {
		log.Error().Err(err).Msg("[NewSlot] curate")
	}

	return created, err
}

// Sort rearranges the order of a Slot's Playlists to ensure it is consistent. If the Slot is OnRotation (i.e. QueueTime is within), it bases the sort on the order to the Playlist slice; otherwise, it bases it on the order of the ScheduleTime.
func Sort(s *crate.Slot) error {
	if len(s.Playlists) == 0 {
		return fmt.Errorf("there are no playlists for slot #%d", s.ID)
	}
	rotatingSlots, err := crate.GetSlotsOnRotation()
	if err != nil {
		return err
	}

	isOnRotation := false
	for _, rotating := range rotatingSlots {
		if rotating.ID == s.ID {
			isOnRotation = true
			break
		}
	}

	if isOnRotation { // keep array index -> recalculate scheduleTime
		var queueTime time.Duration
		for i, playlist := range s.Playlists {
			s.Playlists[i].ScheduleTime = s.QueueTime.Add(queueTime)
			_, err := crate.EditPlaylist(s.Playlists[i])
			if err != nil {
				return err
			}
			queueTime += playlist.Duration
		}
	} else { // keep schedule time -> sort slice by ScheduleTime
		sort.Slice(s.Playlists, func(i, j int) bool {
			return s.Playlists[i].ScheduleTime.Before(s.Playlists[j].ScheduleTime)
		})
	}

	_, err = crate.EditSlot(*s)
	return err
}

// Curate picks the least played playlists, then creates a slice of those playlists until a given MaxDuration.
// if the chosen playlists do not have an associated filename, it calls Prepare (think about locks?)
// it then updates the slot duration, updates the slot associations, and updates the playlists status
func Curate(s *crate.Slot) error {
	playlists, err := crate.GetLeastPlayedPlaylists(s.Moment)
	if err != nil {
		return err
	}

	old_playlists := s.Playlists
	new_playlists := make([]crate.Playlist, 0)

	// find playlists within max duration; make available and update ScheduleTime as you go
	s.Duration, _ = time.ParseDuration("0h")
	for i, playlist := range playlists {
		if s.Duration+playlist.Duration < config.Slots().Duration {
			_, err := os.Stat(filepath.Join(config.LibraryDir(), playlist.Moment, playlist.Filename))
			if err != nil {
				log.Warn().Uint("ID", playlist.ID).Str("filename", playlist.Filename).Msg("[Curate] making available...")
				go Prepare(&playlist)
			}

			playlists[i].ScheduleTime = s.QueueTime.Add(s.Duration)
			new_playlists = append(new_playlists, playlists[i])

			s.Duration += playlist.Duration
		}
	}

	if len(new_playlists) == 0 {
		log.Warn().Msg("[Curate] no playlists")
	}

	// if there is time left, create the selection playlist to conclude the slot
	if s.Duration < time.Hour*6 {
		selection := crate.Playlist{
			Title:        fmt.Sprintf("%s selections", s.Moment),
			Artist:       "Dial Radio Team",
			Moment:       s.Moment,
			URL:          filepath.Join(config.SelectionsDir(), s.Moment, fmt.Sprintf("%d", s.ID)),
			Email:        config.Bots().TeamEmail,
			Type:         utils.Selection,
			ScheduleTime: s.QueueTime.Add(s.Duration),
			Duration:     (time.Hour * 6) - s.Duration,
			Color:        "#111",
			Description:  "A collection of tracks from Dial Radio's crates, until the next playlist starts!",
		}
		selection.Palette, err = crate.GetPalette(selection.Color, selection.Moment)
		if err != nil {
			log.Error().Err(err).Msg("[Curate]")
			return err
		}
		selection, err = crate.CreatePlaylist(selection)
		if err != nil {
			log.Error().Err(err).Msg("[Curate] creating selection playlist")
		} else {
			new_playlists = append(new_playlists, selection)
		}
	}

	s.Playlists = new_playlists
	edited, err := crate.EditSlotPlaylists(*s)
	if err != nil {
		log.Error().Err(err)
		return err
	}

	// once we're done, update the different playlists
	for _, playlist := range edited.Playlists {
		if playlist.Type == utils.Contribution && playlist.PlayCount == 0 {
			if config.Bots().CanSendEmail {
				if err := SendEmail(&playlist); err != nil {
					log.Error().Err(err).Msg("[Curate] error sending email, skipping...")
				}
			}

			if err := SendSocialMedia(&playlist); err != nil {
				log.Error().Err(err).Msg("[Curate] error posting bot, skipping...")
			}
		}

		playlist.Status = utils.Scheduled
		if playlist.PaletteID == 0 {
			p, err := crate.GetPalette(playlist.Color, playlist.Moment)
			if err != nil {
				log.Error().Err(err).Msg("[Curate] get palette for playlist")
				return err
			}
			playlist.Palette = p
		}
		_, err = crate.EditPlaylist(playlist)
		if err != nil {
			log.Error().Err(err).Msg("[Curate] edit playlist")
			return err
		}
	}

	// now that we have curated, we check if the playlists need to be queued
	queued, err := crate.GetSlotsOnRotation()
	if err != nil {
		return err
	}
	for _, q := range queued {
		if q.ID == s.ID {
			err := queue(s)
			if err != nil {
				log.Error().Err(err).Msg("[Curate] failed to queue playlist")
			}
		}
	}

	for _, playlist := range old_playlists {
		playlist.Status, err = crate.GetStatusFromSlots(playlist)
		if err != nil {
			log.Error().Err(err).Msg("[Curate] get playlist status")
			return err
		}
		_, err := crate.EditPlaylist(playlist)
		if err != nil {
			log.Error().Err(err).Msg("[Curate] edit playlist")
			return err
		}
	}

	log.Info().Uint("ID", edited.ID).Str("moment", edited.Moment).Str("queue_date", edited.QueueTime.Format(time.DateOnly)).Str("queue_time", edited.QueueTime.Format(time.TimeOnly)).Int("playlists", len(edited.Playlists)).Str("duration", edited.Duration.String()).Msg("[Curate]")
	return nil
}

// moves files over to rotation dir, then updates status to OnRotation
func queue(s *crate.Slot) error {
	log.Info().Uint("ID", s.ID).Str("moment", s.Moment).Str("queue_date", s.QueueTime.Format(time.DateOnly)).Str("queue_time", s.QueueTime.Format(time.TimeOnly)).Int("playlists", len(s.Playlists)).Msg("[queue] slot")

	// make sure the slice matches the schedule time order
	sort.Slice(s.Playlists, func(i, j int) bool {
		return s.Playlists[i].ScheduleTime.Before(s.Playlists[j].ScheduleTime)
	})

	// check if playlists are already in the directories to prevent LS from reloading
	playlistFilesReady, err := checkPlaylistFiles(s)
	if err != nil {
		log.Error().Err(err).Msg("[queue] could not check if playlists files are ready")
		return err
	}

	if !playlistFilesReady {
		err := utils.CleanupDirFiles(filepath.Join(config.RotationDir(), s.Moment))
		if err != nil {
			return err
		}

		for index, p := range s.Playlists {
			if p.Type == utils.Selection {
				continue
			}

			src := filepath.Join(config.LibraryDir(), s.Moment, p.Filename)
			dst := filepath.Join(config.RotationDir(), s.Moment, fmt.Sprintf("%d_%s", (index+1), p.Filename))

			log.Debug().Uint("ID", p.ID).Str("filename", p.Filename).Msg("[queue] playlist")

			if err := utils.CopyFile(src, dst); err != nil {
				log.Warn().Err(err).Msg("[queue] file not queued, skipping...")
				return nil
			}

		}
	} else {
		log.Info().Msg("[queue] playlists files already present")
	}

	for _, playlist := range s.Playlists {
		playlist.Status = utils.OnRotation
		_, err := crate.EditPlaylist(playlist)
		if err != nil {
			log.Error().Err(err).Msg("[queue] edit playlist")
			return err
		}
	}

	_, err = crate.EditSlot(*s)
	return err
}

// getNextQueueTime returns the next open queue time for a slot of a give moment
func getNextQueueTime(moment string, slots []crate.Slot) (time.Time, error) {
	var queue_time time.Time

	last_slot := slots[len(slots)-1]
	future_slot := last_slot.QueueTime.Add(time.Hour * 24)

	// get the date
	d := fmt.Sprintf("%d", future_slot.Day())
	if future_slot.Day() < 10 {
		d = fmt.Sprintf("0%d", future_slot.Day())
	}

	m := fmt.Sprintf("%d", future_slot.Month())
	if future_slot.Month() < 10 {
		m = fmt.Sprintf("0%d", future_slot.Month())
	}

	y := fmt.Sprintf("%d", future_slot.Year())

	// get the time
	var base_time time.Time
	switch moment {
	case utils.Morning:
		base_time, _ = time.Parse(time.Kitchen, config.Times().Morning)
	case utils.Afternoon:
		base_time, _ = time.Parse(time.Kitchen, config.Times().Afternoon)
	case utils.Evening:
		base_time, _ = time.Parse(time.Kitchen, config.Times().Evening)
	case utils.Night:
		base_time, _ = time.Parse(time.Kitchen, config.Times().Night)
	default:
		return queue_time, fmt.Errorf("base time could not be parsed")
	}

	h := fmt.Sprintf("%d", base_time.Hour())
	if base_time.Hour() < 10 {
		h = fmt.Sprintf("0%d", base_time.Hour())
	}

	min := fmt.Sprintf("%d", base_time.Minute())
	if base_time.Minute() < 10 {
		min = fmt.Sprintf("0%d", base_time.Minute())
	}

	// finally, parse the time
	queue_time, err := time.ParseInLocation(time.DateTime, fmt.Sprintf("%s-%s-%s %s:%s:00", y, m, d, h, min), time.Local)
	if err != nil {
		return queue_time, err
	}

	// check that this is indeed the last slot
	current_timeslot := time.Now().Add(-time.Hour * 6)
	for {
		if queue_time.After(current_timeslot) {
			break
		} else {
			log.Warn().Msg("[NewSlot] generating queue_time has to go through loop!")
			queue_time = queue_time.Add(time.Hour * 24)
		}
	}

	return queue_time, nil
}

// checkPlaylistFiles returns true if the  playlist files for a given slot are all in the rotation folder
func checkPlaylistFiles(s *crate.Slot) (bool, error) {
	playlistsReady := true
	for _, playlist := range s.Playlists {
		if playlist.Type == utils.Selection {
			continue
		}

		entries, err := os.ReadDir(filepath.Join(config.RotationDir(), s.Moment))
		if err != nil {
			log.Error().Err(err).Msg("[Init] read rotation dir")
			return playlistsReady, err
		}

		playlistInRotation := false
		for _, e := range entries {
			if strings.Contains(e.Name(), playlist.Filename) {
				playlistInRotation = true
				break
			}
		}

		if !playlistInRotation {
			playlistsReady = false
			break
		}
	}

	log.Debug().Uint("ID", s.ID).Str("moment", s.Moment).Str("queue_date", s.QueueTime.Format(time.DateOnly)).Str("queue_time", s.QueueTime.Format(time.TimeOnly)).Int("playlists", len(s.Playlists)).Bool("playlists_ready", playlistsReady).Msg("[checkPlaylistFiles]")

	return playlistsReady, nil
}
