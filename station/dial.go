package station

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"runtime"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

// Init checks for whether each Moment has a Slot scheduled, and if each of these Slots are properly queued (e.g. have files on rotation). It then airs a Slot based on the station Moment
func Init() error {
	required_dependencies := []string{"ffmpeg", "spotdl", "yt-dlp", "freyr", "magick"}
	for _, b := range required_dependencies {
		if _, err := exec.LookPath(b); err != nil {
			return err
		}
	}

	if config.Audio().ShouldNormalize {
		if _, err := exec.LookPath("ffmpeg-normalize"); err != nil {
			return err
		}
	}

	if err := checkRotationPlaylists(); err != nil {
		log.Error().Err(err).Msg("[Init] clear rotation slots")
		return err
	}

	err := addMissingSlots()
	if err != nil {
		log.Error().Err(err).Msg("[Init] populate queue slots")
		return err
	}

	log.Info().Msg("[Init] queuing rotation slots...")
	queuedSlots, err := crate.GetQueuedSlots()
	if err != nil {
		log.Error().Err(err).Msg("[Init] get queued slots")
		return err
	}

	// queue the four first slots to rotation
	for _, slot := range queuedSlots[0:4] {
		log.Debug().Uint("ID", slot.ID).Str("moment", slot.Moment).Str("queue_time", slot.QueueTime.String()).Msg("[Init] queueing slot")

		err = queue(&slot)
		if err != nil {
			log.Error().Err(err).Str("moment", slot.Moment).Msg("[Init] queue slot")
			return err
		}
	}

	return nil
}

// Rotate takes care of creation, archival, queuing and updating, given a moment that has just finished.
// It creates a new Slot if necessary (e.g. the number of queued slots is below a certain threshold)
// then archives the Slot that just played (sets status and deleting files)
// queues the Slot that will play next cycle (sets status and copies files)
// updates the status and start_time for the Slot that is currently playing.
func Rotate(moment string) error {
	if !utils.IsValidMoment(moment) {
		return fmt.Errorf("bad moment: %v", moment)
	}

	log.Debug().Str("moment", moment).Msg("[Rotate]")

	// archive the slot that just finished
	s, err := crate.GetPreviousSlot()
	if err == nil {
		for _, p := range s.Playlists {
			if err := archivePlaylist(&p); err != nil {
				log.Error().Err(err).Msg("[Rotate] archive")
				return err
			}
		}

		if err = utils.CleanupDirFiles(filepath.Join(config.RotationDir(), s.Moment)); err != nil {
			log.Error().Err(err).Msg("[Rotate] clear")
			return err
		}
	} else if !errors.Is(err, gorm.ErrRecordNotFound) {
		return err
	} else {
		log.Warn().Msg("[Rotate] no previous slot found, creating new...")
	}

	// create
	_, err = NewSlot(moment)
	if err != nil {
		return err
	}

	// queue
	s, err = upcomingSlot(moment)
	if err != nil {
		return err
	}
	err = queue(&s)
	if err != nil {
		log.Error().Err(err).Msg("[Rotate] queue")
		return err
	}

	return nil
}

// gets the slot that should play in the next cycle (now + 24h)
func upcomingSlot(moment string) (crate.Slot, error) {
	next, err := crate.GetNextSlot(moment)
	if err != nil {
		return next, err
	}

	if len(next.Playlists) == 0 {
		err = Curate(&next)
	}
	return next, err
}

// addMissingSlots makes sure that there are enough slots of each moment in the queue
func addMissingSlots() error {
	var moments = []string{utils.Morning, utils.Afternoon, utils.Evening, utils.Night}
	for _, m := range moments {
		slots, err := crate.GetQueuedSlotsByMoment(m)
		if err != nil {
			log.Error().Err(err).Str("moment", m).Msg("[addMissingSlots] getting queued slots")
			return err
		}

		if len(slots) < config.Slots().MinNumber/4 {
			needed := config.Slots().MinNumber/4 - len(slots)
			log.Debug().Int("needed", needed).Msg("[populateSlot] creating missing slot")
			for i := 0; i < needed; i++ {
				_, err := NewSlot(m)
				if err != nil {
					return err
				}
			}
		}
	}
	return nil
}

// checkRotationPlaylists looks for any discrepancy in the rotation playlists and the files in the rotation folders.
// Any playlist that is marked as OnRotation but does not have a file is archived.
// Any file found in the rotation folders who is not the file of one of the rotation playlists is deleted.
func checkRotationPlaylists() error {
	rotationPlaylists, err := crate.GetPlaylistsByStatus(utils.OnRotation)
	if err != nil {
		log.Error().Err(err).Msg("[checkRotationPlaylists] get rotation slots")
		return err
	}

	for _, p := range rotationPlaylists {
		// check if the rotation playlist has a file in the rotation dir
		rotationDir, err := os.ReadDir(filepath.Join(config.RotationDir(), p.Moment))
		if err != nil {
			return err
		}

		isOnRotation := false
		for _, f := range rotationDir {
			if strings.Contains(f.Name(), p.Filename) {
				isOnRotation = true
				break
			}
		}

		if !isOnRotation {
			log.Warn().Uint("ID", p.ID).Msg("[checkRotationPlaylist] found discrepant playlist, archiving...")
			err := archivePlaylist(&p)
			if err != nil {
				log.Error().Err(err).Uint("ID", p.ID).Msg("[checkRotationPlaylists] archive playlist")
				return err
			}
		}
	}

	// check if the file in the rotation dir is associated with a rotation playlist
	for _, moment := range []string{utils.Morning, utils.Afternoon, utils.Evening, utils.Night} {
		rotationDir, err := os.ReadDir(filepath.Join(config.RotationDir(), moment))
		if err != nil {
			return err
		}
		for _, f := range rotationDir {
			nameWithoutPrefix := f.Name()[2:]
			p, err := crate.GetPlaylistBy("filename", nameWithoutPrefix, false)

			if p.Status != utils.OnRotation || errors.Is(err, gorm.ErrRecordNotFound) {
				log.Warn().Str("filename", f.Name()).Msg("[checkRotationplaylists] file on rotation without playlist on rotation, removing...")
				return os.Remove(filepath.Join(config.RotationDir(), moment, f.Name()))
			}

			if err != nil {
				return err
			}
		}
	}

	return nil
}
