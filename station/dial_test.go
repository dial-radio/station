package station

import (
	"os"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/utils"
	"testing"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func setupDial() func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	if err := crate.OpenDB(utils.Memory, true); err != nil {
		panic(err)
	}
	if err := crate.RunFixtures(); err != nil {
		panic(err)
	}

	if err := Init(); err != nil {
		panic(err)
	}

	return func(t *testing.T) {}
}

func TestSchedule(t *testing.T) {
	teardown := setupDial()
	defer teardown(t)

	t.Run("get schedule", func(t *testing.T) {
		sched, err := GetSchedule()
		require.Nil(t, err)
		assert.Equal(t, len(sched), 2)
	})
}

func TestRotate(t *testing.T) {
	teardown := setupDial()
	defer teardown(t)

	moment := utils.GetLocalMoment()
	t.Run("rotate the current moment", func(t *testing.T) {
		// check for files before and after
		old, err := os.ReadDir(filepath.Join(config.RotationDir(), utils.Morning))
		if err != nil {
			t.Error(err)
		}

		err = Rotate(moment)
		require.Nil(t, err)

		new, err := os.ReadDir(filepath.Join(config.RotationDir(), moment))
		if err != nil {
			t.Error(err)
		}

		for _, o := range old {
			for _, n := range new {
				if o.Name() == n.Name() {
					t.Errorf("Dial.Rotate() found old playlist file %s, which should have been moved", o.Name())
				}
			}
		}
	})
}
