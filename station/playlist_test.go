package station

import (
	"fmt"
	_ "image/gif"
	_ "image/png"
	"os"
	"path/filepath"
	"station/config"
	"station/crate"
	"station/utils"
	"strings"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var testFilesPath string
var testMoment = utils.Afternoon

func setupPlaylist(runFixtures bool, runInit bool) func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	if err := crate.OpenDB(utils.Memory, true); err != nil {
		panic(err)
	}

	if runFixtures {
		if err := crate.RunFixtures(); err != nil {
			panic(err)
		}
	}

	if runInit {
		if err := Init(); err != nil {
			panic(err)
		}
	}

	//-- init playlist and files
	testFilesPath = filepath.Join(Basepath, "../files/test")
	dirRead, _ := os.Open(testFilesPath)
	dirFiles, _ := dirRead.Readdir(0)
	for _, f := range dirFiles {
		fullPath := filepath.Join(testFilesPath, f.Name())
		if err := os.MkdirAll(filepath.Join(config.ExtractDir(), testMoment), os.ModePerm); err != nil {
			panic(err)
		}
		dest := filepath.Join(config.ExtractDir(), testMoment, f.Name())
		err := utils.CopyFile(fullPath, dest)
		if err != nil {
			panic(err)
		}
	}

	return func(t *testing.T) {
		utils.CleanupDirs([]string{filepath.Join(config.ExtractDir())})
	}
}

func TestPlaylist_Duration(t *testing.T) {
	var s crate.Playlist
	var t0, t1 crate.Track
	t0.Duration, _ = time.ParseDuration("2m45")
	t1.Duration, _ = time.ParseDuration("4m35")
	s.Tracks = append(s.Tracks, t0, t1)
	want, _ := time.ParseDuration("7m20")

	t.Run("check correct duration", func(t *testing.T) {
		duration := calculateDuration(&s)
		if duration != want {
			t.Errorf("Playlist.Duration() = %v, want %v", duration, want)
		}
	})

	t.Run("check error handling", func(t *testing.T) {
		t.Skip("todo")
	})
}

func TestPlaylist_SaveImage(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	var p crate.Playlist
	p.ID = 8
	p.UUID = uuid.New()
	fileName := "fallback_morning"
	fileExt := "png"
	fullFilename := fmt.Sprintf("%s.%s", fileName, fileExt)
	src := filepath.Join(Basepath, "../files/", fullFilename)

	bytes, err := os.ReadFile(src)
	if err != nil {
		t.Error(err)
	}

	t.Run("check image saving", func(t *testing.T) {
		err := SaveImage(&p, bytes, fullFilename)
		require.Nil(t, err, "should save correctly")

		frag := strings.Split(p.UUID.String(), "-")[0]
		require.Equal(t, fmt.Sprintf("%d_%s_%s.jpg", p.ID, fileName, frag), p.Image)
		if _, err := os.Stat(filepath.Join(config.ImageDir(), p.Image)); err != nil {
			t.Error(err)
		}
	})

	t.Run("check error handling", func(t *testing.T) {
		t.Skip("todo")
	})
}

func TestPlaylist_Validate(t *testing.T) {
	teardown := setupPlaylist(true, false)
	defer teardown(t)

	longFile := filepath.Join(config.Station().BaseDir, "test_long.ogg")
	err := utils.CopyFile(filepath.Join(Basepath, "../files/test/test_long.ogg"), longFile)
	if err != nil {
		panic(err)
	}
	shortFile := filepath.Join(config.Station().BaseDir, "test_short.ogg")
	err = utils.CopyFile(filepath.Join(Basepath, "../files/test/test_short.ogg"), shortFile)
	if err != nil {
		panic(err)
	}

	type fields struct {
		Description string
		Email       string
		Moment      string
		URL         string
		Status      string
		Artist      string
		Title       string
		Filename    string
		Tracks      []crate.Track
		PlayCount   int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should validate",
			fields: fields{
				Description: "     Sample description   ",
				Email:       "test@test.com",
				Moment:      utils.Afternoon,
				URL:         longFile,
				Status:      utils.Pending,
				Title:       "A Minima                 ",
				Artist:      "           Samuel Beckett",
				Filename:    "",
				Tracks:      []crate.Track{},
				PlayCount:   0,
			},
			wantErr: false,
		},
		{
			name: "should not validate - email",
			fields: fields{
				Description: "Sample description",
				Email:       "test",
				Moment:      utils.Afternoon,
				URL:         longFile,
				Status:      utils.Pending,
				Title:       "A Minima",
				Artist:      "Samuel Beckett",
				Filename:    "",
				Tracks:      []crate.Track{},
				PlayCount:   0,
			},
			wantErr: true,
		},
		{
			name: "should not validate - moment",
			fields: fields{
				Description: "Sample description",
				Email:       "test@test.com",
				Moment:      "wrong",
				URL:         shortFile,
				Status:      utils.Pending,
				Title:       "A Minima",
				Artist:      "Samuel Beckett",
				Filename:    "",
				Tracks:      []crate.Track{},
				PlayCount:   0,
			},
			wantErr: true,
		},
		{
			name: "should not validate - duration",
			fields: fields{
				Description: "Sample description",
				Email:       "test@test.com",
				Moment:      utils.Afternoon,
				URL:         filepath.Join(config.Station().BaseDir, "fallback.ogg"),
				Status:      utils.Pending,
				Title:       "A Minima",
				Artist:      "Samuel Beckett",
				Filename:    "",
				Tracks:      []crate.Track{},
				PlayCount:   0,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &crate.Playlist{
				Description: tt.fields.Description,
				Artist:      tt.fields.Artist,
				Email:       tt.fields.Email,
				Moment:      tt.fields.Moment,
				URL:         tt.fields.URL,
				Status:      tt.fields.Status,
				Title:       tt.fields.Title,
				Filename:    tt.fields.Filename,
				Tracks:      tt.fields.Tracks,
				PlayCount:   tt.fields.PlayCount,
			}
			if err := Validate(p); (err != nil) != tt.wantErr {
				t.Errorf("Playlist.Validate() error = %v, wantErr %v", err, tt.wantErr)
			}

			if p.Artist != "Samuel Beckett" || p.Title != "A Minima" || p.Description != "Sample description" {
				t.Errorf("playlist strings space not trimmed")
			}
		})
	}
}

func TestPlaylist_Prepare(t *testing.T) {
	teardown := setupPlaylist(true, false)
	defer teardown(t)

	pendingPlaylist := crate.Playlist{
		Description: "This playlists tests MakeAvailability",
		Email:       "pierre@test.com",
		Moment:      testMoment,
		URL:         testFilesPath,
		Status:      utils.Pending,
		Title:       "making available",
		Artist:      "periode",
		Filename:    "",
		Tracks:      []crate.Track{},
		PlayCount:   0,
	}

	playlist, err := crate.CreatePlaylist(pendingPlaylist)
	if err != nil {
		panic(err)
	}

	type fields struct {
		ID          uint
		Description string
		Email       string
		Moment      string
		URL         string
		Status      string
		Artist      string
		Title       string
		Filename    string
		Tracks      []crate.Track
		PlayCount   int
	}
	tests := []struct {
		name       string
		fields     fields
		wantErr    bool
		wantStatus string
	}{
		{
			name: "should not make playlist available (empty URL)",
			fields: fields{
				ID:          playlist.ID,
				Description: playlist.Description,
				Email:       playlist.Email,
				Moment:      playlist.Moment,
				URL:         "",
				Status:      utils.OnRotation,
				Title:       playlist.Title,
				Filename:    playlist.Filename,
				Tracks:      playlist.Tracks,
				PlayCount:   playlist.PlayCount,
			},
			wantErr:    true,
			wantStatus: utils.Pending,
		},
		{
			name: "should make playlist available",
			fields: fields{
				ID:          playlist.ID,
				Description: playlist.Description,
				Email:       playlist.Email,
				Moment:      playlist.Moment,
				URL:         playlist.URL,
				Status:      playlist.Status,
				Artist:      playlist.Artist,
				Title:       playlist.Title,
				Filename:    playlist.Filename,
				Tracks:      playlist.Tracks,
				PlayCount:   playlist.PlayCount,
			},
			wantErr:    false,
			wantStatus: utils.InLibrary,
		},
		{
			name: "should not make playlist available (status processing)",
			fields: fields{
				Description: playlist.Description,
				Email:       playlist.Email,
				Moment:      testMoment,
				URL:         testFilesPath,
				Status:      utils.Processing,
				Title:       playlist.Title,
				Artist:      playlist.Artist,
				Filename:    playlist.Filename,
				Tracks:      playlist.Tracks,
				PlayCount:   playlist.PlayCount,
			},
			wantErr:    true,
			wantStatus: utils.Processing,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &crate.Playlist{
				Description: tt.fields.Description,
				Email:       tt.fields.Email,
				Moment:      tt.fields.Moment,
				URL:         tt.fields.URL,
				Status:      tt.fields.Status,
				Title:       tt.fields.Title,
				Artist:      tt.fields.Artist,
				Filename:    tt.fields.Filename,
				Tracks:      tt.fields.Tracks,
				PlayCount:   tt.fields.PlayCount,
			}
			p.ID = playlist.ID

			var err error
			if err = Prepare(p); (err != nil) != tt.wantErr {
				if tt.wantStatus != p.Status {
					t.Errorf("Playlist.Prepare() error = %v, wantErr %v, status = %s, wantStatus = %s", err, tt.wantErr, p.Status, tt.wantStatus)
				}
			}

			if err == nil {
				artist, title, err := utils.GetFileMetadata(filepath.Join(config.LibraryDir(), p.Moment, p.Filename))
				require.Nil(t, err)
				assert.Equal(t, p.Artist, artist, "artist")
				assert.Equal(t, p.Title, title, "title")
			}
		})
	}
}

func TestPlaylist_Normalize(t *testing.T) {
	teardown := setupPlaylist(true, false)
	defer teardown(t)

	pendingPlaylist := crate.Playlist{
		Description: "This playlists tests Normalize",
		Email:       "pierre@test.com",
		Moment:      testMoment,
		URL:         testFilesPath,
		Status:      utils.Pending,
		Title:       "normalizing",
		Artist:      "periode",
		Filename:    "",
		Tracks:      []crate.Track{},
		PlayCount:   0,
	}

	playlist, err := crate.CreatePlaylist(pendingPlaylist)
	if err != nil {
		panic(err)
	}

	base := filepath.Join(config.ExtractDir(), testMoment)
	dirRead, _ := os.Open(base)
	dirFiles, _ := dirRead.Readdir(1)
	for _, f := range dirFiles {
		playlist.Tracks = append(playlist.Tracks, crate.Track{
			Filename: filepath.Join(base, f.Name()),
		})
	}

	type fields struct {
		ID          uint
		Description string
		Email       string
		Moment      string
		URL         string
		Status      string
		Artist      string
		Title       string
		Filename    string
		Tracks      []crate.Track
		PlayCount   int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should normalize tracks",
			fields: fields{
				ID:          playlist.ID,
				Description: playlist.Description,
				Email:       playlist.Email,
				Moment:      playlist.Moment,
				URL:         playlist.URL,
				Status:      playlist.Status,
				Title:       playlist.Title,
				Filename:    playlist.Filename,
				Tracks:      playlist.Tracks,
				PlayCount:   playlist.PlayCount,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &crate.Playlist{
				Description: tt.fields.Description,
				Email:       tt.fields.Email,
				Moment:      tt.fields.Moment,
				URL:         tt.fields.URL,
				Status:      tt.fields.Status,
				Title:       tt.fields.Title,
				Filename:    tt.fields.Filename,
				Tracks:      tt.fields.Tracks,
				PlayCount:   tt.fields.PlayCount,
			}
			s.ID = playlist.ID

			if err := normalizePlaylist(s); (err != nil) != tt.wantErr {
				t.Errorf("Playlist.normalize() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPlaylist_CopyFiles(t *testing.T) {
	teardown := setupPlaylist(true, false)
	defer teardown(t)

	pendingPlaylist := crate.Playlist{
		Description: "This playlists tests CoyFiles",
		Email:       "pierre@test.com",
		Moment:      testMoment,
		URL:         testFilesPath,
		Status:      utils.Pending,
		Title:       "testing_copy",
		Artist:      "",
		Filename:    "",
		Tracks:      []crate.Track{},
		PlayCount:   0,
	}

	pendingPlaylist, err := crate.CreatePlaylist(pendingPlaylist)
	if err != nil {
		panic(err)
	}

	type fields struct {
		ID          uint
		Description string
		Email       string
		Moment      string
		URL         string
		Status      string
		Artist      string
		Title       string
		Filename    string
		Tracks      []crate.Track
		PlayCount   int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should copy local files over",
			fields: fields{
				ID:          pendingPlaylist.ID,
				Description: pendingPlaylist.Description,
				Email:       pendingPlaylist.Email,
				Moment:      pendingPlaylist.Moment,
				URL:         pendingPlaylist.URL,
				Status:      pendingPlaylist.Status,
				Title:       pendingPlaylist.Title,
				Filename:    pendingPlaylist.Filename,
				Tracks:      pendingPlaylist.Tracks,
				PlayCount:   pendingPlaylist.PlayCount,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &crate.Playlist{
				Description: tt.fields.Description,
				Email:       tt.fields.Email,
				Moment:      tt.fields.Moment,
				URL:         tt.fields.URL,
				Status:      tt.fields.Status,
				Title:       tt.fields.Title,
				Filename:    tt.fields.Filename,
				Tracks:      tt.fields.Tracks,
				PlayCount:   tt.fields.PlayCount,
			}
			s.ID = uint(666)

			if err := copy(s); (err != nil) != tt.wantErr {
				t.Errorf("Playlist.copy() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestPlaylist_RemoveSilence(t *testing.T) {
	teardown := setupPlaylist(true, false)
	defer teardown(t)

	pendingPlaylist := crate.Playlist{
		Description: "This playlist tests removeSilence",
		Email:       "pierre@test.com",
		Moment:      testMoment,
		URL:         testFilesPath,
		Status:      utils.Pending,
		Title:       "testing_silence",
		Artist:      "",
		Filename:    "mixdown_with_silence.opus",
		Tracks:      []crate.Track{},
		PlayCount:   0,
	}

	pendingPlaylist, err := crate.CreatePlaylist(pendingPlaylist)
	if err != nil {
		panic(err)
	}

	if err := os.MkdirAll(filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", pendingPlaylist.ID, pendingPlaylist.Moment)), os.ModePerm); err != nil {
		t.Error(err)
	}

	src := filepath.Join(config.ExtractDir(), testMoment, pendingPlaylist.Filename)
	dst := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", pendingPlaylist.ID, pendingPlaylist.Moment), pendingPlaylist.Filename)
	if err := utils.CopyFile(src, dst); err != nil {
		t.Error(err)
	}

	duration_before, err := utils.GetAudioFileDuration(dst)
	if err != nil {
		t.Error(err)
	}

	type fields struct {
		ID          uint
		Description string
		Email       string
		Moment      string
		URL         string
		Status      string
		Artist      string
		Title       string
		Filename    string
		Tracks      []crate.Track
		PlayCount   int
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "should remove silence",
			fields: fields{
				ID:          pendingPlaylist.ID,
				Description: pendingPlaylist.Description,
				Email:       pendingPlaylist.Email,
				Moment:      pendingPlaylist.Moment,
				URL:         pendingPlaylist.URL,
				Status:      pendingPlaylist.Status,
				Title:       pendingPlaylist.Title,
				Filename:    pendingPlaylist.Filename,
				Tracks:      pendingPlaylist.Tracks,
				PlayCount:   pendingPlaylist.PlayCount,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := &crate.Playlist{
				Description: tt.fields.Description,
				Email:       tt.fields.Email,
				Moment:      tt.fields.Moment,
				URL:         tt.fields.URL,
				Status:      tt.fields.Status,
				Title:       tt.fields.Title,
				Filename:    tt.fields.Filename,
				Tracks:      tt.fields.Tracks,
				PlayCount:   tt.fields.PlayCount,
			}
			s.ID = pendingPlaylist.ID

			if err := removeSilence(s); (err != nil) != tt.wantErr {
				t.Errorf("Playlist.removeSilence() error = %v, wantErr %v", err, tt.wantErr)
			}

			duration_after, err := utils.GetAudioFileDuration(dst)
			if err != nil {
				t.Error(err)
			}

			assert.Greater(t, duration_before, duration_after, "duration should change after processing")
		})
	}
}

func Test_getStatusFromSlots(t *testing.T) {
	teardown := setupPlaylist(true, true)
	defer teardown(t)

	moment := utils.GetLocalMoment()
	queued, err := crate.GetQueuedSlotsByMoment(moment)
	if err != nil {
		t.Error(err)
	}

	rotationPlaylist := queued[0].Playlists[0]
	scheduledPlaylist := queued[1].Playlists[0]

	libraryPlaylist := crate.Playlist{
		Moment: utils.Night,
		Status: utils.Pending,
	}
	libraryPlaylist.ID = 666

	type args struct {
		playlist crate.Playlist
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "test a playlist in library",
			args: args{
				playlist: libraryPlaylist,
			},
			want:    utils.InLibrary,
			wantErr: false,
		},
		{
			name: "test a playlist scheduled",
			args: args{
				playlist: scheduledPlaylist,
			},
			want:    utils.Scheduled,
			wantErr: false,
		},
		{
			name: "test a playlist rotation",
			args: args{
				playlist: rotationPlaylist,
			},
			want:    utils.OnRotation,
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := crate.GetStatusFromSlots(tt.args.playlist)
			if (err != nil) != tt.wantErr {
				t.Errorf("getStatusFromSlots() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getStatusFromSlots() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUpdateSelectionPlaylist(t *testing.T) {
	teardown := setupPlaylist(true, true)
	defer teardown(t)

	type args struct {
		s crate.Track
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "add to selection playlist",
			args: args{
				s: crate.Track{
					Title:  "test title selection",
					Artist: "test artist selection",
					Moment: utils.Morning,
				},
			},
			wantErr: false,
		},
		{
			name: "error on empty track",
			args: args{
				s: crate.Track{
					Title:  "",
					Artist: "",
					Moment: utils.Morning,
				},
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := UpdateSelectionPlaylist(&tt.args.s); (err != nil) != tt.wantErr {
				t.Errorf("UpdateSelectionPlaylist() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestEncode(t *testing.T) {
	teardown := setupPlaylist(true, true)
	defer teardown(t)

	playlist, err := crate.GetPlaylistBy("id", "1", false)
	if err != nil {
		t.Error(err)
	}
	type args struct {
		p       *crate.Playlist
		encoder string
		format  string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "should encode to mp3",
			args: args{
				p:       &playlist,
				format:  "mp3",
				encoder: "libmp3lame",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := Encode(tt.args.p, tt.args.encoder, tt.args.format); (err != nil) != tt.wantErr {
				t.Errorf("Encode() error = %v, wantErr %v", err, tt.wantErr)
			} else {

				assert.Equal(t, fmt.Sprintf(".%s", tt.args.format), filepath.Ext(tt.args.p.Filename))
			}
		})
	}
}
