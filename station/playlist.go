package station

import (
	"bytes"
	"fmt"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"io/fs"
	"net/mail"
	"os"
	"os/exec"
	"path/filepath"
	"station/bots"
	"station/config"
	"station/crate"
	"station/mailer"
	"station/provider"
	"station/source"
	"station/utils"
	"strings"

	"time"

	"github.com/gosimple/slug"
	"github.com/rs/zerolog/log"
	"github.com/spf13/viper"
	"gorm.io/gorm"
)

// UpdateSelectionPlaylist fetches the current selection playlist, and appends the track to the tracklist
func UpdateSelectionPlaylist(s *crate.Track) error {
	log.Info().Str("moment", s.Moment).Str("title", s.Title).Str("artist", s.Artist).Msg("[UpdateSelectionPlaylist]")

	currentSlot, err := crate.GetSlotOnAir()
	if err != nil {
		return err
	}

	selection := currentSlot.Playlists[len(currentSlot.Playlists)-1]
	if selection.Type != utils.Selection {
		log.Warn().Msg("[UpdateSelectionPlaylist] no selection playlist on slot")
		return fmt.Errorf("no selection playlist on slot")
	}

	if s.Title == "" && s.Artist == "" {
		log.Warn().Msg("[UpdateSelectionPlaylist] skipping empty track")
		return fmt.Errorf("not adding empty track metadata to selection")
	}

	_, err = crate.AppendPlaylistTracks(selection, *s)
	return err
}

// return the Source object, based on the url input, possibly caching/singleton
func Source(p *crate.Playlist) (source.Source, error) {
	return source.New(p.URL)
}

// return the Provider object, based on the url input, possibly caching/singleton
func Provider(p *crate.Playlist) (provider.Provider, error) {
	return provider.New(p.URL)
}

// calculateDuration returns the sum of all Track durations
func calculateDuration(p *crate.Playlist) time.Duration {
	var total time.Duration
	tracks := p.Tracks
	if len(tracks) == 0 {
		playlist, err := crate.GetPlaylistBy("id", fmt.Sprintf("%d", p.ID), true)
		if err != nil {
			log.Error().Err(err).Msg("[Duration]")
			return 0.0
		}
		tracks = playlist.Tracks
	}
	for _, t := range tracks {
		total += t.Duration
	}
	return total
}

// Validate makes sure the playlist returned is usable: valid URL, estimated duration, all form fields bound to member fields
func Validate(p *crate.Playlist) error {
	if !utils.IsValidMoment(p.Moment) {
		return fmt.Errorf("bad moment: %v", p.Moment)
	}

	var err error
	_, err = mail.ParseAddress(p.Email)
	if err != nil {
		return err
	}

	provider, err := provider.New(p.URL)
	if err != nil {
		return err
	}

	u := provider.URL()
	var existing crate.Playlist
	if !u.IsAbs() { // distinguish between local and remote
		existing, err = crate.GetPlaylistBy("url", u.Path, false)
		if err != nil && err != gorm.ErrRecordNotFound {
			return err
		}

		if existing.ID != 0 {
			return fmt.Errorf("found existing playlist %d with URL %s", p.ID, p.URL)
		}
	} else {
		existing, err = crate.GetPlaylistBy("url", u.String(), false)
		if err != nil && err != gorm.ErrRecordNotFound {
			return err
		}

		if existing.ID != 0 {
			return fmt.Errorf("found existing playlist %d with URL %s", p.ID, p.URL)
		}
	}

	prov, err := Provider(p)
	if err != nil {
		log.Error().Err(err).Msg("[Validate]")
		return err
	}

	d, err := prov.Duration()
	if err != nil {
		log.Error().Err(err).Msg("[Validate]")
		return err
	}

	if d > config.Playlists().MaxDuration {
		return fmt.Errorf("the playlist is too long! It should not be longer than %d hours %d minutes (currently, it's %d hours %d minutes)", int(config.Playlists().MaxDuration.Hours()), int(config.Playlists().MaxDuration.Minutes())%60, int(d.Hours()), int(d.Minutes())%60)
	} else if d < config.Playlists().MinDuration {
		return fmt.Errorf("the playlist is too short! It should be at least %d minutes (currently, it's %d minutes)", int(config.Playlists().MinDuration.Minutes())%60, int(d.Minutes())%60)
	}

	p.Artist = strings.TrimSpace(p.Artist)
	p.Title = strings.TrimSpace(p.Title)
	p.Description = strings.TrimSpace(p.Description)

	return nil
}

func SendSocialMedia(p *crate.Playlist) error {
	palette, err := crate.GetPaletteByID(fmt.Sprintf("%d", p.PaletteID))
	if err != nil {
		log.Error().Err(err).Msg("[Curate] fetching palette")
		return err
	}
	p.Palette = palette

	if err = bots.PostSocialMedia(*p); err != nil {
		log.Error().Err(err).Msg("[Curate] generate image")
		return err
	}

	return nil
}

func SendEmail(p *crate.Playlist) error {
	t := p.ScheduleTime.Format(time.TimeOnly)
	t = t[:len(t)-3] // get rid of seconds

	d := fmt.Sprintf("%s, %d/%d", p.ScheduleTime.Weekday(), p.ScheduleTime.Day(), p.ScheduleTime.Month())
	payload := mailer.ScheduledPayload{
		Name:   p.Artist,
		Moment: p.Moment,
		Title:  p.Title,
		Date:   d,
		Time:   t,
	}

	if err := mailer.SendMail(p.Email, "Playlist scheduled!", "scheduled", payload); err != nil {
		log.Error().Err(err).Msg("[Curate] sending scheduled email")
		return err
	}

	return nil
}

// SaveImage takes a byte slice and takes care of:
// (1) generating a filename, and a destination file
// (2) decoding the bytes into an image, then re-encoding it to JPEG in the destination file
// (3) updating the Image field on the playlist with the filename of the saved image
func SaveImage(p *crate.Playlist, b []byte, name string) error {
	frag := strings.Split(p.UUID.String(), "-")[0]
	noExtension := strings.Replace(name, filepath.Ext(name), "", 1)
	clean := slug.Make(noExtension)

	filename := fmt.Sprintf("%d_%s_%s.jpg", p.ID, clean, frag)
	dst, err := os.Create(filepath.Join(config.ImageDir(), filename))
	if err != nil {
		return err
	}
	defer dst.Close()

	imgFile, _, err := image.Decode(bytes.NewReader(b))
	if err != nil {
		return err
	}

	err = jpeg.Encode(dst, imgFile, &jpeg.Options{Quality: 50})
	if err != nil {
		return err
	}

	p.Image = filename
	return nil
}

// Prepare() makes sure the playlist is playable: it copies all tracks to the extractdir,
// returns all associated Track metadata, processes the audio files, bounces the mixdown to the library
// and updates the playlist
func Prepare(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Str("url", p.URL).Msg("[Prepare] start")
	start := time.Now()
	if p.Status == utils.Processing {
		log.Warn().Str("title", p.Title).Str("status", p.Status).Msg("[Prepare] playlist already processing")
		return fmt.Errorf("playlist is already processing")
	}

	defer func() {
		// if the process has failed, reset status back to Pending
		if p.Status == utils.Processing { // if it's in library, scheduled, or on rotation, we ignore
			p.Status = utils.Pending
			_, err := crate.EditPlaylist(*p)
			if err != nil {
				log.Error().Err(err).Msg("[Prepare] defer setting playlist status to pending")
			}
		}
	}()

	if p.Status != utils.Pending {
		log.Warn().Str("title", p.Title).Str("status", p.Status).Msg("[Prepare] forcing re-availability")
	}

	p.Status = utils.Processing
	_, err := crate.EditPlaylist(*p)
	if err != nil {
		log.Error().Err(err).Msg("[Prepare]")
		return err
	}

	err = copy(p)
	if err != nil {
		log.Error().Err(err).Msg("[Prepare]")
		return err
	}

	err = process(p)
	if err != nil {
		log.Error().Err(err).Msg("[Prepare]")
		return err
	}
	file, err := bounce(p)
	if err != nil {
		log.Error().Err(err).Msg("[Prepare]")
		return err
	}
	defer file.Close()

	p.Filename = filepath.Base(file.Name())
	p.Duration = calculateDuration(p)
	p.Status, err = crate.GetStatusFromSlots(*p)
	if err != nil {
		return err
	}
	*p, err = crate.EditPlaylist(*p)
	if err != nil {
		log.Error().Err(err).Msg("[Prepare]")
		return err
	}
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[Prepare] done")
	return nil
}

// Encode re-encodes an already prepared playlist to the given format with the given encoder.
func Encode(p *crate.Playlist, encoder string, format string) error {
	if p.Status == utils.Pending || p.Status == utils.Processing {
		return fmt.Errorf("cannot encode a playlist with status: %s", p.Status)
	}

	if encoder == "" {
		encoder = config.Audio().Encoder
	}
	if format == "" {
		format = config.Audio().Format
	}

	log.Info().Uint("ID", p.ID).Str("format", format).Str("encoder", encoder).Str("filename", p.Filename).Msg("[Encode] start")
	start := time.Now()

	src := filepath.Join(config.LibraryDir(), p.Moment, p.Filename)
	encoded, err := utils.Encode(src, format, encoder)
	if err != nil {
		log.Error().Err(err).Msg("[Encode]")
		return err
	}

	p.Filename = filepath.Base(encoded)
	_, err = crate.EditPlaylist(*p)

	log.Info().Uint("ID", p.ID).Dur("duration", time.Since(start)).Msg("[Encode] done")
	return err
}

// archive updates the Status, LastPlayedAt and PlayCount of a playlist
func archivePlaylist(p *crate.Playlist) error {
	log.Debug().Uint("ID", p.ID).Str("status", p.Status).Str("moment", p.Moment).Str("LastPlayedAt", p.LastPlayedAt.String()).Int("play_count", p.PlayCount).Msg("[archivePlaylist]")
	status, err := crate.GetStatusFromSlots(*p)
	if err != nil {
		return err
	}

	p.Status = status
	p.LastPlayedAt = p.ScheduleTime
	p.PlayCount += 1

	if _, err = crate.EditPlaylist(*p); err != nil {
		log.Error().Err(err).Uint("ID", p.ID).Msg("[archive] edit previous playlist")
		return err
	}
	return nil
}

// copy creates a new source, copies the files to a newly created ExtractDir, and assigns the []Track
func copy(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[copy] start")
	start := time.Now()
	src, err := Source(p)
	if err != nil {
		return err
	}

	dst := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment))
	err = src.Copy(dst)
	if err != nil {
		return err
	}

	// populate tracks
	p.Tracks = []crate.Track{}
	files, _ := os.ReadDir(dst)
	for _, f := range files {
		if utils.IsAudioExtension(filepath.Ext(f.Name())) {
			fullPath := filepath.Join(dst, f.Name())
			duration, err := utils.GetAudioFileDuration(fullPath)
			if err != nil {
				log.Warn().Err(err).Str("file", f.Name()).Msg("[copy] file not parsable, skipping...")
			} else {
				t := crate.Track{
					Filename: fullPath,
					Duration: time.Duration(duration),
				}
				err = extractMetadata(&t)
				if err != nil {
					log.Error().Err(err).Msg("[copy] failed to parse metadata")
					return err
				}
				p.Tracks = append(p.Tracks, t)
			}
		} else {
			log.Debug().Str("file", f.Name()).Msg("[copy] skipping non-audio file")
		}
	}

	*p, err = crate.ReplacePlaylistTracks(*p)
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[copy] done")
	return err
}

// process cleans a Playlist's Tracks' filenames, re-encodes to the current AudioFormat, and optionally normalizes the tracks.
func process(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[process] start")
	start := time.Now()
	for i := range p.Tracks {
		cleaned, err := utils.CleanFilename(p.Tracks[i].Filename)
		if err != nil {
			log.Error().Err(err).Msg("[process] failed to clean filename")
			return err
		}
		p.Tracks[i].Filename = cleaned
	}

	// always check if it needs to be re-encoded
	err := encodePlaylist(p)
	if err != nil {
		log.Error().Err(err).Msg("[process] failed to encode")
		return err
	}

	if config.Audio().ShouldNormalize {
		err := normalizePlaylist(p)
		if err != nil {
			log.Error().Err(err).Msg("[process] failed to normalize")
			return err
		}
	}

	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[process] done")
	return nil
}

// encode checks for the codecs of the Track files and encodes to AudioFormat if necessary
func encodePlaylist(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[encodePlaylist] start")
	start := time.Now()
	for i, t := range p.Tracks {
		codec_name, err := utils.GetAudioFileCodecName(t.Filename)
		if err != nil {
			log.Error().Err(fmt.Errorf("[encodePlaylist] get audio codec name"))
			return err
		}

		if !utils.IsCurrentAudioExtension(codec_name) {
			encoded, err := utils.Encode(t.Filename, config.Audio().Format, config.Audio().Encoder)
			if err != nil {
				log.Error().Err(fmt.Errorf("[encodePlaylist] encode audio"))
				return err
			}
			p.Tracks[i].Filename = encoded
		} else {
			log.Debug().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[encodePlaylist] skip")
		}
	}
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[encodePlaylist] done")
	return nil
}

// normalize sets all the track volume to an optimal db
func normalizePlaylist(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[normalize] start")
	start := time.Now()
	for i := range p.Tracks {
		encoded, err := utils.Normalize(p.Tracks[i].Filename)
		if err != nil {
			log.Error().Err(fmt.Errorf("[encodePlaylist] encode audio"))
			return err
		}
		p.Tracks[i].Filename = encoded
	}
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[normalize] done")
	return nil
}

// bounce looks for audio files in the ExtractDir, concatenates them, optionally removes silence, and writes the final metadata, and returns a file handle
func bounce(p *crate.Playlist) (os.File, error) {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[bounce] start")
	start := time.Now()
	err := os.MkdirAll(filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment)), os.ModePerm)
	if err != nil {
		log.Warn().Msg("[Copy] cannot make dest dir")
		return os.File{}, err
	}

	// cleanup after yourself, both mixdown and tracks
	defer utils.CleanupDirs([]string{filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment))})

	// we might need to change the file extension if it doesn't match the one in the config
	if !utils.IsCurrentAudioExtension(filepath.Ext(p.Filename)) {
		oldFile := filepath.Join(config.LibraryDir(), p.Moment, p.Filename)
		defer os.Remove(oldFile)
		p.Filename = strings.Replace(p.Filename, filepath.Ext(p.Filename), fmt.Sprintf(".%s", config.Audio().Format), 1)
	}

	src := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), p.Filename)
	if len(p.Tracks) > 1 {
		_, err := concatenate(p)
		if err != nil {
			log.Error().Err(err).Msg("[bounce] failed to concatenate")
			return os.File{}, err
		}
	} else if len(p.Tracks) > 0 {
		err := utils.CopyFile(p.Tracks[0].Filename, src)
		if err != nil {
			log.Error().Err(err).Msg("[bounce] failed to rename")
			return os.File{}, err
		}
	} else {
		log.Error().Err(err).Msg("[bounce] no tracks")
		return os.File{}, fmt.Errorf("no tracks to bounce")
	}

	if config.Audio().ShouldRemoveSilence {
		err := removeSilence(p)
		if err != nil {
			log.Error().Err(err).Msg("[process] failed to remove silence")
			return os.File{}, err
		}
	}

	err = embedMetadata(p)
	if err != nil {
		log.Error().Err(err).Msg("[Prepare]")
		return os.File{}, err
	}

	dest := filepath.Join(config.LibraryDir(), p.Moment, p.Filename)
	err = utils.CopyFile(src, dest)
	if err != nil {
		log.Error().Err(err).Msg("[bounce] failed to copy to library")
		return os.File{}, err
	}

	result, err := os.OpenFile(dest, os.O_RDONLY, fs.ModePerm)
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[bounce] done")
	return *result, err
}

// concatenates all of a Playlist's Tracks
func concatenate(p *crate.Playlist) (os.File, error) {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[concatenate] start")
	start := time.Now()
	dest := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), p.Filename)
	if _, err := exec.LookPath("ffmpeg"); err != nil {
		log.Error().Err(err).Msg("[concatenate]")
		return os.File{}, err
	}

	list := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), "list.txt")
	flist, err := os.Create(list)
	if err != nil {
		return os.File{}, err
	}
	defer flist.Close()

	final := ""
	for _, t := range p.Tracks {
		final += fmt.Sprintf("file '%s'\n", t.Filename)
	}
	_, err = flist.WriteString(final)
	if err != nil {
		return os.File{}, err
	}

	args := []string{}
	args = append(args, "-hide_banner")
	args = append(args, "-f", "concat")
	args = append(args, "-safe", "0")
	args = append(args, "-i", list)
	args = append(args, "-vn")
	args = append(args, "-c:a", "copy") // copy codec
	args = append(args, "-ac", "2")     // set stereo
	args = append(args, dest)
	args = append(args, "-y")

	log.Debug().Msg("[concatenate] concatening files...")
	cmd := exec.Command("ffmpeg", args...)
	suffix := utils.RandString(8)

	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_concat_%s.log", y, utils.PadDateTime(int(m)), utils.PadDateTime(d), utils.PadDateTime(h), utils.PadDateTime(min), utils.PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	if err != nil {
		log.Error().Err(err).Msg("[concatenate] error starting ffmpeg")
		return os.File{}, err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[concatenate] error completing ffmpeg")
		return os.File{}, err
	}
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[concatenate] done")
	result, err := os.Open(dest)
	return *result, err
}

// embedMetadata writes artist, title and moment to the Playlist file
func embedMetadata(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[embedMetadata] start")
	start := time.Now()
	if _, err := exec.LookPath("ffmpeg"); err != nil {
		log.Error().Err(err).Msg("[embedMetadata]")
		return err
	}

	src := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), p.Filename)
	dest := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), fmt.Sprintf("tmp_metadata.%s", config.Audio().Format))

	args := []string{}
	args = append(args, "-hide_banner")
	args = append(args, "-i", src)
	args = append(args, "-map_metadata", "-1") // strip existing
	args = append(args, "-metadata")
	args = append(args, fmt.Sprintf("title=%s", p.Title))
	args = append(args, "-metadata")
	args = append(args, fmt.Sprintf("artist=%s", p.Artist))
	args = append(args, "-metadata")
	args = append(args, fmt.Sprintf("album=%s", p.Moment))
	args = append(args, "-metadata")
	args = append(args, "performer=dial radio")
	args = append(args, "-metadata")
	args = append(args, fmt.Sprintf("description=%s", p.Description))
	args = append(args, "-vn")
	args = append(args, "-c:a", "copy") // copy codec
	args = append(args, dest)
	args = append(args, "-y")

	cmd := exec.Command("ffmpeg", args...)
	suffix := utils.RandString(8)

	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_metadata_%s.log", y, utils.PadDateTime(int(m)), utils.PadDateTime(d), utils.PadDateTime(h), utils.PadDateTime(min), utils.PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	if err != nil {
		log.Error().Err(err).Msg("[embedMetadata] starting ffmpeg")
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[embedMetadata] completing ffmpeg")
		return err
	}

	err = os.Rename(dest, src)
	if err != nil {
		log.Error().Err(err).Msg("[embedMetadata] renaming")
		return err
	}

	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[embedMetadata] done")
	return nil
}

func removeSilence(p *crate.Playlist) error {
	log.Info().Uint("ID", p.ID).Str("title", p.Title).Str("filename", p.Filename).Msg("[removeSilence] start")
	start := time.Now()
	if _, err := exec.LookPath("ffmpeg"); err != nil {
		log.Error().Err(err).Msg("[removeSilence]")
		return err
	}

	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	src := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), p.Filename)
	dst := filepath.Join(config.ExtractDir(), fmt.Sprintf("%d_%s", p.ID, p.Moment), fmt.Sprintf("silence_removed.%s", config.Audio().Format))

	args := []string{}
	args = append(args, "-hide_banner")
	args = append(args, "-i", src)
	args = append(args, "-af", "silenceremove=window=0:detection=peak:stop_mode=all:start_mode=all:stop_periods=-1:stop_threshold=0")
	args = append(args, dst)

	log.Debug().Strs("ffmpeg_args", args).Msg("[removeSilence]")
	cmd := exec.Command("ffmpeg", args...)
	suffix := utils.RandString(8)

	y, m, d := time.Now().Date()
	log_dst := fmt.Sprintf("%d%d%d_remove_silence_%s.log", y, int(m), d, suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	if err != nil {
		log.Error().Err(err).Msg("[removeSilence] error starting ffmpeg")
		return err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[removeSilence] error completing ffmpeg")
		return err
	}
	log.Info().Uint("ID", p.ID).Str("duration", time.Since(start).String()).Msg("[removeSilence] done")
	err = os.Rename(dst, src)
	return err
}
