# Station

This is the station backend for [Dial Radio](https://dialradio.live), a worldwide playlist broadcasting station. Anyone can submit a link to a playlist and a time of the day at which the playlist should be played.

## Architecture

__Slots__ are composed of user-submitted __playlists__ of a particular __moment__, and up to a particular __duration__. if no playlists are playing during a slot, then songs from the __selections__ of the current moment is playing.

The audio streaming part is handled by a [liquidsoap script](https://gitlab.com/dial-radio/broadcaster), which monitors specific directories for audio files to be streamed to an Icecast server.

See this [flow diagram](https://excalidraw.com/#json=Yd4sC1hCMFaVDN_KimmOj,Ycm8qbGcNuwG2Tu4kEDjBA) for a visual representation.

### Slots

Slots are continuously created, by calling `Rotate()`, if the number of the slots is the below `MIN_SLOT_NUMBER`. their status is based on their `QueueTime` (i.e. in the past or in the future), and only one slot can be marked as `OnAir`. all queued Slots represent the schedule.

Slots have a given `QueueTime`, which takes the queue time of the last Slot with a similar `Moment` and then adds 24 hours to it.

### Playlists

Playlists are user-submitted representations of playlists. The minimum needed for a playlist to be created is to have a Moment and a URL. the playlist is then processed by a Source (downloads, concatenates, filters, tags metadata, see [audio pipeline](#audio-pipeline)), and is then set as available for curation by being marked as `InLibrary`. Playlists also hold references to its tracks, and it calculates its own duration as the sum of all tracks durations.

There are two kinds of Playlists: `Contribution` and `Selection`. The first is uploaded by a listener, while the second is automatically created to fill the remaining time in a Slot.

Those statuses only apply to Playlists, and describe their lifecycle:

- `Pending`: playable audio is not ready yet
- `Processing`: audio is currently being made available
- `InLibrary`: audio is playable, is not in a Slot
- `Scheduled`: is in a queued Slot
- `OnRotation`: is in a filesystem directory where it can be picked up by a streaming application (e.g. a [liquidsoap script](https://gitlab.com/dial-radio/broadcaster))

### Dial

The Dial keeps track of and manages the Slot lifecycle. At the end of each __moment__, the broadcasting process requests a rotation to the __Dial__. The __Dial__ then goes through the process of creating a new __Slot__, curating it with __Playlists__, archiving the previous slot, removing any previous playlists from the , and setting the `LastPlayedTime` of the slot.

The schedule of the radio is provided by the Dial, and is composed of upcoming Slots, sorted by ascending `QueueTime`. The slot currently on air is the first element of the queue.

## Filesystem

### Folders

| path | -
|-----------------------|--------------------------------------------
| `/var/radio/rotation/{moment}` | Hosts playlist files that are currently on rotation. This is where the liquidsoap process looks for audio files to broadcast
| `/var/radio/library/{moment}` | Stores all submitted playlists that are ready to be played
| `/var/radio/selections/{moment}` | songs that are played if nothing else is played
| `/var/radio/extracted/{playlist_id}_{playlist_moment}` | Temporary directory where playlists are created from user contribution. These folders are always deleted at the end of the playlist processing step.

### Selections

The selections folders contain songs that are played whenever all playlists of a given slot have played, or if a slot has no playlists to play. They are organized by moments. In order to add some songs to the selections, you can:

1. log in to `http://128.140.9.107` port `22` via `sftp`
2. credentials: `username: yournamelowercase / password: radioradioyournamelowercase666`
3. put the music under `/var/radio/selections/{moment}`, in `.ogg`, `.opus`, `.wav` or `.mp3`, preferably

## Audio

The file encoding is done by the [playlist](./app/station/playlist.go) and the stream encoding is done by the [broadcaster](./broadcaster/broadcaster.liq).

### Audio pipeline

First, we process each track separately:

1. if the files are directly sent to us, they can be of any format. if the files are downloaded via a given [source](app/source/source.go), depending on the streaming provider. Sources are configured such that they provide files at `128k`. this is the main constraint that forces us to operate at this specific bitrate.
2. Filenames are cleaned to remove any special characters (i.e. whitespace, single quote, commas).
3. Each file is normalized with `ffmpeg-normalize`. the encoder is `libopus`, the extension is `.opus`, the frequency is `48k` and the bitrate is `128k`.
4. in case the files are not `.opus` files, they are encoded via ffmpeg. the encoder is `libopus`, the extension is `.ogg`, the frequency is `48k` and the bitrate is `256k` (that's assuming we have a source file with a higher bitrate, for instance when people send us directly `.wav` or `.mp3` files).

This means we now have a list of tracks that all have the same audio encoding. Next, we create the playlist file by mixing it all together.

1. If there is only one track in the playlist, we consider that it's already mixed.
2. If there are more than one track in the playlist, we concatenate it via ffmpeg by encoding it with `libopus` at `256k` (shouldn't it just be `128k` anyways?).
3. Once the playlist file is concatenated, we run an ffmpeg filter which removes any silence between each track (i.e. audio playlist of a given duration in seconds under a given dB threshold).

## Requirements

- go
- sqlite
- ffmpeg
- ffmpeg-normalize
- yt-dlp
- freyr-js
- spotdl
- imagemagick
