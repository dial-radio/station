package utils

const (
	Morning   = "morning"
	Afternoon = "afternoon"
	Evening   = "evening"
	Night     = "night"

	Pending    = "pending"     // is not ready yet
	Processing = "processing"  // is currently downloading
	InLibrary  = "in_library"  // is ready to be played
	Scheduled  = "scheduled"   // is in a slot
	OnRotation = "on_rotation" // is in a liquidsoap rotation folder

	Contribution = "contribution" // listener-submitted playlist
	Selection    = "selection"    // automatically-generated playlist

	Memory = "memory"
)
