package utils

import (
	"fmt"
	"path/filepath"
	"station/config"
	"testing"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/require"
)

func setupTrack() func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	src := filepath.Join(Basepath, "../files/metadata.mp3")
	dst := filepath.Join(config.Station().BaseDir, "metadata.mp3")
	err := CopyFile(src, dst)
	if err != nil {
		panic(err)
	}

	return func(t *testing.T) {}
}

func TestTrackEncoding(t *testing.T) {
	setupTrack()

	encoder := "libvorbis"
	format := "ogg"
	t.Run("test track encoding", func(t *testing.T) {
		src := filepath.Join(config.Station().BaseDir, "metadata.mp3")
		dst := filepath.Join(config.Station().BaseDir, fmt.Sprintf("metadata.%s", format))
		encoded, err := Encode(src, format, encoder)
		require.Nil(t, err)
		require.Equal(t, encoded, dst)
	})
}
