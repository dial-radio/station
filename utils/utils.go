package utils

import (
	"fmt"
	"io"
	"math/rand"
	"os"
	"os/exec"
	"path/filepath"
	"station/config"
	"strconv"
	"strings"
	"time"
	"unicode"

	"github.com/dhowden/tag"
	"github.com/rs/zerolog/log"
)

// IsAudioExtension checks for whitelisted file extensions
func IsAudioExtension(ext string) bool {
	exts := []string{
		".ogg",
		".opus",
		".mp3",
		".aac",
		".wav",
		".flac",
		".m4a",
	}

	for _, allowed := range exts {
		if strings.Contains(allowed, ext) {
			return true
		}
	}

	return false
}

// IsCurrentAudioExtension checks for a match with the current config
func IsCurrentAudioExtension(ext string) bool {
	return strings.Contains(ext, config.Audio().Format)
}

// returns the duration of an audio file in seconds
func GetAudioFileDuration(fname string) (time.Duration, error) {
	var duration time.Duration
	if _, err := exec.LookPath("ffprobe"); err != nil {
		return duration, err
	}

	args := []string{}
	args = append(args, "-show_entries", "format=duration")
	args = append(args, "-of", "default=noprint_wrappers=1:nokey=1")
	args = append(args, fname)

	cmd := exec.Command("ffprobe", args...)
	suffix := RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_probe_%s.log", y, PadDateTime(int(m)), PadDateTime(d), PadDateTime(h), PadDateTime(min), PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stderr = logfile

	bytes, err := cmd.Output()
	if err != nil {
		log.Error().Err(err).Msg("[GetAudioFileDuration] error starting ffprobe")
		return duration, err
	}
	raw := strings.TrimSpace(string(bytes))
	seconds, err := strconv.ParseFloat(raw, 64)
	if err != nil {
		log.Error().Err(err).Msg("[GetAudioFileDuration] error converting duration")
		return duration, err
	}

	duration, err = time.ParseDuration(fmt.Sprintf("%fs", seconds))

	return duration, err
}

func GetAudioFileFormat(fname string) (string, error) {
	var format_name string
	if _, err := exec.LookPath("ffprobe"); err != nil {
		return format_name, err
	}

	args := []string{}
	args = append(args, "-show_entries", "format=format_name")
	args = append(args, "-of", "default=noprint_wrappers=1:nokey=1")
	args = append(args, fname)

	cmd := exec.Command("ffprobe", args...)
	suffix := RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_probe_%s.log", y, PadDateTime(int(m)), PadDateTime(d), PadDateTime(h), PadDateTime(min), PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stderr = logfile

	bytes, err := cmd.Output()
	if err != nil {
		log.Error().Err(err).Msg("[GetAudioFileFormat] error starting ffprobe")
		return format_name, err
	}
	format_name = strings.TrimSpace(string(bytes))
	return format_name, err
}

func GetAudioFileCodecName(fname string) (string, error) {
	var codec_name string
	if _, err := exec.LookPath("ffprobe"); err != nil {
		return codec_name, err
	}

	args := []string{}
	args = append(args, "-select_streams", "a:0")
	args = append(args, "-show_entries", "stream=codec_name")
	args = append(args, "-of", "default=noprint_wrappers=1:nokey=1")
	args = append(args, fname)

	cmd := exec.Command("ffprobe", args...)
	suffix := RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_probe_%s.log", y, PadDateTime(int(m)), PadDateTime(d), PadDateTime(h), PadDateTime(min), PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		panic(err)
	}
	defer logfile.Close()
	cmd.Stderr = logfile

	bytes, err := cmd.Output()
	if err != nil {
		log.Error().Err(err).Msg("[GetAudioFileCodecName] error starting ffprobe")
		return codec_name, err
	}
	codec_name = strings.TrimSpace(string(bytes))
	return codec_name, err
}

// GetFileMetadata returns the artist and title from the filepath
func GetFileMetadata(filepath string) (string, string, error) {
	var artist, title string

	file, err := os.Open(filepath)
	if err != nil {
		return artist, title, err
	}

	metadata, err := tag.ReadFrom(file)
	if (err != nil && err != tag.ErrNoTagsFound) || metadata == nil { // fallback
		artist, title, err = readMetadataFromFile(filepath)
		if err != nil {
			log.Warn().Err(err).Str("track", filepath).Msg("failed to read tags, skipping...")
		}
	} else {
		artist = metadata.Artist()
		title = metadata.Title()
	}

	return artist, title, nil
}

// readMetadataFromFile is a fallback using ffprobe and parsing the output to get the artist and title
func readMetadataFromFile(filepath string) (string, string, error) {
	var artist, title string
	args := []string{}
	args = append(args, filepath)
	out, err := exec.Command("ffprobe", args...).CombinedOutput()
	if err != nil {
		return artist, title, err
	}

	lines := strings.Split(string(out), "\n")
	for _, line := range lines {
		pair := strings.Split(line, ":")
		if len(pair) == 2 && strings.Trim(pair[0], " ") == "title" {
			title = strings.Trim(pair[1], " ")
		}

		if len(pair) == 2 && strings.Trim(pair[0], " ") == "artist" {
			artist = strings.Trim(pair[1], " ")
		}
	}

	return artist, title, nil
}

func Capitalize(s string) string {
	r := []rune(s)
	r[0] = unicode.ToUpper(r[0])
	return string(r)
}

// cleanFilename takes the Track Filename path and removes all the single quotes and commas in it, then updates the track to the new filename
func CleanFilename(path string) (string, error) {
	_, err := os.Stat(path)
	if err != nil {
		return "", err
	}

	dir := filepath.Dir(path)
	base := filepath.Base(path)
	ext := filepath.Ext(path)
	r := strings.NewReplacer(
		".", "_",
		"'", "_",
		",", "_",
		" ", "_")
	name := strings.Replace(base, ext, "", 1)
	clean := r.Replace(name)

	new := filepath.Join(dir, fmt.Sprintf("%s%s", clean, ext))
	err = os.Rename(path, new)
	return new, err

}

func CopyFile(_src, _dst string) error {
	fin, err := os.Open(_src)
	if err != nil {
		return err
	}
	defer fin.Close()

	fout, err := os.Create(_dst)
	if err != nil {
		return err
	}
	defer fout.Close()

	_, err = io.Copy(fout, fin)
	return err
}

// CleanupDirs removes all directories passed as argument
func CleanupDirs(paths []string) {
	for _, p := range paths {
		log.Info().Str("dir", p).Msg("[CleanupDirs]")
		if err := os.RemoveAll(p); err != nil {
			log.Error().Err(err).Msg("[CleanupDirs]")
		}
	}
}

// CleanupDirFiles removes all files in the given directory
func CleanupDirFiles(path string) error {
	files, err := os.ReadDir(path)
	if err != nil {
		return err
	}
	for _, f := range files {
		fullPath := filepath.Join(path, f.Name())
		if err := os.Remove(fullPath); err != nil {
			return err
		}
	}
	return nil
}

const letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

func RandString(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

// PadDateTime left-pads a given numerical date/time (month, day, hour, minute) with a 0 if there are not enough characters
func PadDateTime(date int) string {
	if date < 10 {
		return fmt.Sprintf("0%d", date)
	}

	return fmt.Sprintf("%d", date)
}

// getTodayDate returns the date from time.Now(), zero-ing hours, minutes and seconds
func GetTodayDate() time.Time {
	y, m, d := time.Now().Date()
	var year, month, day string
	year = fmt.Sprintf("%d", y)
	if int(m) < 10 {
		month = fmt.Sprintf("0%d", m)
	} else {
		month = fmt.Sprintf("%d", m)
	}
	if d < 10 {
		day = fmt.Sprintf("0%d", d)
	} else {
		day = fmt.Sprintf("%d", d)
	}

	today, err := time.ParseInLocation(time.DateTime, fmt.Sprintf("%s-%s-%s 00:00:00", year, month, day), time.Local)
	if err != nil {
		panic(err)
	}

	return today
}

func IsValidMoment(m string) bool {
	switch m {
	case Morning:
		return true
	case Afternoon:
		return true
	case Evening:
		return true
	case Night:
		return true
	}

	return false
}

func getNextMoment(m string) (string, error) {
	switch m {
	case Morning:
		return Afternoon, nil
	case Afternoon:
		return Evening, nil
	case Evening:
		return Night, nil
	case Night:
		return Morning, nil
	}

	return "", fmt.Errorf("invalid moment: %v", m)
}
func GetLocalMoment() string {
	now := time.Now().Hour()

	m, _ := time.ParseInLocation(time.Kitchen, config.Times().Morning, time.Local)
	a, _ := time.ParseInLocation(time.Kitchen, config.Times().Afternoon, time.Local)
	e, _ := time.ParseInLocation(time.Kitchen, config.Times().Evening, time.Local)
	n, _ := time.ParseInLocation(time.Kitchen, config.Times().Night, time.Local)

	if now >= n.Hour() && now < m.Hour() {
		return Night
	} else if now >= m.Hour() && now < a.Hour() {
		return Morning
	} else if now >= a.Hour() && now < e.Hour() {
		return Afternoon
	} else {
		return Evening
	}
}
