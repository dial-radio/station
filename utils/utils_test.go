package utils

import (
	"io"
	"log"
	"os"
	"path/filepath"
	"runtime"
	"station/config"
	"testing"
	"time"

	"github.com/rs/zerolog"
	"github.com/spf13/viper"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

var (
	_, b, _, _ = runtime.Caller(0)
	Basepath   = filepath.Dir(b)
)

func setupUtils() func(t *testing.T) {
	viper.Set("STATION_MODE", "test")
	zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	src, err := os.Open(filepath.Join(Basepath, "../files/fallback.ogg"))
	if err != nil {
		panic(err)
	}
	dst, err := os.Create(filepath.Join(config.Station().BaseDir, "fallback.ogg"))
	if err != nil {
		panic(err)
	}

	_, err = io.Copy(dst, src)
	if err != nil {
		panic(err)
	}

	return func(t *testing.T) {}
}

func TestTrackDuration(t *testing.T) {
	teardown := setupUtils()
	teardown(t)

	t.Run("get track duration", func(t *testing.T) {
		expected, _ := time.ParseDuration("1m17.090227s")
		duration, err := GetAudioFileDuration(filepath.Join(config.Station().BaseDir, "fallback.ogg"))
		require.Nil(t, err)
		assert.Equal(t, expected, duration, "computed duration")
	})
}

func TestFileMetadata(t *testing.T) {
	t.Run("get file metadata", func(t *testing.T) {
		expectedArtist := "trigger"
		expectedTitle := "chrono"
		artist, title, err := GetFileMetadata(filepath.Join(Basepath, "../files/metadata.mp3"))
		require.Nil(t, err)
		assert.Equal(t, expectedArtist, artist, "artist")
		assert.Equal(t, expectedTitle, title, "title")
	})
}

func TestGetTodayDate(t *testing.T) {
	t.Run("get today date", func(t *testing.T) {
		now := time.Now()
		today := GetTodayDate()
		require.NotNil(t, today)
		ny, nm, nd := now.Date()
		ty, tm, td := today.Date()
		assert.Equal(t, ny, ty)
		assert.Equal(t, nm, tm)
		assert.Equal(t, nd, td)
	})
}

func Test_getNextMoment(t *testing.T) {
	type args struct {
		m string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name:    "correct next moment",
			args:    args{m: "morning"},
			want:    "afternoon",
			wantErr: false,
		},
		{
			name:    "wrong next moment",
			args:    args{m: "bla"},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := getNextMoment(tt.args.m)
			if (err != nil) != tt.wantErr {
				t.Errorf("getNextMoment() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("getNextMoment() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsValidMoment(t *testing.T) {
	type args struct {
		m string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "correct moment",
			args: args{m: "morning"},
			want: true,
		},
		{
			name: "correct moment",
			args: args{m: "afternoon"},
			want: true,
		},
		{
			name: "correct moment",
			args: args{m: "evening"},
			want: true,
		},
		{
			name: "correct moment",
			args: args{m: "night"},
			want: true,
		},
		{
			name: "wrong moment",
			args: args{m: "bla"},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsValidMoment(tt.args.m); got != tt.want {
				t.Errorf("IsValidMoment() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestIsAudioExtension(t *testing.T) {
	type args struct {
		ext string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			name: "valid extension",
			args: args{
				ext: "opus",
			},
			want: true,
		},
		{
			name: "valid extension",
			args: args{
				ext: ".opus",
			},
			want: true,
		},
		{
			name: "invalid extension",
			args: args{
				ext: "mkv",
			},
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := IsAudioExtension(tt.args.ext); got != tt.want {
				t.Errorf("IsAudioExtension() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetAudioFileFormat(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))

	type args struct {
		fname string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "finds correct format",
			args: args{
				fname: filepath.Join(Basepath, "../files/fallback.ogg"),
			},
			want:    "ogg",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetAudioFileFormat(tt.args.fname)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAudioFileFormat() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetAudioFileFormat() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetAudioFileCodecName(t *testing.T) {
	type args struct {
		fname string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "finds correct codec",
			args: args{
				fname: filepath.Join(Basepath, "../files/fallback.ogg"),
			},
			want:    "vorbis",
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := GetAudioFileCodecName(tt.args.fname)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetAudioFileCodecName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetAudioFileCodecName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCleanFilename(t *testing.T) {
	config.Init(viper.GetString("STATION_CONFIG_PATH"))
	f, err := os.Create(filepath.Join(config.Station().BaseDir, "test files.txt"))
	if err != nil {
		log.Fatal(err)
	}
	f.Close()

	defer os.Remove("test_files.txt")

	type args struct {
		path string
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "cleans filename",
			args: args{
				path: filepath.Join(config.Station().BaseDir, "test files.txt"),
			},
			want:    filepath.Join(config.Station().BaseDir, "test_files.txt"),
			wantErr: false,
		},
		{
			name: "cleans nonexistant file",
			args: args{
				path: "home/periode/double pon'ey",
			},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := CleanFilename(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("CleanFilename() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("CleanFilename() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestGetLocalMoment(t *testing.T) {
	tests := []struct {
		name string
	}{
		{
			name: "tests local moment",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := GetLocalMoment(); !IsValidMoment(got) {
				t.Errorf("GetLocalMoment() = %v, want valid", got)
			}
		})
	}
}

func TestCopyFile(t *testing.T) {
	setupUtils()

	type args struct {
		_src string
		_dst string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name: "copies files",
			args: args{
				_src: filepath.Join(config.Station().BaseDir, "fallback.ogg"),
				_dst: filepath.Join(config.Station().BaseDir, "fallback_copied.ogg"),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := CopyFile(tt.args._src, tt.args._dst); (err != nil) != tt.wantErr {
				t.Errorf("CopyFile() error = %v, wantErr %v", err, tt.wantErr)
			}

			if _, err := os.Stat(filepath.Join(config.Station().BaseDir, "fallback_copied.ogg")); err != nil {
				t.Errorf("CopyFile() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
