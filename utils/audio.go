package utils

import (
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"station/config"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
)

// Encode takes an absolute source filepath, encodes it to the new format with the new encoder, deletes the original, and returns the encoded filename
func Encode(src string, format string, encoder string) (string, error) {
	if _, err := exec.LookPath("ffmpeg"); err != nil {
		return "", err
	}

	if format == "" {
		format = config.Audio().Format
	}

	if encoder == "" {
		encoder = config.Audio().Encoder
	}

	dst := filepath.Join(filepath.Dir(src), strings.Replace(filepath.Base(src), filepath.Ext(src), fmt.Sprintf(".%s", format), 1))
	if src == dst {
		log.Debug().Str("src", src).Str("dst", dst).Msg("[encode] not overwriting already encoded file!")
		return dst, nil
	}

	args := []string{}
	args = append(args, "-hide_banner")
	args = append(args, "-i", src)
	args = append(args, "-y")
	args = append(args, "-vn")
	args = append(args, "-c:a", encoder)
	args = append(args, "-b:a", "128k")
	args = append(args, "-ac", "2") // set stereo
	args = append(args, dst)

	cmd := exec.Command("ffmpeg", args...)
	suffix := RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_encode_%s.log", y, PadDateTime(int(m)), PadDateTime(d), PadDateTime(h), PadDateTime(min), PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		return dst, err
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	err = cmd.Start()
	log.Debug().Strs("args", args).Str("filename", src).Msg("[encode] cmd")
	if err != nil {
		log.Error().Err(err).Msg("[encode] error starting ffmpeg-encode")
		return dst, err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[encode] error completing ffmpeg-encode")
		return dst, err
	}

	err = os.Remove(src)
	if err != nil {
		log.Error().Err(err).Msg("[encode] error renaming encoded file")
		return dst, err
	}

	return dst, nil
}

func Normalize(src string) (string, error) {
	if _, err := exec.LookPath("ffmpeg-normalize"); err != nil {
		return "", err
	}

	dst := filepath.Join(filepath.Dir(src), strings.Replace(filepath.Base(src), filepath.Ext(src), fmt.Sprintf("_normalized.%s", config.Audio().Format), 1))
	args := []string{}
	args = append(args, src)
	args = append(args, "-vn")
	args = append(args, "-f")
	args = append(args, "-b:a", "128k")
	args = append(args, "-c:a", config.Audio().Encoder)
	args = append(args, "-ar", "48000")
	args = append(args, "-ext", config.Audio().Format)
	args = append(args, "-o", dst)

	cmd := exec.Command("ffmpeg-normalize", args...)
	suffix := RandString(8)
	y, m, d := time.Now().Date()
	h, min, sec := time.Now().Clock()
	log_dst := fmt.Sprintf("%d-%s-%s_%s-%s-%s_normalize_%s.log", y, PadDateTime(int(m)), PadDateTime(d), PadDateTime(h), PadDateTime(min), PadDateTime(sec), suffix)
	logfile, err := os.Create(filepath.Join(config.LogsDir(), "ffmpeg", log_dst))
	if err != nil {
		return dst, err
	}
	defer logfile.Close()
	cmd.Stdout = logfile
	cmd.Stderr = logfile

	log.Debug().Strs("args", args).Str("filename", src).Msg("[normalize]")
	err = cmd.Start()
	if err != nil {
		log.Error().Err(err).Msg("[normalize] error starting ffmpeg-normalize")
		return dst, err
	}

	err = cmd.Wait()
	if err != nil {
		log.Error().Err(err).Msg("[normalize] error completing ffmpeg-normalize")
		return dst, err
	}

	if err = os.Remove(src); err != nil {
		log.Error().Err(err).Msg("[normalize] error removing old file")
		return dst, err
	}

	return dst, nil
}
